package business.to;

import business.bo.Optional;

public class SceltaOptionalTO {

	private Optional optional;
	private int quantita;
	//costo immediatamente dopo aver convalidato l'ordine
	private double costo;
	
	
	public SceltaOptionalTO() {
		// TODO Auto-generated constructor stub
	}
	
	public SceltaOptionalTO(Optional optional, int quantita, double costo)
	{
		init(optional, quantita, costo);
	}
	
	public SceltaOptionalTO(SceltaOptionalTO sceltaOptionalTO)
	{
		init(sceltaOptionalTO.optional, sceltaOptionalTO.quantita, sceltaOptionalTO.costo);
	}
	
	private void init(Optional optional, int quantita, double costo)
	{
		this.optional = optional;
		this.quantita = quantita;
		this.costo = costo;
	}
	
	public SceltaOptionalTO getData()
	{
		return new SceltaOptionalTO(this);
	}
	
	public OptionalTO getOptionalData()
	{
		return optional.getData();
	}
	
	public void setData(Optional optional, int quantita, double costo)
	{
		init(optional, quantita, costo);
	}

	public Optional getOptional() {
		return optional;
	}

	public void setOptional(Optional optional) {
		this.optional = optional;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}
	
	

}
