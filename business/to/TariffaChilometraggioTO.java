package business.to;

public class TariffaChilometraggioTO {

	private int id;
	private String nome;
	private String descrizione;
	

	private void init(int id, String nome, String descrizione)
	{
		this.id = id;
		this.nome = nome;
		this.descrizione = descrizione;
	}
	
	public TariffaChilometraggioTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TariffaChilometraggioTO(TariffaChilometraggioTO TariffaChilometraggioTO)
	{
		init(TariffaChilometraggioTO.id, TariffaChilometraggioTO.nome, TariffaChilometraggioTO.descrizione);
	}
	
	public TariffaChilometraggioTO(int id, String nome, String descrizione)
	{
		init(id, nome, descrizione);
	}
	
	public TariffaChilometraggioTO getData()
	{
		return new TariffaChilometraggioTO(this);
	}
	
	public void setData(int id, String nome, String descrizione)
	{
		init(id, nome, descrizione);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

}
