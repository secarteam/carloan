package business.to;

public class TariffaTemporaleTO {

	private int id;
	private String nome;
	private String descrizione;
	
	private void init(int id, String nome, String descrizione)
	{
		this.id = id;
		this.nome = nome;
		this.descrizione = descrizione;
	}
	
	public TariffaTemporaleTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TariffaTemporaleTO(TariffaTemporaleTO tariffaTemporaleTO)
	{
		init(tariffaTemporaleTO.id, tariffaTemporaleTO.nome, tariffaTemporaleTO.descrizione);
	}
	
	public TariffaTemporaleTO(int id, String nome, String descrizione)
	{
		init(id, nome, descrizione);
	}
	
	public TariffaTemporaleTO getData()
	{
		return new TariffaTemporaleTO(this);
	}
	
	public void setData(int id, String nome, String descrizione)
	{
		init(id, nome, descrizione);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	

}
