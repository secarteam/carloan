package business.to;

public class ClienteTO {
	
	private String username;
	private String email;
	private String nome;
	private String cognome;
	private String numTelefono;
	private String codiceFiscale;
	
	
	public ClienteTO() {}
	
	public ClienteTO(String username, String email, String nome, String cognome, String numTelefono,
			String codiceFiscale) {
		init(username, email, nome, cognome, numTelefono, codiceFiscale);
	}
	
	public ClienteTO(ClienteTO cliente)
	{
		init(cliente.username, cliente.email, cliente.nome, cliente.cognome, cliente.numTelefono, cliente.codiceFiscale);
	}
	
	private void init(String username, String email, String nome, String cognome, String numTelefono,
			String codiceFiscale)
	{
		this.username = username;
		this.email = email;
		this.nome = nome;
		this.cognome = cognome;
		this.numTelefono = numTelefono;
		this.codiceFiscale = codiceFiscale;
	}
	
	public ClienteTO getData()
	{
		return new ClienteTO(this);
	}
	
	public void setData(String username, String email, String nome, String cognome, String numTelefono,
			String codiceFiscale)
	{
		init(username, email, nome, cognome, numTelefono, codiceFiscale);
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String getNumTelefono() {
		return numTelefono;
	}
	
	public void setNumTelefono(String numTelefono) {
		this.numTelefono = numTelefono;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	
}
