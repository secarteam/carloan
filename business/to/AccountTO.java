package business.to;

public class AccountTO {

	private String username;
	private String email;
	
	public AccountTO() {
		// TODO Auto-generated constructor stub
	}

	public AccountTO(String username, String email) {
		init(username, email);
	}
	
	public AccountTO(AccountTO account)
	{
		init(account.username, account.email);
	}
	
	private void init(String username, String email)
	{
		this.username = username;
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setData(String username, String email)
	{
		init(username, email);
	}

	@Override
	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((email == null) ? 0 : email.hashCode());
//		result = prime * result + ((username == null) ? 0 : username.hashCode());
//		return result;
		return username.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		
		AccountTO other = (AccountTO) obj;
		
		if(this.username.equals(other.getUsername()))
			return true;
		else
			return false;
	}
	

}
