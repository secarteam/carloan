package business.to;


public class LavoratoreTO {
	
	private String username;
	private String email;
	private String nome;
	private String cognome;
	private int aziendaID;
	private LavoratoreRuolo ruolo;
	
	
	public LavoratoreTO() {}
	
	public LavoratoreTO(String username, String email, String nome, String cognome, int azienda, LavoratoreRuolo ruolo) {
		init(username, email, nome, cognome, azienda, ruolo);
	}
	
	public LavoratoreTO(LavoratoreTO pers)
	{
		init(pers.username, pers.email, pers.nome, pers.cognome, pers.aziendaID, pers.ruolo);
	}
	
	
	private void init(String username, String email, String nome, String cognome, int azienda, LavoratoreRuolo ruolo)
	{
		this.username = username;
		this.email = email;
		this.nome = nome;
		this.cognome = cognome;
		this.aziendaID = azienda;
		this.ruolo = ruolo;
	}
	
	public LavoratoreTO getData()
	{
		return new LavoratoreTO(this);
	}
	
	public AccountTO getAccountData()
	{
		return new AccountTO(username, email);
	}
	
	public void setData(String username, String email, String nome, String cognome, int azienda, LavoratoreRuolo ruolo)
	{
		init(username, email, nome, cognome, azienda, ruolo);
	}

	public int getAzienda() {
		return aziendaID;
	}

	public void setAzienda(int azienda) {
		this.aziendaID = azienda;
	}

	public LavoratoreRuolo getRuolo() {
		return ruolo;
	}

	public void setRuolo(LavoratoreRuolo ruolo) {
		this.ruolo = ruolo;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

}
