package business.to;


import java.util.LinkedList;
import java.util.List;

import business.bo.Lavoratore;

public class AgenziaTO {

	private int id;
	private String nome;
	private String provincia;
	private String citta;
	private String via;
	private String numCivico;
	protected List<Lavoratore> lavoratori;

	public AgenziaTO()
	{}

	public AgenziaTO(int id, String nome, String provincia, String città, String via, String numCivico, List<Lavoratore> lavoratori) {
		init(id, nome, provincia, città, via, numCivico, lavoratori);
	}

	public AgenziaTO(AgenziaTO agenzia)
	{
		init(agenzia.id, agenzia.nome, agenzia.provincia, agenzia.citta, agenzia.via, agenzia.numCivico, agenzia.lavoratori);
	}

	private void init(int id, String nome, String provincia, String città, String via, String numCivico, List<Lavoratore> lavoratori)
	{
		this.id = id;
		this.nome = nome;
		this.provincia = provincia;
		this.citta = città;
		this.via = via;
		this.numCivico = numCivico;
		this.lavoratori = lavoratori;
	}

	public AgenziaTO getData()
	{
		return new AgenziaTO(this);
	}

	public List<AccountTO> getLavoratoriPersonalData()
	{
		List<AccountTO> accList = new LinkedList<AccountTO>();

		for(Lavoratore e : lavoratori)
		{
			accList.add(new AccountTO(e.getUsername(), e.getEmail()));
		}

		return accList;
	}

	public void setData(int id, String nome, String provincia, String città, String via, String numCivico, List<Lavoratore> lavoratori)
	{
		init(id, nome, provincia, città, via, numCivico, lavoratori);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String città) {
		this.citta = città;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getNumCivico() {
		return numCivico;
	}

	public void setNumCivico(String numCivico) {
		this.numCivico = numCivico;
	}

	public List<Lavoratore> getLavoratori() {
		return lavoratori;
	}

	public void setLavoratori(List<Lavoratore> lavoratori) {
		this.lavoratori = lavoratori;
	}



}
