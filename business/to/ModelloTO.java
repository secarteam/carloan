package business.to;

import business.bo.Fascia;

public class ModelloTO {
	
	private String modello;
	private Fascia fascia;

	public ModelloTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ModelloTO(String modello, Fascia fascia)
	{
		init(modello, fascia);
	}
	
	public ModelloTO(ModelloTO modelloTO)
	{
		init(modelloTO.modello, modelloTO.fascia);
	}
	
	private void init(String modello, Fascia fascia)
	{
		this.modello = modello;
		this.fascia = fascia;
	}
	
	public ModelloTO getData()
	{
		return new ModelloTO(this);
	}
	
	public void setData(String modello, Fascia fascia)
	{
		init(modello, fascia);
	}

	public String getModello() {
		return modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public Fascia getFascia() {
		return fascia;
	}

	public void setFascia(Fascia fascia) {
		this.fascia = fascia;
	}
	
	

}
