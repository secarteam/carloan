package business.to;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import business.bo.Cliente;
import business.bo.Vettura;
import business.bo.SceltaOptional;
import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;

public class ContrattoInCorsoTO {
	
	private int id;
	private Vettura vettura;
	private Cliente cliente;
	private LocalDate dataInizio;
	private LocalDate previsioneConsegna;
	private double saldoIniziale;
	private double acconto;
	private int puntoConsegna;
	
	private List<SceltaOptional> optionalScelti;
	private double costoTariffaTemporale; //al momento della convalida dell'ordine
	private double costoTariffaChilometraggio; //al momento della convalida dell'ordine
	private TariffaChilometraggio tariffaChilometraggio;
	private TariffaTemporale tariffaTemporale;
	
	
	private void init(int id, Vettura vettura, Cliente cliente, LocalDate dataInizio, LocalDate previsioneConsegna, 
					  double saldoIniziale, double acconto, List<SceltaOptional> optionalScelti, double costoTariffaTemporale, 
					  double costoTariffaChilometraggio, TariffaChilometraggio tariffaChilometraggio,
					  TariffaTemporale tariffaTemporale, int puntoConsegna)
	{
		this.id = id;
		this.vettura = vettura;
		this.cliente = cliente;
		this.dataInizio = dataInizio;
		this.previsioneConsegna = previsioneConsegna;
		this.saldoIniziale = saldoIniziale;
		this.acconto = acconto;
		this.optionalScelti = optionalScelti;
		this.costoTariffaChilometraggio = costoTariffaChilometraggio;
		this.costoTariffaTemporale = costoTariffaTemporale;
		this.tariffaChilometraggio = tariffaChilometraggio;
		this.tariffaTemporale = tariffaTemporale;
		this.puntoConsegna = puntoConsegna;
	}
	
	public ContrattoInCorsoTO(int id, Vettura vettura, Cliente cliente, LocalDate dataInizio, LocalDate previsioneConsegna, 
					  double saldoIniziale, double acconto, List<SceltaOptional> optionalScelti, double costoTariffaTemporale, 
					  double costoTariffaChilometraggio, TariffaChilometraggio tariffaChilometraggio,
					  TariffaTemporale tariffaTemporale, int puntoConsegna)
	{
		init(id, vettura, cliente, dataInizio, previsioneConsegna, saldoIniziale, acconto, optionalScelti, costoTariffaTemporale,
				costoTariffaChilometraggio, tariffaChilometraggio, tariffaTemporale, puntoConsegna);
	}
	
	public ContrattoInCorsoTO(ContrattoInCorsoTO cTO)
	{
		init(cTO.id, cTO.vettura, cTO.cliente, cTO.dataInizio, cTO.previsioneConsegna, cTO.saldoIniziale, cTO.acconto, cTO.optionalScelti, cTO.costoTariffaTemporale,
				cTO.costoTariffaChilometraggio, cTO.tariffaChilometraggio, cTO.tariffaTemporale, cTO.puntoConsegna);
	}
	
	public ContrattoInCorsoTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ContrattoInCorsoTO getData()
	{
		return new ContrattoInCorsoTO(this);
	}
	
	public List<SceltaOptionalTO> getOptionalSceltiData()
	{
		List<SceltaOptionalTO> optionalScelti = new LinkedList<SceltaOptionalTO>();
		
		for(SceltaOptional so : this.optionalScelti)
		{
			optionalScelti.add(so.getData());
		}
		
		return optionalScelti;
	}
	
	public void setData(int id, Vettura vettura, Cliente cliente, LocalDate dataInizio, LocalDate previsioneConsegna, 
			  double saldoIniziale, double acconto, List<SceltaOptional> optionalScelti, double costoTariffaTemporale, 
			  double costoTariffaChilometraggio, TariffaChilometraggio tariffaChilometraggio,
			  TariffaTemporale tariffaTemporale, int puntoConsegna)
	{
		init(id, vettura, cliente, dataInizio, previsioneConsegna, saldoIniziale, acconto, optionalScelti, costoTariffaTemporale,
				costoTariffaChilometraggio, tariffaChilometraggio, tariffaTemporale, puntoConsegna);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Vettura getVettura() {
		return vettura;
	}

	public void setVettura(Vettura vettura) {
		this.vettura = vettura;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public LocalDate getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(LocalDate dataInizio) {
		this.dataInizio = dataInizio;
	}

	public LocalDate getPrevisioneConsegna() {
		return previsioneConsegna;
	}

	public void setPrevisioneConsegna(LocalDate previsioneConsegna) {
		this.previsioneConsegna = previsioneConsegna;
	}

	public double getSaldoIniziale() {
		return saldoIniziale;
	}

	public void setSaldoIniziale(double saldoIniziale) {
		this.saldoIniziale = saldoIniziale;
	}

	public double getAcconto() {
		return acconto;
	}

	public void setAcconto(double acconto) {
		this.acconto = acconto;
	}

	public List<SceltaOptional> getOptionalScelti() {
		return optionalScelti;
	}

	public void setOptionalScelti(List<SceltaOptional> optionalScelti) {
		this.optionalScelti = optionalScelti;
	}

	public double getCostoTariffaTemporale() {
		return costoTariffaTemporale;
	}

	public void setCostoTariffaTemporale(double costoTariffaTemporale) {
		this.costoTariffaTemporale = costoTariffaTemporale;
	}

	public double getCostoTariffaChilometraggio() {
		return costoTariffaChilometraggio;
	}

	public void setCostoTariffaChilometraggio(double costoTariffaChilometraggio) {
		this.costoTariffaChilometraggio = costoTariffaChilometraggio;
	}

	public TariffaChilometraggio getTariffaChilometraggio() {
		return tariffaChilometraggio;
	}

	public void setTariffaChilometraggio(TariffaChilometraggio tariffaChilometraggio) {
		this.tariffaChilometraggio = tariffaChilometraggio;
	}

	public TariffaTemporale getTariffaTemporale() {
		return tariffaTemporale;
	}

	public void setTariffaTemporale(TariffaTemporale tariffaTemporale) {
		this.tariffaTemporale = tariffaTemporale;
	}

	public int getPuntoConsegna() {
		return puntoConsegna;
	}

	public void setPuntoConsegna(int puntoConsegna) {
		this.puntoConsegna = puntoConsegna;
	}
	
	
}
