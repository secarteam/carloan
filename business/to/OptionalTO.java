package business.to;

public class OptionalTO {
	
	private int id;
	private String nome;
	private String descrizione;
	private int quantitaDisponibile;
	private double costo;

	public OptionalTO() {
		// TODO Auto-generated constructor stub
	}
	
	public OptionalTO(int id, String nome, String descrizione, int quantitaDisponibile, double costo)
	{
		init(id, nome, descrizione, quantitaDisponibile, costo);
	}
	
	public OptionalTO(OptionalTO optionalTO)
	{
		init(optionalTO.id, optionalTO.nome, optionalTO.descrizione, optionalTO.quantitaDisponibile, optionalTO.costo);
	}
	
	private void init(int id, String nome, String descrizione, int quantitaDisponibile, double costo)
	{
		this.id = id;
		this.nome = nome;
		this.descrizione = descrizione;
		this.quantitaDisponibile = quantitaDisponibile;
		this.costo = costo;
	}
	
	public OptionalTO getData()
	{
		return new OptionalTO(this);
	}
	
	public void setData(int id, String nome, String descrizione, int quantitaDisponibile, double costo)
	{
		init(id, nome, descrizione, quantitaDisponibile, costo);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public int getQuantitaDisponibile() {
		return quantitaDisponibile;
	}

	public void setQuantitaDisponibile(int quantitaDisponibile) {
		this.quantitaDisponibile = quantitaDisponibile;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}
	
	

}
