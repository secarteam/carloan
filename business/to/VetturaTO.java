package business.to;

import business.bo.Modello;

public class VetturaTO {

	private String targa;
	private String casaProduttrice;
	private Modello modello;
	private ManutenzioneType isInManutenzione;
	private int agenzia;
	private double km;

	public VetturaTO() {
		// TODO Auto-generated constructor stub
	}

	public VetturaTO(String targa, String casaProduttrice, Modello modello, ManutenzioneType isInManutenzione, int agenzia, double km)
	{
		init(targa, casaProduttrice, modello, isInManutenzione, agenzia, km);
	}

	public VetturaTO(VetturaTO vetturaTO)
	{
		init(vetturaTO.targa, vetturaTO.casaProduttrice, vetturaTO.modello, vetturaTO.isInManutenzione, vetturaTO.agenzia, vetturaTO.km);
	}

	private void init(String targa, String casaProduttrice, Modello modello, ManutenzioneType isInManutenzione, int agenzia, double km)
	{
		this.targa = targa;
		this.casaProduttrice = casaProduttrice;
		this.modello = modello;
		this.isInManutenzione = isInManutenzione;
		this.agenzia = agenzia;
		this.km = km;
	}

	public VetturaTO getData()
	{
		return new VetturaTO(this);
	}

	public ModelloTO getModelloData()
	{
		return modello.getData();
	}

	public void setData(String targa, String casaProduttrice, Modello modello, ManutenzioneType isInManutenzione, int agenzia, double km)
	{
		init(targa, casaProduttrice, modello, isInManutenzione, agenzia, km);
	}

	public String getTarga() {
		return targa;
	}

	public void setTarga(String targa) {
		this.targa = targa;
	}

	public String getCasaProduttrice() {
		return casaProduttrice;
	}

	public void setCasaProduttrice(String casaProduttrice) {
		this.casaProduttrice = casaProduttrice;
	}

	public Modello getModello() {
		return modello;
	}

	public void setModello(Modello modello) {
		this.modello = modello;
	}

	public boolean isInManutenzione() {
		if(isInManutenzione.equals(isInManutenzione.ORDINARIA) || isInManutenzione.equals(isInManutenzione.STRAORDINARIA))
			return true;
		else
			return false;
	}

	public ManutenzioneType inManutenzione(){
		return isInManutenzione;
	}

	public void setInManutenzione(ManutenzioneType isInManutenzione) {
		this.isInManutenzione = isInManutenzione;
	}

	public int getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(int agenzia) {
		this.agenzia = agenzia;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

}
