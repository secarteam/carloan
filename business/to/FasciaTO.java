package business.to;

import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;

public class FasciaTO {

	private int id;
	private String nome;
	private String descrizione;
	private TariffaChilometraggio tariffaChilometraggio;
	private double tariffaChilometraggioCosto;
	private TariffaTemporale tariffaTemporale;
	private double tariffaTemporaleCosto;

	public FasciaTO() {
		// TODO Auto-generated constructor stub
	}
	
	public FasciaTO(int id, String nome, String descrizione, TariffaChilometraggio tariffaChilometraggio,
			 double tariffaChilometraggioCosto, TariffaTemporale tariffaTemporale, double tariffaTemporaleCosto)
	{
		init(id, nome, descrizione, tariffaChilometraggio, tariffaChilometraggioCosto, tariffaTemporale, tariffaTemporaleCosto);
	}
	
	public FasciaTO(FasciaTO fasciaTO)
	{
		init(fasciaTO.id, fasciaTO.nome, fasciaTO.descrizione, fasciaTO.tariffaChilometraggio,
			 fasciaTO.tariffaChilometraggioCosto, fasciaTO.tariffaTemporale, fasciaTO.tariffaTemporaleCosto);
	}
	
	private void init(int id, String nome, String descrizione, TariffaChilometraggio tariffaChilometraggio,
					 double tariffaChilometraggioCosto, TariffaTemporale tariffaTemporale, double tariffaTemporaleCosto)
	{
		this.id = id;
		this.nome = nome;
		this.descrizione = descrizione;
		this.tariffaChilometraggio = tariffaChilometraggio;
		this.tariffaChilometraggioCosto = tariffaChilometraggioCosto;
		this.tariffaTemporale = tariffaTemporale;
		this.tariffaTemporaleCosto = tariffaTemporaleCosto;
	}
	
	public FasciaTO getData()
	{
		return new FasciaTO(this);
	}
	
	public TariffaChilometraggioTO getTariffaChilometraggioData()
	{
		return tariffaChilometraggio.getData();
	}
	
	public TariffaTemporaleTO getTariffaTemporaleData()
	{
		return tariffaTemporale.getData();
	}
	
	public void setData(int id, String nome, String descrizione, TariffaChilometraggio tariffaChilometraggio,
			 double tariffaChilometraggioCosto, TariffaTemporale tariffaTemporale, double tariffaTemporaleCosto)
	{
		init(id, nome, descrizione, tariffaChilometraggio, tariffaChilometraggioCosto, tariffaTemporale, tariffaTemporaleCosto);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public TariffaChilometraggio getTariffaChilometraggio() {
		return tariffaChilometraggio;
	}

	public void setTariffaChilometraggio(TariffaChilometraggio tariffaChilometraggio) {
		this.tariffaChilometraggio = tariffaChilometraggio;
	}

	public double getTariffaChilometraggioCosto() {
		return tariffaChilometraggioCosto;
	}

	public void setTariffaChilometraggioCosto(double tariffaChilometraggioCosto) {
		this.tariffaChilometraggioCosto = tariffaChilometraggioCosto;
	}

	public TariffaTemporale getTariffaTemporale() {
		return tariffaTemporale;
	}

	public void setTariffaTemporale(TariffaTemporale tariffaTemporale) {
		this.tariffaTemporale = tariffaTemporale;
	}

	public double getTariffaTemporaleCosto() {
		return tariffaTemporaleCosto;
	}

	public void setTariffaTemporaleCosto(double tariffaTemporaleCosto) {
		this.tariffaTemporaleCosto = tariffaTemporaleCosto;
	}
	
	

}
