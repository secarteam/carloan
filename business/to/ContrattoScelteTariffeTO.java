package business.to;

import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;

public class ContrattoScelteTariffeTO {

	private int contrattoID;
	private double costoTariffaTemporale; //al momento della convalida dell'ordine
	private double costoTariffaChilometraggio; //al momento della convalida dell'ordine
	private TariffaChilometraggio tariffaChilometraggio;
	private TariffaTemporale tariffaTemporale;
	
	public ContrattoScelteTariffeTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ContrattoScelteTariffeTO(int contrattoID, double costoTariffaTemporale, double costoTariffaChilometraggio,
					  TariffaChilometraggio tariffaChilometraggio, TariffaTemporale tariffaTemporale)
	{
		init(contrattoID, costoTariffaTemporale, costoTariffaChilometraggio, tariffaChilometraggio, tariffaTemporale);
	}
	
	public ContrattoScelteTariffeTO(ContrattoScelteTariffeTO cto)
	{
		init(cto.contrattoID, cto.costoTariffaTemporale, cto.costoTariffaChilometraggio, cto.tariffaChilometraggio, cto.tariffaTemporale);
	}
	
	private void init(int contrattoID, double costoTariffaTemporale, double costoTariffaChilometraggio,
					  TariffaChilometraggio tariffaChilometraggio, TariffaTemporale tariffaTemporale)
	{
		this.contrattoID = contrattoID;
		this.costoTariffaChilometraggio = costoTariffaChilometraggio;
		this.costoTariffaTemporale = costoTariffaTemporale;
		this.tariffaChilometraggio = tariffaChilometraggio;
		this.tariffaTemporale = tariffaTemporale;
	}
	
	public void setData(int contrattoID, double costoTariffaTemporale, double costoTariffaChilometraggio,
			  TariffaChilometraggio tariffaChilometraggio, TariffaTemporale tariffaTemporale)
	{
		init(contrattoID, costoTariffaTemporale, costoTariffaChilometraggio, tariffaChilometraggio, tariffaTemporale);
	}

	public int getcontrattoID() {
		return contrattoID;
	}

	public void setcontrattoID(int contrattoID) {
		this.contrattoID = contrattoID;
	}

	public double getCostoTariffaTemporale() {
		return costoTariffaTemporale;
	}

	public void setCostoTariffaTemporale(double costoTariffaTemporale) {
		this.costoTariffaTemporale = costoTariffaTemporale;
	}

	public double getCostoTariffaChilometraggio() {
		return costoTariffaChilometraggio;
	}

	public void setCostoTariffaChilometraggio(double costoTariffaChilometraggio) {
		this.costoTariffaChilometraggio = costoTariffaChilometraggio;
	}

	public TariffaChilometraggio getTariffaChilometraggio() {
		return tariffaChilometraggio;
	}

	public void setTariffaChilometraggio(TariffaChilometraggio tariffaChilometraggio) {
		this.tariffaChilometraggio = tariffaChilometraggio;
	}

	public TariffaTemporale getTariffaTemporale() {
		return tariffaTemporale;
	}

	public void setTariffaTemporale(TariffaTemporale tariffaTemporale) {
		this.tariffaTemporale = tariffaTemporale;
	}

}
