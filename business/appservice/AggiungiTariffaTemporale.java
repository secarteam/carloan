package business.appservice;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Fascia;
import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoChilFascia;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;

/**
 * Realizza il caso d'uso relativo all'aggiunta di una nuova tariffa temporale.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class AggiungiTariffaTemporale implements AppServiceInt{
	private String nTT;
	private String descr;
	private String nFascia;
	private double costo;

	@Override
	public Object execute(){
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);

		DaoFascia daoF = null;
		DaoTariffaTemporale daoTT= null;
		DaoTempFascia daoCF= null;

		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

		try{
			daoF = daoFactory.getDaoFascia();
			daoTT= daoFactory.getDaoTariffaTemporale();
			daoCF= daoFactory.getDaoTempFascia();

			Fascia f = new Fascia(daoF.getFascia(nFascia));
			TariffaTemporale tc = new TariffaTemporale(1, nTT, descr);

			if(tc.isTempFasciaPresente(f.getId(), tc.getId()))
				daoCF.updateTempFascia(f.getId(), tc.getId(), costo);
			else
				daoCF.setTempFascia(f.getId(), tc.getId(), costo);
		}
		catch(OperazioneNonCompletata | DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.nTT = (String) parameters.get(0);
		this.descr = (String) parameters.get(1);
		this.nFascia = (String) parameters.get(2);
		this.costo = (Double) parameters.get(3);
	}

}
