package business.appservice;

import java.util.List;

import business.bo.Fascia;
import business.bo.TariffaChilometraggio;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoChilFascia;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;

/**
 * Realizza il caso d'uso relativo alla rimozione di una tariffa di chilometraggio.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RimuoviTariffaChilometraggio implements AppServiceInt{
	private String nTC;
	private String nF;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoFascia df = daoFactory.getDaoFascia();
		DaoTariffaChilometraggio daott = daoFactory.getDaoTariffaChilometraggio();
		DaoChilFascia daoTC= null;

		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

		try{
			daoTC= daoFactory.getDaoChilFascia();

			daoTC.deleteChilFascia(df.getFascia(nF).getId(), daott.getTariffaChilometraggio(nTC).getId() );

		}
		catch(DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.nTC = (String) parameters.get(0);
		this.nF = (String) parameters.get(1);
	}

}
