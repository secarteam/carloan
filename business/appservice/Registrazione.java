package business.appservice;

import java.util.List;

import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import business.OperazioneNonCompletata;
import business.bo.Cliente;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoCliente;
import integration.interfaces.DaoSecData;

/**
 * Realizza il caso d'uso relativo alla registrazione dei clienti.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class Registrazione  implements AppServiceInt{
	private Cliente cliente;
	private String username;
	private String email;
	private String nome;
	private String cognome;
	private String numtelefono;
	private String codiceFiscale;
	private String pws;

	public Registrazione(){}

	public String getPws() {
		return pws;
	}

	public void setPws(String pws) {
		this.pws = pws;
	}

	public Registrazione(Cliente cliente,String pws ){
		this.pws = pws;
		this.cliente = cliente;
	}

	/**/
	@Override
	public Object execute(){
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoCliente daoCliente = null;
		DaoSecData daoSecData = null;
		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoCliente = daoFactory.getDaoCliente();
			daoSecData = daoFactory.getDaoSecData();

			if(cliente.isClienteInStorageByUsername())
				throw new OperazioneNonCompletata("Username già esistente");
			else if(cliente.isClienteInStorageByEmail())
				throw new OperazioneNonCompletata("Email già esistente fra le registrazioni");
			else
			{
				CryptoInt cryptoService = serviceFactory.getEncryptionService("Encryption");
				String hashedPwd = cryptoService.encrypt(pws);
				daoSecData.setUserHash(cliente.getUsername(), hashedPwd);

				daoCliente.setCliente(cliente.getData());

			}

		} catch (DaoException | OperazioneNonCompletata e) {
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		return null;
	}


	@Override
	public void init(List<Object> parameters) {
		this.username = (String) parameters.get(0);
		this.email = (String) parameters.get(1);
		this.nome = (String) parameters.get(2);
		this.cognome = (String) parameters.get(3);
		this.numtelefono = (String) parameters.get(4);
		this.codiceFiscale = (String) parameters.get(5);
		this.pws = (String) parameters.get(6);

		cliente = new Cliente(username, email, nome, cognome, numtelefono, codiceFiscale);
	}
}
