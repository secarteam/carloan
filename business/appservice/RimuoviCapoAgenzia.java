package business.appservice;

import java.util.List;

import business.bo.Agenzia;
import business.bo.Lavoratore;
import business.to.LavoratoreRuolo;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoLavoratore;
import integration.interfaces.DaoSecData;

/**
 * Realizza il caso d'uso relativo alla rimozione di un capo agenzia.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RimuoviCapoAgenzia implements AppServiceInt{
	private Lavoratore lavoratore;

	public RimuoviCapoAgenzia () {}

	public RimuoviCapoAgenzia (Lavoratore lavoratore){
		this.lavoratore=lavoratore;
	}

	@Override
	public Object execute() {

		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoLavoratore daoLavoratore = null;
		DaoSecData daoSecData = null;


		try
		{
			daoLavoratore = daoFactory.getDaoLavoratore();
			daoSecData = daoFactory.getDaoSecData();

			if(daoLavoratore != null && daoSecData != null){
				lavoratore.setRuolo(LavoratoreRuolo.CAPOAGENZIA);
				Lavoratore retrievalLav = new Lavoratore(daoLavoratore.getLavoratore(lavoratore.getData()));


				daoSecData.deleteUserHash(retrievalLav.getUsername());
				daoLavoratore.deleteLavoratore(retrievalLav);
			}

		}
		catch(DaoException | NoEntryException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.lavoratore = (Lavoratore) parameters.get(0);
	}

}
