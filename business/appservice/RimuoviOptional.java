package business.appservice;

import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoOptional;

import java.util.List;

import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import business.OperazioneNonCompletata;
import business.bo.Optional;

/**
 * Realizza il caso d'uso relativo alla rimozione di un optional.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RimuoviOptional implements AppServiceInt{
	private String nop;

	@Override
	public Object execute() {

		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoOptional = null;
		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoOptional = daoFactory.getDaoOptional();

			Optional op = new Optional(daoOptional.getOptional(nop).getData());

			if(!op.isOptionalInContrattoInCorso()){
					daoOptional.deleteOptional(op.getData());
			}
			else{
				throw new OperazioneNonCompletata("Impossibile rimuovere optional. Optional presente in un contratto in corso.");
			}
		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.nop = (String) parameters.get(0);
	}
}
