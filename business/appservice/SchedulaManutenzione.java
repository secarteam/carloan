package business.appservice;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Vettura;
import business.to.ManutenzioneType;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoVettura;

/**
 * Realizza il caso d'uso relativo alla schedulazione della manutenzione per una
 * determinata auto.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class SchedulaManutenzione implements AppServiceInt{
	private String targa;
	private String manut;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura daov = daoFactory.getDaoVettura();
		Vettura v = new Vettura(daov.getVettura(targa).getData());

		try {
			if(v.isVetturaInStorage()){
				ManutenzioneType m = null;
				if(manut.equals(m.ORDINARIA.name()))
					v.setInManutenzione(m.ORDINARIA);
				else
					v.setInManutenzione(m.STRAORDINARIA);
				daov.updateVettura(v);
			}
		}
		catch (DaoException | OperazioneNonCompletata e) {
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.targa = (String) parameters.get(0);
		this.manut = (String) parameters.get(1);
	}


}
