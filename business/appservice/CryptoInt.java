package business.appservice;

import business.OperazioneNonCompletata;

/**
 * Interfaccia per le funzionalità di crittografia.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public interface CryptoInt {
	public String encrypt(String pwd) throws OperazioneNonCompletata;
	public boolean verify(String pwd, String enc) throws OperazioneNonCompletata;
}
