package business.appservice;

import java.util.Iterator;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Cliente;
import business.bo.Vettura;
import business.to.VetturaTO;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoCliente;

/**
 * Realizza il caso d'uso relativo all'ottenimento dei dati del cliente.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class OttieniDatiCliente implements AppServiceInt{
	private String mail;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoCliente daoC = null;
		Cliente c = null;

		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoC = daoFactory.getDaoCliente();
			c = new Cliente(daoC.getClienteByEmail(mail));
		}
		catch(DaoException | NoEntryException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return c;
	}

	@Override
	public void init(List<Object> parameters) {
		this.mail = (String) parameters.get(0);
	}

}
