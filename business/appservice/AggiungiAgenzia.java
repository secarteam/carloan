package business.appservice;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Agenzia;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;

/**
 * Realizza il caso d'uso relativo all'aggiunta di una agenzia.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class AggiungiAgenzia implements AppServiceInt{
	private String nome;
	private String provincia;
	private String citta;
	private String via;
	private String numCivico;

	@Override
	public void init(List<Object> parameters) {
		this.nome = (String) parameters.get(0);
		this.provincia = (String) parameters.get(1);
		this.citta = (String) parameters.get(2);
		this.via = (String) parameters.get(3);
		this.numCivico = (String) parameters.get(4);
	}

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoAgenzia daoA = null;

		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoA = daoFactory.getDaoAgenzia();
			Agenzia agenzia = new Agenzia(1, nome, provincia, citta, via, numCivico, null);
			
			if(!agenzia.isAgenziaInStorageByName())
				daoA.setAgenzia(agenzia.getData());
			else
				throw new OperazioneNonCompletata("Agenzia con lo stesso nome già presente nel database");
		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return null;
	}


}
