package business.appservice;

import java.time.LocalDate;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.SceltaOptional;
import business.bo.Vettura;
import business.to.ContrattoInCorsoTO;
import business.to.OptionalTO;
import business.to.SceltaOptionalTO;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoContrattoScelte;
import integration.interfaces.DaoContrattoTerminato;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoVettura;

/**
 * Realizza il caso d'uso relativo alla chiusura del contratto.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class ChiudiContratto implements AppServiceInt{

	private String idC;
	private LocalDate dataCE;
	private double saldoF;
	private String chilEff;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoContrattoTerminato daoCT = null;
		DaoContrattoInCorso daoCiC = null;
		DaoContrattoScelte daoO = null;
		DaoOptional daoop = null;
		DaoVettura daov = null;


		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try{
			daoCiC = daoFactory.getDaoContratto();
			daoO = daoFactory.getDaoContrattoScelte();
			daoCT = daoFactory.getDaoContrattoTerminato();
			daoop = daoFactory.getDaoOptional();
			daov = daoFactory.getDaoVettura();


			ContrattoInCorsoTO CiC = daoCiC.getContratto(Integer.valueOf(idC));
			Vettura v = daoCiC.getContratto(CiC.getId()).getVettura();
			daoCT.setContratto(CiC, dataCE, saldoF);

			List<SceltaOptionalTO> so = CiC.getOptionalSceltiData();

			for(SceltaOptionalTO soTO : so){
				OptionalTO o = soTO.getOptionalData();
				o.setQuantitaDisponibile(o.getQuantitaDisponibile() + soTO.getQuantita());
				daoop.updateOptional(o);
			}

			if(CiC.getPuntoConsegna() != daoCiC.getContratto(CiC.getId()).getVettura().getAgenzia())
				v.setAgenzia(CiC.getPuntoConsegna());

			v.setKm(v.getKm() + Double.parseDouble(chilEff));
			daov.updateVettura(v);

			daoO.deleteContrattoScelte(Integer.valueOf(idC));
			//se non funzione la delete a cascata implementare deleteContrattoOptional(int idContratto)
			//in MysqlDaoContrattoInCorso

			daoCiC.deleteContratto(CiC);
		}
		catch(DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.idC = (String) parameters.get(0);
		this.dataCE = (LocalDate) parameters.get(1);
		this.saldoF = (Double) parameters.get(2);
		this.chilEff = (String) parameters.get(3);
	}

}
