package business.appservice;

import java.util.List;

import business.to.ContrattoInCorsoTO;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoContrattoInCorso;

/**
 * Realizza il caso d'uso relativo alla registrazione dell'acconto relativo ad un contratto.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RegistraAcconto implements AppServiceInt {
	
	private int idContratto;
	private double acconto;

	@Override
	public Object execute() {
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();

			ContrattoInCorsoTO cTO = daoCiC.getContratto(idContratto);
			
			cTO.setAcconto(acconto);
			daoCiC.updateContratto(cTO);
		}
		catch(DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		idContratto = (Integer) parameters.get(0);
		acconto = (Double) parameters.get(1);
	}

}
