package business.appservice;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Agenzia;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;

/**
 * Realizza il caso d'uso relativo alla rimozione di un'agenzia.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RimuoviAgenzia implements AppServiceInt{
	private  String na;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoAgenzia daoA = null;

		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoA = daoFactory.getDaoAgenzia();
			
			Agenzia agenzia = new Agenzia();
			agenzia.setId(0);
			agenzia.setNome(na);
			
			if(agenzia.isAgenziaInStorageByName())
				daoA.deleteAgenzia(agenzia.getData());
			else
				throw new OperazioneNonCompletata("Agenzia non presente nel database");
		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.na = (String) parameters.get(0);
	}
}
