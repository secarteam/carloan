package business.appservice;

public class ServiceProperties {
	
	private Class serviceImpl;
	private String view;

	public Class getServiceImpl() {
		return serviceImpl;
	}

	public void setServiceImpl(Class serviceImpl) {
		this.serviceImpl = serviceImpl;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}


	public ServiceProperties(Class serviceImpl, String view) {
		this.serviceImpl = serviceImpl;
		this.view = view;
	}
	
	public ServiceProperties() {}

}
