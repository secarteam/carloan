package business.appservice;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Vettura;
import business.to.ManutenzioneType;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoVettura;

/**
 * Realizza il caso d'uso che permette di togliere le auto dalla manutenzione e renderle
 * nuovamente disponibile.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RendiNoleggiabile implements AppServiceInt{
		private String targa;

		@Override
		public Object execute() {
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoVettura daov = daoFactory.getDaoVettura();
			Vettura v = new Vettura(daov.getVettura(targa).getData());

			try {
				if(v.isVetturaInStorage() && v.isInManutenzione()){
					ManutenzioneType m = null;
					v.setInManutenzione(m.NO);
					daov.updateVettura(v);
				}
			}
			catch (DaoException | OperazioneNonCompletata e) {
				ErrorHandlerInt errorWindow = new ErrorHandler(e);
				errorWindow.handleError();
			}

			return null;
		}

		@Override
		public void init(List<Object> parameters) {
			this.targa = (String) parameters.get(0);
		}
}
