package business.appservice;

import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Agenzia;
import business.bo.Lavoratore;
import business.to.LavoratoreRuolo;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoLavoratore;
import integration.interfaces.DaoSecData;

/**
 * Implementa il caso d'uso Inserisci Operatore Azienda.
 * Aggiunge un Operatore d'Azienda nell'agenzia specificata.
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 *
 */
public class InserisciOperatoreAzienda implements AppServiceInt {
	
	private Agenzia agenzia;
	private Lavoratore lavoratore;
	private String pwd;
	
	public InserisciOperatoreAzienda() {}

	public Agenzia getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(Agenzia agenzia) {
		this.agenzia = agenzia;
	}

	public Lavoratore getLavoratore() {
		return lavoratore;
	}

	public void setLavoratore(Lavoratore lavoratore) {
		this.lavoratore = lavoratore;
	}
	
	public String getPwd()
	{
		return pwd;
	}
	
	public void setPwd(String pwd)
	{
		this.pwd = pwd;
	}
	

	public InserisciOperatoreAzienda(Agenzia agenzia, Lavoratore lavoratore, String pwd) {
		this.agenzia = agenzia;
		this.lavoratore = lavoratore;
		this.pwd = pwd;
	}
	
	public void init(List<Object> parameters)
	{
		this.agenzia = (Agenzia) parameters.get(0);
		this.lavoratore = (Lavoratore) parameters.get(1);
		this.pwd = (String) parameters.get(2);
	}
	/**
	 * Ottiene tramite i DAO l'agenzia avente i criteri specificati dall'entità Agenzia assegnata
	 * alla classe tramite il costruttore o il setter.
	 * Setta il ruolo del lavoratore, chiama il relativo metodo dell'entità Agenzia per aggiungerlo ad essa
	 * e tramite gli appositi DAO memorizza username e password crittografata sul datasource/storage
	 * e crea un'entry per il lavoratore.
	 */
	@Override
	public Object execute() {
		
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoAgenzia daoAgenzia = null;
		DaoLavoratore daoLavoratore = null;
		DaoSecData daoSecData = null;
		AppServiceFactory serviceFactory = null;
		
		serviceFactory = AppServiceFactory.getInstance();
		
		try
		{
			daoAgenzia = daoFactory.getDaoAgenzia();
			daoLavoratore = daoFactory.getDaoLavoratore();
			daoSecData = daoFactory.getDaoSecData();
		}
		catch(DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		
		if(daoAgenzia != null && daoLavoratore != null && daoSecData != null)
		{
			Agenzia retrievedAgency = new Agenzia(daoAgenzia.getAgenzia(agenzia.getNome()));
			agenzia = retrievedAgency;
			
			lavoratore.setRuolo(LavoratoreRuolo.OPERATOREAZIENDA);
			
			try
			{
				agenzia.aggiungiLavoratore(lavoratore);
				
				CryptoInt cryptoService = serviceFactory.getEncryptionService("Encryption");
				String hashedPwd = cryptoService.encrypt(pwd);
				daoSecData.setUserHash(lavoratore.getUsername(), hashedPwd);
				
				
				daoLavoratore.setLavoratore(lavoratore.getData());
			}
			catch(OperazioneNonCompletata | DaoException e)
			{
				ErrorHandlerInt errorWindow = new ErrorHandler(e);
				errorWindow.handleError();
			}
			
		}
		
		return null;
		

	}

}
