package business.appservice;

import java.util.List;

import business.OperazioneNonCompletata;

/**
 * Interfaccia degli Application Service, i quali sono realizzati
 * seguendo il design pattern Command.
 * Oltre al metodo {@link #execute()} è anche richiesto il metodo
 * {@link #init(List parameters) init(List)} per passare i parametri
 * di input all'istanza creata.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public interface AppServiceInt {
	public Object execute();
	public void init(List<Object> parameters);
}
