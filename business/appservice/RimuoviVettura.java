package business.appservice;

import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoVettura;

import java.util.List;

import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import business.OperazioneNonCompletata;
import business.bo.Vettura;

/**
 * Realizza il caso d'uso relativo alla rimozione di una vettura.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RimuoviVettura implements AppServiceInt{
	private String targa;
	
	public RimuoviVettura() {}
	
	public RimuoviVettura(String targa){
		this.targa = targa;
	}
	
	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura daoVettura = null;
		AppServiceFactory serviceFactory = null;
		
		serviceFactory = AppServiceFactory.getInstance();
		
		try
		{
			daoVettura = daoFactory.getDaoVettura();
			
			Vettura vettura = new Vettura();
			vettura.setTarga(targa);
			
			if(!vettura.isVetturaInStorage())
				throw new OperazioneNonCompletata("Vettura non presente nello storage");
			else if(vettura.isVetturaInContrattoInCorso())
				throw new OperazioneNonCompletata("Vettura sotto contratto");
			
			daoVettura.deleteVettura(targa);
		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.targa=(String) parameters.get(0);
	}

}
