package business.appservice;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.bo.Agenzia;
import business.bo.Fascia;
import business.bo.Vettura;
import business.to.VetturaTO;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoVettura;

/**
 * Realizza il caso d'uso relativo all'esplorazione dei servizi disponibili,
 * conoscendo le date desiderate, la fascia desiderata e il punto di ritiro desiderati.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class EsploraServizi implements AppServiceInt{
	private String naziendaRitiro;
	//private String naziendaConsegna;
	//private LocalDate dataInizio;
	//private LocalDate dataFine;
	private String nfascia;

	@Override
	public void init(List<Object> parameters) {
		this.naziendaRitiro = (String) parameters.get(0);
		//this.naziendaConsegna = (String) parameters.get(1);
		//this.dataInizio = (LocalDate) parameters.get(2);
		//this.dataFine = (LocalDate) parameters.get(3);
		this.nfascia = (String) parameters.get(1);
	}

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura daoVettura = null;
		DaoAgenzia daoAgenzia = null;
		DaoFascia daoFascia = null;
		List<VetturaTO> listV = new LinkedList<VetturaTO>();
		List<Vettura> listVet = new LinkedList<Vettura>();
		Vettura v = null;

		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoVettura = daoFactory.getDaoVettura();
			daoAgenzia = daoFactory.getDaoAgenzia();
			daoFascia = daoFactory.getDaoFascia();
			Agenzia a = new Agenzia(daoAgenzia.getAgenzia(naziendaRitiro));
			Fascia f = new Fascia(daoFascia.getFascia(nfascia));
			listV = daoVettura.getVetture(a.getId());

			Iterator<VetturaTO> vetIter = listV.iterator();

			while(vetIter.hasNext()){
				v = new Vettura(vetIter.next());
				if(!v.isVetturaInContrattoInCorso() && (v.getModello().getFascia().getId() == f.getId()) && !v.isInManutenzione())
						listVet.add(v);
			}
		}
		catch(OperazioneNonCompletata | DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		return listVet;
	}

}
