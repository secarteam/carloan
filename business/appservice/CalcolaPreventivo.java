package business.appservice;

import java.util.List;
import java.util.Map;
import java.util.Set;

import business.OperazioneNonCompletata;
import business.bo.Fascia;
import business.bo.Optional;
import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.config.ConfigKeysSpec;
import integration.config.ConfigReader;
import integration.factories.DaoFactory;
import integration.interfaces.DaoChilFascia;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;
import integration.interfaces.DaoVettura;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Realizza il caso d'uso relativo al calcolo del preventivo di un contratto.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class CalcolaPreventivo implements AppServiceInt{

	private String vettura;
	private String cliente;
	private LocalDate d1;
	private LocalDate d2;
	private double saldoIniziale;
	private String nTC;
	private String nTT;
	private Map<String, Integer> optionalScelti;


	private double Moltiplica(long intervallo, double costoTariffa ){
		double m = intervallo * costoTariffa;
		return m;
	}

	@Override
	public void init(List<Object> parameters) {
		this.vettura = (String) parameters.get(0);
		this.cliente = (String) parameters.get(1);
		this.d1 = (LocalDate) parameters.get(2);
		this.d2 = (LocalDate) parameters.get(3);
		this.saldoIniziale = (Double) parameters.get(4);
		this.nTC = (String) parameters.get(5);
		this.nTT = (String) parameters.get(6);
		this.optionalScelti = (Map<String, Integer>) parameters.get(7);
	}


	@Override
	public Object execute() {

		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura daoVettura = null;
		DaoTariffaChilometraggio daoTC = null;
		DaoTariffaTemporale daoTT = null;
		DaoTempFascia dtf = null;
		DaoChilFascia dcf = null;
		DaoOptional daoO = null;
		double saldoI = 0;
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

		try{
			daoVettura = daoFactory.getDaoVettura();
			daoTC = daoFactory.getDaoTariffaChilometraggio();
			daoTT = daoFactory.getDaoTariffaTemporale();
			dtf = daoFactory.getDaoTempFascia();
			dcf = daoFactory.getDaoChilFascia();
			daoO = daoFactory.getDaoOptional();

			Fascia f = new Fascia(daoVettura.getVettura(vettura).getModello().getFascia().getData());
			TariffaChilometraggio tc = new TariffaChilometraggio(daoTC.getTariffaChilometraggio(nTC).getData());
			TariffaTemporale tt = new TariffaTemporale(daoTT.getTariffaTemporale(nTT).getData());


			ConfigReader reader = new ConfigReader(ConfigKeysSpec.getPath());

			long tempIntervallo = ChronoUnit.DAYS.between(d1, d2);

			long intervallo = (tempIntervallo >= 6) ? tempIntervallo : 6;

			if(tt.getNome().equals(reader.getProperty(ConfigKeysSpec.getTargior()))){
				saldoI = saldoI + Moltiplica(intervallo,dtf.getTempFasciaCosto(f.getId(), tt.getId()));
			}
			else{
				saldoI = Moltiplica( (int) Math.ceil(intervallo/6),dtf.getTempFasciaCosto(f.getId(), tt.getId()));
			}

			if(tc.getNome().equals(reader.getProperty(ConfigKeysSpec.getTarill()))){
				saldoI = saldoI + dcf.getChilFasciaCosto(f.getId(), tc.getId());
			}

			if(!optionalScelti.isEmpty()){
				Set<Map.Entry<String, Integer>> entries = optionalScelti.entrySet();
				for(Map.Entry<String, Integer> entry : entries)
				{
					Optional opt = new Optional(daoO.getOptional(entry.getKey()));
					saldoI = saldoI + (opt.getCosto() * entry.getValue());
				}
			}
			saldoIniziale = saldoI;
		}
		catch(DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return this.saldoIniziale;
	}

}
