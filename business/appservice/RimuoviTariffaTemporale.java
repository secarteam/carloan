package business.appservice;

import java.util.List;

import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;

/**
 * Realizza il caso d'uso relativo alla rimozione di una tariffa temporale.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class RimuoviTariffaTemporale implements AppServiceInt{
	private String nTT;
	private String nF;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoFascia df = daoFactory.getDaoFascia();
		DaoTariffaTemporale daott = daoFactory.getDaoTariffaTemporale();
		DaoTempFascia daoTF= null;

		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

		try{
			daoTF= daoFactory.getDaoTempFascia();

			daoTF.deleteTempFascia(df.getFascia(nF).getId(),daott.getTariffaTemporale(nTT).getId() );

		}
		catch(DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.nTT = (String) parameters.get(0);
		this.nF = (String) parameters.get(1);
	}
}
