package business.appservice;

import java.util.List;

import presentation.Logger;
import presentation.UserRole;

/**
 * Realizza il caso d'uso relativo alle operazioni di logout
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class EffettuaLogout implements AppServiceInt {

	@Override
	public Object execute() {
		
		Logger logger = Logger.getInstance();
		logger.setUsername("Anonymous");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}

}
