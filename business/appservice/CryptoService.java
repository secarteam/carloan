package business.appservice;

import business.OperazioneNonCompletata;
import business.appservice.PasswordStorage.CannotPerformOperationException;
import business.appservice.PasswordStorage.InvalidHashException;

/**
 * Implementa {@link CryptoInt} fornendo un'implementazione per i metodi
 * definiti in quell'interfaccia. Questa classe usa {@link PasswordStorage}
 * per fornire funzionalità di crittografia riguardanti il salted hashing.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class CryptoService implements CryptoInt {
	
	public CryptoService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String encrypt(String pwd) throws OperazioneNonCompletata {
		try {
			return PasswordStorage.createHash(pwd);
		} catch (CannotPerformOperationException e) {
			throw new OperazioneNonCompletata(e);
		}
	}

	@Override
	public boolean verify(String pwd, String enc) throws OperazioneNonCompletata {
		
		try {
			return PasswordStorage.verifyPassword(pwd,  enc);
		} catch (CannotPerformOperationException | InvalidHashException e) {
			throw new OperazioneNonCompletata(e);
		}
	}

}
