package business.appservice;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import business.bo.Agenzia;
import business.bo.Cliente;
import business.bo.ContrattoInCorso;
import business.bo.Optional;
import business.bo.SceltaOptional;
import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;
import business.bo.Vettura;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoChilFascia;
import integration.interfaces.DaoCliente;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoLavoratore;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoSecData;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;
import integration.interfaces.DaoVettura;
import javafx.collections.SetChangeListener;

/**
 * Realizza il caso d'uso relativo alla convalida del noleggio.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class Noleggia implements AppServiceInt{
	private ContrattoInCorso CiC;

	private String targav;
	private String user;
	private LocalDate d1;
	private LocalDate d2;
	private double saldoIniziale;
	private double acconto;
	private String nAgenziaCons;
	private Map<String, Integer> optionalScelti;
	private String nTC;
	private String nTT;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura daoVettura = null;
		DaoTariffaChilometraggio daoTC = null;
		DaoTariffaTemporale daoTT = null;
		DaoTempFascia dtf = null;
		DaoChilFascia dcf = null;
		DaoOptional daoO = null;
		DaoCliente daoCl = null;
		DaoAgenzia daoA = null;
		DaoContrattoInCorso daoCiC = null;
		AppServiceFactory serviceFactory = null;
		List<SceltaOptional> opt = new LinkedList<SceltaOptional>();
		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoVettura = daoFactory.getDaoVettura();
			daoCl = daoFactory.getDaoCliente();
			daoTC = daoFactory.getDaoTariffaChilometraggio();
			daoTT = daoFactory.getDaoTariffaTemporale();
			dtf = daoFactory.getDaoTempFascia();
			dcf = daoFactory.getDaoChilFascia();
			daoO = daoFactory.getDaoOptional();
			daoA = daoFactory.getDaoAgenzia();
			daoCiC = daoFactory.getDaoContratto();

			Vettura v = new Vettura(daoVettura.getVettura(targav));
			Cliente c = new Cliente(daoCl.getCliente(user));
			TariffaChilometraggio tc = new  TariffaChilometraggio(daoTC.getTariffaChilometraggio(nTC));
			TariffaTemporale tt = new TariffaTemporale(daoTT.getTariffaTemporale(nTT));
			Agenzia ac = new Agenzia(daoA.getAgenzia(nAgenziaCons));
			double c1 = dtf.getTempFasciaCosto(v.getModello().getFascia().getId(), tc.getId());
			double c2 = dcf.getChilFasciaCosto(v.getModello().getFascia().getId(), tt.getId());

			if(!optionalScelti.isEmpty()){
				Set<Map.Entry<String, Integer>> entries = optionalScelti.entrySet();
				for(Map.Entry<String, Integer> entry : entries)
				{
					Optional op = new Optional(daoO.getOptional(entry.getKey()));
					SceltaOptional so = new SceltaOptional(op, entry.getValue(), daoO.getOptional(op.getId()).getCosto());
					op.setQuantitaDisponibile(op.getQuantitaDisponibile() - entry.getValue());
					daoO.updateOptional(op.getData());
					opt.add(so);
				}
			}


			CiC = new ContrattoInCorso(1, v, c, d1, d2, saldoIniziale, acconto, opt, c1, c2, tc, tt, ac.getId());
			daoCiC.setContratto(CiC.getData());
		}
		catch(DaoException e)
		{
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.targav= (String) parameters.get(0);
		this.user = (String) parameters.get(1);
		this.d1 = (LocalDate) parameters.get(2);
		this.d2 = (LocalDate) parameters.get(3);
		this.saldoIniziale = (Double) parameters.get(4);
		this.nTC = (String) parameters.get(5);
		this.nTT = (String) parameters.get(6);
		this.optionalScelti = (Map<String, Integer>) parameters.get(7);
		this.acconto = (Double) parameters.get(8);
		this.nAgenziaCons = (String) parameters.get(9);
	}


}
