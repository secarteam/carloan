package business.appservice;

import java.util.List;

import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoCliente;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoModello;
import integration.interfaces.DaoVettura;
import business.OperazioneNonCompletata;
import business.bo.Agenzia;
import business.bo.Fascia;
import business.bo.Modello;
import business.bo.Vettura;
import business.to.ManutenzioneType;

/**
 * Realizza il caso d'uso relativo all'aggiunta di una vettura.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class AggiungiVettura implements AppServiceInt{
	private String targa;
	private String nomeModello;
	private String nomeFascia;
	private String nomeAzienda;
	private String casaProduttrice;
	private String isManutenzione;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura daoVettura = null;
		DaoAgenzia daoAgenzia = null;
		DaoModello daoModello = null;
		DaoFascia daoFascia = null;

		AppServiceFactory serviceFactory = null;

		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoVettura = daoFactory.getDaoVettura();
			daoAgenzia = daoFactory.getDaoAgenzia();
			daoModello = daoFactory.getDaoModello();
			daoFascia = daoFactory.getDaoFascia();
			Modello m = new Modello(nomeModello, new Fascia(daoFascia.getFascia(nomeFascia)));
			if(!m.isModelloPresente())
				daoModello.setModello(m.getData());
			Agenzia a = new Agenzia(daoAgenzia.getAgenzia(nomeAzienda));
			ManutenzioneType manut = null;
			Vettura v = new Vettura(targa, casaProduttrice, m, manut.valueOf(isManutenzione), a.getId(), 0.0);
			daoVettura.setVettura(v.getData());
		}
		catch(OperazioneNonCompletata | DaoException e)
		{
				ErrorHandlerInt errorWindow = new ErrorHandler(e);
				errorWindow.handleError();
		}

		return null;
	}

	@Override
	public void init(List<Object> parameters) {
		this.targa= (String) parameters.get(0);
		this.nomeModello = (String) parameters.get(1);
		this.nomeFascia = (String) parameters.get(2);
		this.nomeAzienda = (String) parameters.get(3);
		this.casaProduttrice = (String) parameters.get(4);
		this.isManutenzione = (String) parameters.get(5);
	}


}
