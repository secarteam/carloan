package business.appservice;

import java.util.List;

import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoVettura;
import business.bo.Optional;

/**
 * Realizza il caso d'uso relativo all'aggiunta di un optional.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class AggiungiOptional implements AppServiceInt{
	private String nome;
	private String descrizione;
	private int quantitaDisponibile;
	private double costo;


	@Override
	public void init(List<Object> parameters) {
		this.nome = (String) parameters.get(0);
		this.descrizione = (String) parameters.get(1);
		this.quantitaDisponibile = (Integer) parameters.get(2);
		this.costo = (double) parameters.get(3);
	}

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoOptional = null;
		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

		try
		{
			daoOptional = daoFactory.getDaoOptional();
			Optional optional = new Optional(1, nome, descrizione, quantitaDisponibile, costo);
			daoOptional.setOptional(optional.getData());
		}
		catch(DaoException e)
		{
				ErrorHandlerInt errorWindow = new ErrorHandler(e);
				errorWindow.handleError();
		}
		return null;
	}



}
