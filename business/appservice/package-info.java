/**
 * Contiene le classi di tipo Application Service, usate per concretizzare i casi d'uso
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */
package business.appservice;