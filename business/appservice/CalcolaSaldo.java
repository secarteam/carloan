package business.appservice;

import java.util.Iterator;
import java.util.List;

import business.bo.ContrattoInCorso;
import business.bo.SceltaOptional;
import business.bo.Vettura;
import business.to.SceltaOptionalTO;
import business.to.VetturaTO;
import integration.config.ConfigKeysSpec;
import integration.config.ConfigReader;
import integration.factories.DaoFactory;
import integration.interfaces.DaoContrattoInCorso;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Realizza il caso d'uso relativo al calcolo del saldo di un contratto.
 * Implementa l'interfaccia {@link AppServiceInt} ed è realizzata
 * seguendo il design pattern Command
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class CalcolaSaldo implements AppServiceInt{

	private String chilEffettivi;
	private String idCic;
	private String acconto;
	private LocalDate dataCF;

	@Override
	public Object execute() {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();
		ContrattoInCorso CiC = new ContrattoInCorso(daoCiC.getContratto(Integer.valueOf(idCic)));

		AppServiceFactory serviceFactory = null;
		serviceFactory = AppServiceFactory.getInstance();

		double saldoI = daoCiC.getContratto(Integer.valueOf(idCic)).getSaldoIniziale();
		ConfigReader reader = new ConfigReader(ConfigKeysSpec.getPath());

		long intervallo = ChronoUnit.DAYS.between(daoCiC.getContratto(Integer.valueOf(idCic)).getPrevisioneConsegna(), dataCF);

		if(intervallo!=0)
			saldoI = saldoI + (CiC.getCostoTariffaTemporale() * intervallo);

		if(CiC.getTariffaChilometraggio().getNome().equals(reader.getProperty(ConfigKeysSpec.getTarlim())))
			saldoI = saldoI + (CiC.getCostoTariffaChilometraggio() * Double.valueOf(chilEffettivi));

		if(acconto.isEmpty())
			saldoI = saldoI - 0;
		else{
			double s = Double.valueOf(acconto);
			saldoI = saldoI - s;
		}

		return saldoI;
	}

	@Override
	public void init(List<Object> parameters) {
		this.idCic = (String) parameters.get(0);
		this.chilEffettivi = (String) parameters.get(1);
		this.acconto = (String) parameters.get(2);
		this.dataCF = (LocalDate) parameters.get(3);
	}

}
