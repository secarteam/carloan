package business.appservice;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import business.OperazioneNonCompletata;

/**
 * Implementa il pattern Singleton e viene usata per risolvere il mapping
 * fra chiavi che identificano un servizio e gli application service che li implementano,
 * restituendo la corretta istanza di Application Service.
 * Viene anche utilizzata per restituire il path della view in fxml associato ad un
 * determinato servizio.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class AppServiceFactory {
	
	private final static String PATH = "/integration/config/servicemapping.xml";
	private static Map<String, ServiceProperties> serviceMap;
	
	private static AppServiceFactory factory;
	
	/**
	 * Permette di ottenere un'istanza di {@link AppServiceFactory}.
	 * Viene restituita l'istanza esistente se è già stata precedentemente creata
	 * o ne crea al momento una nuova (pattern Singleton).
	 * @return {@link AppServiceFactory}
	 */
	public static AppServiceFactory getInstance()
	{
		if(factory == null)
		{
			factory = new AppServiceFactory();
		}
		
		return factory;
	}

	/**
	 * Costruttore privato, che viene usato da {@link #getInstance()}}
	 */
	private AppServiceFactory() {
		
		try {
			loadMap();
		} catch (OperazioneNonCompletata e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Carica i dati presenti in servicemapping.xml in un oggetto di tipo Map.
	 * Viene usata dal costruttore di AppServiceFactory, in questo modo
	 * il file xml viene parserizzato completamente quando viene creata l'istanza di
	 * AppServiceFactory.
	 * È quindi possibile ottenere i servizi richiesti evitando di parserizzare ogni volta
	 * il file xml, eseguendo nuovamente righe di codice di parsing e rallentando
	 * il sistema a causa di operazioni di I/O
	 * @throws OperazioneNonCompletata
	 * 
	 */
	private static void loadMap() throws OperazioneNonCompletata
	{	
		InputStream serviceFile = AppServiceFactory.class.getResourceAsStream(PATH);
		
		try
		{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(serviceFile);
			
			NodeList nList = doc.getElementsByTagName("service");
			
			serviceMap = new HashMap<String, ServiceProperties>();
			
			for(int i = 0; i < nList.getLength(); i++)
			{
				Node node = nList.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{	
					Element element = (Element) node;
					
					ServiceProperties sp = new ServiceProperties();
					sp.setServiceImpl(Class.forName(element.getElementsByTagName("class").item(0).getTextContent()));
					
					Node child;
					child = element.getElementsByTagName("view").item(0);
					
					if(!child.getTextContent().isEmpty())
					{
						sp.setView(child.getTextContent());
					}
					
					serviceMap.put(element.getAttribute("name"), sp);
				}
			}
			
			serviceFile.close();
			
		}
		catch(ParserConfigurationException | SAXException | IOException | ClassNotFoundException | DOMException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		
	}
	
	/**
	 * Permette di ottenere lo specifico Application Service associato alla chiave
	 * identificata dal parametro fornito come input.
	 * @param name	chiave del servizio richiesto
	 * @return {@link AppServiceInt} Application Service richiesto, che implementa
	 * l'interfaccia AppServiceInt
	 * @throws OperazioneNonCompletata
	 */
	public AppServiceInt getService(String name) throws OperazioneNonCompletata
	{	
		try {
			return (AppServiceInt) serviceMap.get(name).getServiceImpl().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			throw new OperazioneNonCompletata(e);
		}
	}
	
	/**
	 * Permette di ottenere il path della view fxml associata ad una determinata
	 * funzionalità offerta.
	 * @param name	chiave del servizio
	 * @return	stringa che rappresenta il path del file .fxml di una view
	 */
	public String getView(String name)
	{
		return serviceMap.get(name).getView();
	}
	
	/**
	 * Restituisce una istanza di una classe che implementa {@link CryptoInt} e la cui
	 * classe è associata ad un servizio identificato dalla chiave fornita come parametro
	 * @param name	nome del servizio di crittografia richiesto
	 * @return	istanza di una classe che implementa {@link CryptoInt}
	 * @throws OperazioneNonCompletata
	 */
	public CryptoInt getEncryptionService(String name) throws OperazioneNonCompletata
	{
		try {
			return (CryptoInt) serviceMap.get(name).getServiceImpl().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			throw new OperazioneNonCompletata(e);
		}
	}
	

}
