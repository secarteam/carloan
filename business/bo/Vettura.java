package business.bo;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoVettura;
import business.OperazioneNonCompletata;
import business.to.ContrattoInCorsoTO;
import business.to.ManutenzioneType;
import business.to.VetturaTO;

public class Vettura extends VetturaTO {

	public Vettura() {
		// TODO Auto-generated constructor stub
	}

	public Vettura(String targa, String casaProduttrice, Modello modello, ManutenzioneType isInManutenzione, int agenzia, double km) {
		super(targa, casaProduttrice, modello, isInManutenzione, agenzia, km);
		// TODO Auto-generated constructor stub
	}

	public Vettura(VetturaTO vetturaTO) {
		super(vetturaTO);
		// TODO Auto-generated constructor stub
	}

	public boolean isVetturaInContrattoInCorso() throws OperazioneNonCompletata{
	boolean isPresent = false;

			try
			{
				DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
				DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();
				List<VetturaTO> listVet = new LinkedList<VetturaTO>();

				listVet = daoCiC.getVettureInContratto();

				Iterator<VetturaTO> vetIter = listVet.iterator();

				while(vetIter.hasNext() && isPresent == false)
				{
					VetturaTO v = vetIter.next();

					if(this.getTarga().equals(v.getTarga()))
						isPresent = true;
				}

			}
			catch(DaoException e)
			{
				throw new OperazioneNonCompletata(e);
			}

			return isPresent;
	}

	public boolean isVetturaInStorage() throws OperazioneNonCompletata
	{
		boolean isPresent = false;

		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoVettura daoVettura = daoFactory.getDaoVettura();

			daoVettura.getVettura(this.getTarga());

			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}

		return isPresent;

	}

}
