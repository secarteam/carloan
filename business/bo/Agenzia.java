package business.bo;

import java.util.Iterator;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.AccountTO;
import business.to.AgenziaTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;

public class Agenzia extends AgenziaTO {

	public Agenzia(int id, String nome, String provincia, String città, String via, String numCivico, List<Lavoratore> lavoratori) {
		super(id, nome, provincia, città, via, numCivico, lavoratori);
		// TODO Auto-generated constructor stub
	}

	public Agenzia() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Agenzia(AgenziaTO agenzia) {
		super(agenzia);
		// TODO Auto-generated constructor stub
	}
	
	public void aggiungiLavoratore(Lavoratore lavoratore) throws OperazioneNonCompletata
	{
		List<AccountTO> listAccountTO = getLavoratoriPersonalData();
		
		AccountTO newWorker = new AccountTO(lavoratore.getUsername(), lavoratore.getEmail());
		
		if(!listAccountTO.contains(newWorker))
		{
			lavoratore.setAzienda(this.getId());
			lavoratori.add(lavoratore);
		}
		else
		{
			throw new OperazioneNonCompletata("Lavoratore già presente nell'agenzia");
		}
		
		/*TODO: ricordare che ci deve essere un Application Service che implementa il caso d'uso
		 * relativo all'aggiunta di operatori d'azienda e capi d'agenzia.
		 * Quest'ultimo deve richiamare la funzione qui definita e a sua volta occuparsi delle operazioni
		 * per rendere i dati persistenti nello storage.
		 */
	}
	
	
	public void deleteLavoratore(Lavoratore lavoratore){
			boolean isPresent = false;
			Iterator<Lavoratore> lavIter = lavoratori.iterator();
			
			for(Iterator<Lavoratore> iterLav = lavoratori.iterator(); iterLav.hasNext() && isPresent == false;)
			{
				Lavoratore worker = iterLav.next();
				
				if(worker.getUsername().equals(lavoratore.getUsername()))
				{
					isPresent = true;
					iterLav.remove();
				}
			}
	}
	
	public boolean isAgenziaInStorageByName() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoAgenzia daoAgenzia = daoFactory.getDaoAgenzia();
			
			daoAgenzia.getAgenzia(this.getNome());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
}
