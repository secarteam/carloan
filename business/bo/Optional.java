package business.bo;

import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoContrattoInCorso;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.ContrattoInCorsoTO;
import business.to.OptionalTO;

public class Optional extends OptionalTO {

	public Optional() {
		// TODO Auto-generated constructor stub
	}

	public Optional(int id, String nome, String descrizione, int quantitaDisponibile, double costo) {
		super(id, nome, descrizione, quantitaDisponibile, costo);
		// TODO Auto-generated constructor stub
	}

	public Optional(OptionalTO optionalTO) {
		super(optionalTO);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isOptionalInContrattoInCorso() throws OperazioneNonCompletata{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();
			List<SceltaOptional> listOpt = new LinkedList<SceltaOptional>();
			
			listOpt = daoCiC.getListaOptionalScelti();
			
			Iterator<SceltaOptional> optIter = listOpt.iterator();
			
			while(optIter.hasNext() && isPresent == false)
			{
				SceltaOptional opt = optIter.next();
				
				if(this.getId() == opt.getOptional().getId())
					isPresent = true;
			}
			
			return isPresent;
			
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		
	}

}
