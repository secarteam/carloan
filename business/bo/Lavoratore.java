package business.bo;

import business.OperazioneNonCompletata;
import business.to.LavoratoreRuolo;
import business.to.LavoratoreTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoLavoratore;

public class Lavoratore extends LavoratoreTO {

	public Lavoratore() {
		// TODO Auto-generated constructor stub
	}

	public Lavoratore(String username, String email, String nome, String cognome, int azienda,
			LavoratoreRuolo ruolo) {
		super(username, email, nome, cognome, azienda, ruolo);
		// TODO Auto-generated constructor stub
	}

	public Lavoratore(LavoratoreTO pers) {
		super(pers);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isLavoratoreInStorageByUsername() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoLavoratore daoLavoratore = daoFactory.getDaoLavoratore();
			
			daoLavoratore.getLavoratore(this.getUsername());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}

}
