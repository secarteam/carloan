package business.bo;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.TariffaChilometraggioTO;
import business.to.VetturaTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoChilFascia;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;

public class TariffaChilometraggio extends TariffaChilometraggioTO {

	public TariffaChilometraggio() {
		// TODO Auto-generated constructor stub
	}

	public TariffaChilometraggio(TariffaChilometraggioTO TariffaChilometraggioTO) {
		super(TariffaChilometraggioTO);
		// TODO Auto-generated constructor stub
	}

	public TariffaChilometraggio(int id, String nome, String descrizione) {
		super(id, nome, descrizione);
		// TODO Auto-generated constructor stub
	}

	public boolean isTariffaChilPresente() throws OperazioneNonCompletata{
		boolean isPresent = false;

				try
				{
					DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
					DaoTariffaChilometraggio daoCiC = daoFactory.getDaoTariffaChilometraggio();
					List<TariffaChilometraggioTO> listVet = new LinkedList<TariffaChilometraggioTO>();

					listVet = daoCiC.getTariffeChilometraggio();

					Iterator<TariffaChilometraggioTO> vetIter = listVet.iterator();

					while(vetIter.hasNext() && isPresent == false)
					{
						TariffaChilometraggioTO v = vetIter.next();

						if(this.getNome().equals(v.getNome()))
							isPresent = true;
					}

				}
				catch(DaoException e)
				{
					throw new OperazioneNonCompletata(e);
				}

				return isPresent;
		}

	public boolean isChilFasciaPresente(int idFascia, int idTC) throws OperazioneNonCompletata{

		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoChilFascia daoCiC = daoFactory.getDaoChilFascia();

			daoCiC.getChilFasciaCosto(idFascia, idTC);

			return true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e){
			return false;
		}
	}
}
