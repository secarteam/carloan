package business.bo;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.ModelloTO;
import business.to.VetturaTO;
import integration.DaoException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoModello;

public class Modello extends ModelloTO {

	public Modello() {
		// TODO Auto-generated constructor stub
	}

	public Modello(String modello, Fascia fascia) {
		super(modello, fascia);
		// TODO Auto-generated constructor stub
	}

	public Modello(ModelloTO modelloTO) {
		super(modelloTO);
		// TODO Auto-generated constructor stub
	}

	public boolean isModelloPresente() throws OperazioneNonCompletata{
		boolean isPresente = false;

		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoModello daom = daoFactory.getDaoModello();
			List<ModelloTO> listM = new LinkedList<ModelloTO>();

			listM = daom.getModelli();

			Iterator<ModelloTO> mIter = listM.iterator();

			while(mIter.hasNext() && isPresente == false)
			{
				ModelloTO m = mIter.next();

				if(this.getModello().equals(m.getModello()))
					isPresente = true;
			}

		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		return isPresente;
	}

}
