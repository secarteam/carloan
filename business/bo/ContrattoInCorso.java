package business.bo;

import java.time.LocalDate;
import java.util.List;

import business.to.ContrattoInCorsoTO;
import business.to.ContrattoScelteTariffeTO;

public class ContrattoInCorso extends ContrattoInCorsoTO {

	public ContrattoInCorso(int id, Vettura vettura, Cliente cliente, LocalDate dataInizio,
			LocalDate previsioneConsegna, double saldoIniziale, double acconto, List<SceltaOptional> optionalScelti,
			double costoTariffaTemporale, double costoTariffaChilometraggio,
			TariffaChilometraggio tariffaChilometraggio, TariffaTemporale tariffaTemporale, int puntoConsegna) {
		super(id, vettura, cliente, dataInizio, previsioneConsegna, saldoIniziale, acconto, optionalScelti,
				costoTariffaTemporale, costoTariffaChilometraggio, tariffaChilometraggio, tariffaTemporale, puntoConsegna);
		// TODO Auto-generated constructor stub
	}

	public ContrattoInCorso(ContrattoInCorsoTO cTO) {
		super(cTO);
		// TODO Auto-generated constructor stub
	}

	public ContrattoInCorso() {
		// TODO Auto-generated constructor stub
	}

	public ContrattoScelteTariffeTO getTariffeScelteData()
	{
		return new ContrattoScelteTariffeTO(getId(), getCostoTariffaTemporale(), getCostoTariffaChilometraggio(), getTariffaChilometraggio(), getTariffaTemporale());
	}
}
