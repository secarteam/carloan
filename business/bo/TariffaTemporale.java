package business.bo;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;

public class TariffaTemporale extends TariffaTemporaleTO {

	public TariffaTemporale() {
		// TODO Auto-generated constructor stub
	}

	public TariffaTemporale(TariffaTemporaleTO tariffaTemporaleTO) {
		super(tariffaTemporaleTO);
		// TODO Auto-generated constructor stub
	}

	public TariffaTemporale(int id, String nome, String descrizione) {
		super(id, nome, descrizione);
		// TODO Auto-generated constructor stub
	}

	public boolean isTariffaTempPresente() throws OperazioneNonCompletata{
		boolean isPresent = false;

				try
				{
					DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
					DaoTariffaTemporale daoCiC = daoFactory.getDaoTariffaTemporale();
					List<TariffaTemporaleTO> listVet = new LinkedList<TariffaTemporaleTO>();

					listVet = daoCiC.getTariffeTemporali();

					Iterator<TariffaTemporaleTO> vetIter = listVet.iterator();

					while(vetIter.hasNext() && isPresent == false)
					{
						TariffaTemporaleTO v = vetIter.next();

						if(this.getNome().equals(v.getNome()))
							isPresent = true;
					}

				}
				catch(DaoException e)
				{
					throw new OperazioneNonCompletata(e);
				}

				return isPresent;
		}


	public boolean isTempFasciaPresente(int idFascia, int idTT) throws OperazioneNonCompletata{

		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoTempFascia daoCiC = daoFactory.getDaoTempFascia();

			daoCiC.getTempFasciaCosto(idFascia, idTT);

			return true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e){
			return false;
		}
	}
}
