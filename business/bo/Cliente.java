package business.bo;

import java.util.LinkedList;
import java.util.List;

import business.OperazioneNonCompletata;
import business.to.ClienteTO;
import business.to.ContrattoInCorsoTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoCliente;
import integration.interfaces.DaoContrattoInCorso;

public class Cliente extends ClienteTO {

	public Cliente() {
		// TODO Auto-generated constructor stub
	}

	public Cliente(String username, String email, String nome, String cognome, String numTelefono,
			String codiceFiscale) {
		super(username, email, nome, cognome, numTelefono, codiceFiscale);
		// TODO Auto-generated constructor stub
	}

	public Cliente(ClienteTO cliente) {
		super(cliente);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isClienteInStorageByUsername() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoCliente daoCliente = daoFactory.getDaoCliente();
			
			daoCliente.getCliente(this.getUsername());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
	
	public boolean isClienteInStorageByEmail() throws OperazioneNonCompletata
	{
		boolean isPresent = false;
		
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoCliente daoCliente = daoFactory.getDaoCliente();
			
			daoCliente.getCliente(this.getEmail());
			
			isPresent = true;
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		catch(NoEntryException e)
		{
			isPresent = false;
		}
		
		return isPresent;
	}
	
	public List<ContrattoInCorso> getContrattiCliente() throws OperazioneNonCompletata
	{
		try
		{
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoContrattoInCorso contratto = daoFactory.getDaoContratto();
			
			List<ContrattoInCorsoTO> lTO = contratto.getContratti(this);
			
			List<ContrattoInCorso> listaContratti = new LinkedList<ContrattoInCorso>();
			
			for(ContrattoInCorsoTO cTO : lTO)
				listaContratti.add(new ContrattoInCorso(cTO));
			
			return listaContratti;
			
		}
		catch(DaoException e)
		{
			throw new OperazioneNonCompletata(e);
		}
		
	}
}
