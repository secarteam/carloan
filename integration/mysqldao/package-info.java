/**
 * Contiene i DAO che accedono a database di tipo MySQL
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */
package integration.mysqldao;