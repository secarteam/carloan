package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.to.TariffaChilometraggioTO;
import integration.DaoException;
import integration.factories.MysqlDaoFactory;

public class MysqlDaoTariffaChilometraggio implements integration.interfaces.DaoTariffaChilometraggio {

	public MysqlDaoTariffaChilometraggio() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public TariffaChilometraggioTO getTariffaChilometraggio(int id) throws DaoException {

		TariffaChilometraggioTO tfc = null;
		Connection con = null;
		String query = "SELECT * from TariffeChilometraggio where idTariffaChilometraggio = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, id);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				tfc = new TariffaChilometraggioTO();
				tfc.setId(results.getInt("idTariffaChilometraggio"));
				tfc.setNome(results.getString("nome"));
				tfc.setDescrizione(results.getString("descrizione"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return tfc;
	}

   public List<TariffaChilometraggioTO> getTariffeChilometraggio() throws DaoException {

		List<TariffaChilometraggioTO> tfc = new LinkedList<TariffaChilometraggioTO>();
		Connection con = null;
		String query = "SELECT * from TariffeChilometraggio;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				TariffaChilometraggioTO tfc1 = new TariffaChilometraggioTO();
				tfc1.setId(results.getInt("idTariffaChilometraggio"));
				tfc1.setNome(results.getString("nome"));
				tfc1.setDescrizione(results.getString("descrizione"));
				tfc.add(tfc1);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return tfc;
	}

	@Override
	public TariffaChilometraggioTO getTariffaChilometraggio(String nome) throws DaoException {

		TariffaChilometraggioTO tfc = null;
		Connection con = null;
		String query = "SELECT * from TariffeChilometraggio where nome = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, nome);;

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				tfc = new TariffaChilometraggioTO();
				tfc.setId(results.getInt("idTariffaChilometraggio"));
				tfc.setNome(results.getString("nome"));
				tfc.setDescrizione(results.getString("descrizione"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return tfc;

	}

	@Override
	public void setTariffaChilometraggio(TariffaChilometraggioTO tariffaChilometraggio) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO TariffeChilometraggio (nome, descrizione) VALUES (?, ?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, tariffaChilometraggio.getNome());
			statement.setString(2, tariffaChilometraggio.getDescrizione());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateTariffaChilometraggio(TariffaChilometraggioTO tariffaChilometraggio) throws DaoException {

		Connection con = null;
		String query = "UPDATE TariffeChilometraggio SET nome = ?, descrizione = ?  WHERE idTariffaChilometraggio = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, tariffaChilometraggio.getNome());
			statement.setString(2, tariffaChilometraggio.getDescrizione());
			statement.setInt(3, tariffaChilometraggio.getId());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteTariffaChilometraggio(TariffaChilometraggioTO tariffaChilometraggio) throws DaoException {

		Connection con = null;
		String query = createQueryString("DELETE", tariffaChilometraggio);
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	String createQueryString(String beginString, TariffaChilometraggioTO tariffaChilometraggio)
	{
		String query = beginString + " from TariffeChilometraggio where ";

		if(tariffaChilometraggio.getId() != 0)
			query += "TariffeChilometraggio.idTariffaChilometraggio = " + tariffaChilometraggio.getId() + " AND ";
		if(tariffaChilometraggio.getNome() != null)
			query += "TariffeChilometraggio.nome = '" + tariffaChilometraggio.getNome() + "' AND ";
		if(tariffaChilometraggio.getDescrizione() != null)
			query += "TariffeChilometraggio.descrizione = '" + tariffaChilometraggio.getDescrizione() + "' AND ";


		query = query.substring(0, query.length() - 5);
		query += ";";

		return query;
	}

}
