package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;
import business.to.FasciaTO;
import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.DaoException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoFascia;

public class MysqlDaoFascia implements DaoFascia {

	public MysqlDaoFascia() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public FasciaTO getFascia(int id) throws DaoException {

		FasciaTO fsc = null;
		Connection con = null;
		String query = "SELECT * from Fascia where idFascia = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, id);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				fsc = new FasciaTO();
				fsc.setId(results.getInt("idFascia"));
				fsc.setNome(results.getString("nome"));
				fsc.setDescrizione("descrizione");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		if(fsc != null)
		{
			try
			{
				retrieveAndSetTariffaTemporaleAndCosti(fsc);
				retrieveAndSetTariffaChilometraggioAndCosti(fsc);
			}
			catch(DaoException e)
			{
				throw new DaoException(e);
			}
		}


		return fsc;
	}

	@Override
	public FasciaTO getFascia(String nome) throws DaoException {


		FasciaTO fsc = null;
		Connection con = null;
		String query = "SELECT * from Fascia where nome = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, nome);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				fsc = new FasciaTO();
				fsc.setId(results.getInt("idFascia"));
				fsc.setNome(results.getString("nome"));
				fsc.setDescrizione("descrizione");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		if(fsc != null)
		{
			try
			{
				retrieveAndSetTariffaTemporaleAndCosti(fsc);
				retrieveAndSetTariffaChilometraggioAndCosti(fsc);
			}
			catch(DaoException e)
			{
				throw new DaoException(e);
			}
		}


		return fsc;


	}

	@Override
	public List<FasciaTO> getFasce() throws DaoException {

		Connection con = null;
		//String query = "SELECT idFascia from Fascia;";
		String query = "SELECT * from Fascia;";
		PreparedStatement statement = null;
		List<FasciaTO> fasce = new LinkedList<FasciaTO>();

		try
		{
			con = MysqlDaoFactory.getConnection();

			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				FasciaTO fasciaTO = new FasciaTO();
				fasciaTO.setId(results.getInt("idFascia"));
				fasciaTO.setNome(results.getString("nome"));
				fasciaTO.setDescrizione("descrizione");
				fasce.add(fasciaTO);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return fasce;

	}

	@Override
	public void setFascia(FasciaTO fascia) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO Fascia (nome, descrizione) VALUES (?, ?);";
		PreparedStatement statement = null;
		PreparedStatement statement2 = null;
		PreparedStatement statement3 = null;
		int rowCount = 0;

		try
		{
			con = MysqlDaoFactory.getConnection();

			statement = con.prepareStatement(query);

			statement.setString(1, fascia.getNome());
			statement.setString(2, fascia.getDescrizione());

			rowCount = statement.executeUpdate();

			if(rowCount != 0)
			{
//				MysqlDaoTariffaTemporale daoTemp = new MysqlDaoTariffaTemporale();
//				MysqlDaoTariffaChilometraggio daoChil = new MysqlDaoTariffaChilometraggio();
//
//				daoTemp.setTariffaTemporale(fascia.getTariffaTemporale().getData());
//				daoChil.setTariffaChilometraggio(fascia.getTariffaChilometraggio().getData());

				FasciaTO insFascia = getFascia(fascia.getNome());

				query = "INSERT INTO TempFascia VALUES (?, ?, ?);";

				statement2 = con.prepareStatement(query);

				statement2.setInt(1, insFascia.getId());
				statement2.setInt(2, fascia.getTariffaTemporale().getId());
				statement2.setDouble(3, fascia.getTariffaTemporaleCosto());

				statement2.executeUpdate();

				query = "INSERT INTO ChilFascia VALUES (?, ?, ?);";

				statement3 = con.prepareStatement(query);

				statement3.setInt(1, insFascia.getId());
				statement3.setInt(2, fascia.getTariffaChilometraggio().getId());
				statement3.setDouble(3, fascia.getTariffaChilometraggioCosto());

				statement3.executeUpdate();
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					statement2.close();
					statement3.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateFascia(FasciaTO fascia) throws DaoException {

		Connection con = null;
		String query = "UPDATE Fascia SET nome = ?, descrizione = ?  WHERE idFascia = ?;";
		PreparedStatement statement = null;
		PreparedStatement statement2 = null;
		PreparedStatement statement3 = null;

		try
		{
			con = MysqlDaoFactory.getConnection();

			statement = con.prepareStatement(query);

			statement.setString(1, fascia.getNome());
			statement.setString(2, fascia.getDescrizione());
			statement.setInt(3, fascia.getId());

			statement.executeUpdate();

			query = "UPDATE TempFascia SET idTariffaTemporale = ?, costo = ? WHERE idFascia = ?;";

			statement2 = con.prepareStatement(query);

			statement2.setInt(1, fascia.getTariffaTemporale().getId());
			statement2.setDouble(2, fascia.getTariffaTemporaleCosto());
			statement2.setInt(3, fascia.getId());

			statement2.executeUpdate();

			query = "UPDATE ChilFascia SET idTariffaChilometraggio = ?, costo = ? WHERE idFascia = ?;";

			statement3 = con.prepareStatement(query);

			statement3.setInt(1, fascia.getTariffaChilometraggio().getId());
			statement3.setDouble(2, fascia.getTariffaChilometraggioCosto());
			statement3.setInt(3, fascia.getId());

			statement3.executeUpdate();

		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					statement2.close();
					statement3.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteFascia(FasciaTO fascia) throws DaoException {

		Connection con = null;
		String query = createQueryString("DELETE", fascia);
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();

			statement = con.prepareStatement(query);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	String createQueryString(String beginString, FasciaTO fascia)
	{

		String query = beginString + " from Fascia where ";

		if(fascia.getId() != 0)
			query += "Fascia.idFascia = " + fascia.getId() + " AND ";
		if(fascia.getNome() != null)
			query += "Fascia.nome = '" + fascia.getNome() + "' AND ";
		if(fascia.getDescrizione() != null)
			query += "Fascia.descrizione = '" + fascia.getDescrizione() + "' AND ";

		query = query.substring(0, query.length() - 5);
		query += ";";

		return query;
	}

	private TariffaChilometraggio getTariffaChilometraggio(int id) throws DaoException
	{
		MysqlDaoTariffaChilometraggio daoChil = new MysqlDaoTariffaChilometraggio();

		TariffaChilometraggio tariffaChil = null;

		try
		{
			tariffaChil = new TariffaChilometraggio(daoChil.getTariffaChilometraggio(id));
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}

		return tariffaChil;
	}

	private TariffaTemporale getTariffaTemporale(int id) throws DaoException
	{
		MysqlDaoTariffaTemporale daoTemp = new MysqlDaoTariffaTemporale();

		TariffaTemporale tariffaTemp = null;

		try
		{
			tariffaTemp = new TariffaTemporale(daoTemp.getTariffaTemporale(id));
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}

		return tariffaTemp;
	}

	private void retrieveAndSetTariffaTemporaleAndCosti(FasciaTO fsc) throws DaoException
	{
		Connection con = null;
		String query = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();

			query = "SELECT * FROM TempFascia WHERE idFascia = " + fsc.getId() + ";";

			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				TariffaTemporale tariffaTemp = getTariffaTemporale(results.getInt("idTariffaTemporale"));

				fsc.setTariffaTemporale(tariffaTemp);
				fsc.setTariffaTemporaleCosto(results.getDouble("costo"));
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	private void retrieveAndSetTariffaChilometraggioAndCosti(FasciaTO fsc) throws DaoException
	{
		Connection con = null;
		String query = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();

			query = "SELECT * FROM ChilFascia WHERE idFascia = " + fsc.getId() + ";";

			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				TariffaChilometraggio tariffaChil = getTariffaChilometraggio(results.getInt("idTariffaChilometraggio"));

				fsc.setTariffaChilometraggio(tariffaChil);
				fsc.setTariffaChilometraggioCosto(results.getDouble("costo"));
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	@Override
	public List<TariffaTemporaleTO> getTariffeTemporaliByFasciaName(String nome) throws DaoException {
		FasciaTO fasciaTO = getFascia(nome);

		int fasciaID = fasciaTO.getId();

		List<TariffaTemporaleTO> list = new ArrayList<TariffaTemporaleTO>();

		Connection con = null;
		String query = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();

			query = "SELECT * from TempFascia INNER JOIN TariffeTemporali "
					+ "ON TempFascia.idTariffaTemporale=TariffeTemporali.idTariffaTemporale "
					+ "WHERE idFascia= ?";

			statement = con.prepareStatement(query);
			statement.setInt(1, fasciaID);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				TariffaTemporale tarChil = new TariffaTemporale();
				tarChil.setId(results.getInt("idTariffaTemporale"));
				tarChil.setNome(results.getString("nome"));
				tarChil.setDescrizione(results.getString("descrizione"));

				list.add(tarChil.getData());
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return list;
	}

	@Override
	public List<TariffaChilometraggioTO> getTariffeChilometraggioByFasciaName(String nome) throws DaoException {

		FasciaTO fasciaTO = getFascia(nome);

		int fasciaID = fasciaTO.getId();

		List<TariffaChilometraggioTO> list = new ArrayList<TariffaChilometraggioTO>();

		Connection con = null;
		String query = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();

			query = "SELECT * from ChilFascia INNER JOIN TariffeChilometraggio "
					+ "ON ChilFascia.idTariffaChilometraggio=TariffeChilometraggio.idTariffaChilometraggio "
					+ "WHERE idFascia= ?";

			statement = con.prepareStatement(query);
			statement.setInt(1, fasciaID);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				TariffaChilometraggio tarChil = new TariffaChilometraggio();
				tarChil.setId(results.getInt("idTariffaChilometraggio"));
				tarChil.setNome(results.getString("nome"));
				tarChil.setDescrizione(results.getString("descrizione"));

				list.add(tarChil.getData());
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return list;
	}


}
