package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.bo.Lavoratore;
import business.to.AgenziaTO;
import business.to.LavoratoreTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoAgenzia;

public class MysqlDaoAgenzia implements DaoAgenzia {

	public MysqlDaoAgenzia() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public AgenziaTO getAgenzia(String nome) throws DaoException, NoEntryException {
		
		AgenziaTO agenziaTO = null;
		Connection con = null;
		String query = "SELECT * from Agenzia where nome = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, nome);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				agenziaTO = new AgenziaTO();
				agenziaTO.setId(results.getInt("idAgenzia"));
				agenziaTO.setNome(results.getString("nome"));
				agenziaTO.setProvincia(results.getString("provincia"));
				agenziaTO.setCitta(results.getString("città"));
				agenziaTO.setVia(results.getString("via"));
				agenziaTO.setNumCivico(results.getString("num_civico"));
				
				List<Lavoratore> listLav = null;
				List<LavoratoreTO> listLavTO = getLavoratori(agenziaTO);
				
				if(listLavTO != null)
				{
					listLav = new LinkedList<Lavoratore>();
					
					for(LavoratoreTO lTO : listLavTO)
					{
						listLav.add(new Lavoratore(lTO));
					}
				}
				
				agenziaTO.setLavoratori(listLav);
			}
			else
				throw new NoEntryException("Non ci sono agenzie con il nome indicato");
			
		}
		catch(SQLException | DaoException e )
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return agenziaTO;
	}

	@Override
	public AgenziaTO getAgenzia(int id) throws DaoException {
		AgenziaTO agenziaTO = null;
		Connection con = null;
		String query = "SELECT * from Agenzia where idAgenzia = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, id);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				agenziaTO = new AgenziaTO();
				agenziaTO.setId(results.getInt("idAgenzia"));
				agenziaTO.setNome(results.getString("nome"));
				agenziaTO.setProvincia(results.getString("provincia"));
				agenziaTO.setCitta(results.getString("città"));
				agenziaTO.setVia(results.getString("via"));
				agenziaTO.setNumCivico(results.getString("num_civico"));
				
				List<Lavoratore> listLav = new LinkedList<Lavoratore>();
				List<LavoratoreTO> listLavTO = getLavoratori(agenziaTO);
				
				if(!listLavTO.isEmpty())
				{
					
					for(LavoratoreTO lTO : listLavTO)
					{
						listLav.add(new Lavoratore(lTO));
					}
				}
				
				agenziaTO.setLavoratori(listLav);
			}
			
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return agenziaTO;
	}

	@Override
	public List<AgenziaTO> getAgenzie(AgenziaTO agenzia) throws DaoException {
		
		List<AgenziaTO> agenzieTO = new LinkedList<AgenziaTO>();
		Connection con = null;
		String query = createQueryString("SELECT *", agenzia);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{	
				AgenziaTO agenziaTO = new AgenziaTO();
				agenziaTO.setId(results.getInt("idAgenzia"));
				agenziaTO.setNome(results.getString("nome"));
				agenziaTO.setProvincia(results.getString("provincia"));
				agenziaTO.setCitta(results.getString("città"));
				agenziaTO.setVia(results.getString("via"));
				agenziaTO.setNumCivico(results.getString("num_civico"));
				
				List<Lavoratore> listLav = new LinkedList<Lavoratore>();
				List<LavoratoreTO> listLavTO = getLavoratori(agenziaTO);
				
				if(!listLavTO.isEmpty())
				{
					
					for(LavoratoreTO lTO : listLavTO)
					{
						listLav.add(new Lavoratore(lTO));
					}
				}
				
				agenziaTO.setLavoratori(listLav);
				
				agenzieTO.add(agenziaTO);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return agenzieTO;
	}

	@Override
	public void setAgenzia(AgenziaTO agenzia) throws DaoException {
		
		Connection con = null;
		String query = "INSERT INTO Agenzia (nome, provincia, città, via, num_civico) VALUES  (?, ?, ?, ?, ?)";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, agenzia.getNome());
			statement.setString(2, agenzia.getProvincia());
			statement.setString(3, agenzia.getCitta());
			statement.setString(4, agenzia.getVia());
			statement.setString(5, agenzia.getNumCivico());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public List<AgenziaTO> getAgenzie() throws DaoException {
		List<AgenziaTO> agenzieTO = new LinkedList<AgenziaTO>();;
		Connection con = null;
		String query = "SELECT * from Agenzia;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
			
				AgenziaTO agenziaTO = new AgenziaTO();
				agenziaTO.setId(results.getInt("idAgenzia"));
				agenziaTO.setNome(results.getString("nome"));
				agenziaTO.setProvincia(results.getString("provincia"));
				agenziaTO.setCitta(results.getString("città"));
				agenziaTO.setVia(results.getString("via"));
				agenziaTO.setNumCivico(results.getString("num_civico"));
				
				List<Lavoratore> listLav = new LinkedList<Lavoratore>();
				List<LavoratoreTO> listLavTO = getLavoratori(agenziaTO);
				
				if(!listLavTO.isEmpty())
				{
					for(LavoratoreTO lTO : listLavTO)
					{
						listLav.add(new Lavoratore(lTO));
					}
				}
				
				agenziaTO.setLavoratori(listLav);
				
				agenzieTO.add(agenziaTO);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return agenzieTO;
	}

	@Override
	public List<LavoratoreTO> getLavoratori(AgenziaTO agenzia) throws DaoException {
		
		LavoratoreTO lavoratoreTO = new LavoratoreTO();
		lavoratoreTO.setAzienda(agenzia.getId());
		
		MysqlDaoLavoratore mysqlDaoLav = new MysqlDaoLavoratore();
		List<LavoratoreTO> listLavoratoriTO = null;
		
		try
		{
			listLavoratoriTO = mysqlDaoLav.getLavoratori(lavoratoreTO);
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}
		
		return listLavoratoriTO;
		
	}

	@Override
	public void deleteAgenzia(AgenziaTO agenzia) throws DaoException {
		
		Connection con = null;
		String query = createQueryString("DELETE", agenzia);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateAgenzia(AgenziaTO agenzia) throws DaoException {
		
		Connection con = null;
		String query = "UPDATE Agenzia SET nome = ?, provincia = ?, città = ?, via = ?, num_civico = ? WHERE idAgenzia = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, agenzia.getNome());
			statement.setString(2, agenzia.getProvincia());
			statement.setString(3, agenzia.getCitta());
			statement.setString(4, agenzia.getVia());
			statement.setString(5, agenzia.getNumCivico());
			statement.setInt(6, agenzia.getId());
			
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}
	
	private String createQueryString(String beginString, AgenziaTO agenzia)
	{
		String query = beginString + " from Agenzia where ";
		
		if(agenzia.getId() != 0)
			query += "Agenzia.idAgenzia = " + agenzia.getId() + " AND ";
		if(agenzia.getNome() != null)
			query += "Agenzia.nome = '" + agenzia.getNome() + "' AND ";
		if(agenzia.getProvincia() != null)
			query += "Agenzia.provincia = '" + agenzia.getProvincia() + "' AND ";
		if(agenzia.getCitta() != null)
			query += "Agenzia.città = '" + agenzia.getCitta() + "' AND ";
		if(agenzia.getVia() != null)
			query += "Agenzia.via = '" + agenzia.getVia() + "' AND ";
		if(agenzia.getNumCivico() != null)
			query += "Agenzia.num_civico = '" + agenzia.getNumCivico() + "' AND ";
		
		query = query.substring(0, query.length() - 5);
		query += ";";
		
		return query;
	}

}
