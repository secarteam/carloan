package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.to.OptionalTO;
import integration.DaoException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoOptional;

public class MysqlDaoOptional implements DaoOptional {

	public MysqlDaoOptional() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public OptionalTO getOptional(int id) throws DaoException {

		OptionalTO optional = null;
		String query = "SELECT * FROM Optional WHERE idOptional = ?;";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, id);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				optional = new OptionalTO();

				optional.setData(results.getInt("idOptional"), results.getString("nome"), results.getString("descrizione"), results.getInt("quantitàDisponibile"), results.getDouble("costo"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return optional;

	}

	@Override
	public OptionalTO getOptional(String nome) throws DaoException {


		OptionalTO optional = null;
		String query = "SELECT * FROM Optional WHERE nome = ?;";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, nome);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				optional = new OptionalTO();

				optional.setData(results.getInt("idOptional"), results.getString("nome"), results.getString("descrizione"), results.getInt("quantitàDisponibile"), results.getDouble("costo"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return optional;


	}

	@Override
	public List<OptionalTO> getOptionals(OptionalTO optional) throws DaoException {

		String query = createQueryString("SELECT *", optional);
		Connection con = null;
		PreparedStatement statement = null;
		List<OptionalTO> listOpt = new LinkedList<OptionalTO>();

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);


			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				OptionalTO optionalTO = new OptionalTO();

				optionalTO.setData(results.getInt("idOptional"), results.getString("nome"), results.getString("descrizione"), results.getInt("quantitàDisponibile"), results.getDouble("costo"));

				listOpt.add(optionalTO);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listOpt;
	}

	@Override
	public void setOptional(OptionalTO optional) throws DaoException {

		String query = "INSERT INTO Optional (nome, descrizione, quantitàDisponibile, costo) VALUES (?, ?, ?, ?)";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, optional.getNome());
			statement.setString(2, optional.getDescrizione());
			statement.setInt(3, optional.getQuantitaDisponibile());
			statement.setDouble(4, optional.getCosto());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}


	}

	@Override
	public void updateOptional(OptionalTO optional) throws DaoException {

		String query = "UPDATE Optional SET nome = ?, descrizione = ?, quantitàDisponibile = ?, costo = ? WHERE idOptional = ?";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, optional.getNome());
			statement.setString(2, optional.getDescrizione());
			statement.setInt(3, optional.getQuantitaDisponibile());
			statement.setDouble(4, optional.getCosto());
			statement.setInt(5, optional.getId());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteOptional(OptionalTO optional) throws DaoException {

		String query = createQueryString("DELETE", optional);
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);


			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	private String createQueryString(String beginString, OptionalTO opto)
	{

		String query = beginString + " from Optional where ";

		if(opto.getId() != 0)
			query += "Optional.idOptional = " + opto.getId() + " AND ";
		if(opto.getNome() != null)
			query += "Optional.nome = '" + opto.getNome() + "' AND ";
		if(opto.getDescrizione() != null)
			query += "Optional.descrizione = '" + opto.getDescrizione() + "' AND ";
		if(opto.getQuantitaDisponibile() != 0) //TODO: come gestiamo il caso in cui l'optional è presente nel DB ma la quantità è 0?
			query += "Optional.quantitàDisponibile = " + opto.getQuantitaDisponibile() + " AND ";
		if(opto.getCosto() != 0) //TODO: idem come prima
			query += "Optional.costo = " + opto.getCosto() + " AND ";


		query = query.substring(0, query.length() - 5);
		query += ";";

		return query;
	}

	@Override
	public List<OptionalTO> getOptionalCostZero() throws DaoException {

		List<OptionalTO> retList = new LinkedList<OptionalTO>();
		List<OptionalTO> opList = null;
		try
		{
			opList = getOptional();
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}

		for(OptionalTO opt : opList)
		{
			if(opt.getCosto() == 0)
				retList.add(opt);
		}

		return retList;
	}

	@Override
	public List<OptionalTO> getOptionalAvalZero() throws DaoException {

		List<OptionalTO> retList = new LinkedList<OptionalTO>();
		List<OptionalTO> opList = null;

		try
		{
			opList = getOptional();
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}

		for(OptionalTO opt : opList)
		{
			if(opt.getQuantitaDisponibile() == 0)
				retList.add(opt);
		}

		return retList;
	}

	@Override
	public List<OptionalTO> getOptional() throws DaoException {

		List<OptionalTO> optList = new LinkedList<OptionalTO>();
		String query = "SELECT * FROM Optional;";
		Connection con = null;
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);


			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				OptionalTO optional = new OptionalTO();

				optional.setId(results.getInt("idOptional"));
				optional.setNome(results.getString("nome"));
				optional.setDescrizione(results.getString("descrizione"));
				optional.setQuantitaDisponibile(results.getInt("quantitàDisponibile"));
				optional.setCosto(results.getDouble("costo"));

				optList.add(optional);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		return optList;

	}

}
