package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.DaoException;
import integration.factories.MysqlDaoFactory;

public class MysqlDaoTariffaTemporale implements integration.interfaces.DaoTariffaTemporale {

	public MysqlDaoTariffaTemporale() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public TariffaTemporaleTO getTariffaTemporale(int id) throws DaoException {

		TariffaTemporaleTO tfc = null;
		Connection con = null;
		String query = "SELECT * from TariffeTemporali where idTariffaTemporale = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, id);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				tfc = new TariffaTemporaleTO();
				tfc.setId(results.getInt("idTariffaTemporale"));
				tfc.setNome(results.getString("nome"));
				tfc.setDescrizione(results.getString("descrizione"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return tfc;

	}

	@Override
	public TariffaTemporaleTO getTariffaTemporale(String nome) throws DaoException {

		TariffaTemporaleTO tfc = null;
		Connection con = null;
		String query = "SELECT * from TariffeTemporali where nome = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, nome);;

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				tfc = new TariffaTemporaleTO();
				tfc.setId(results.getInt("idTariffaTemporale"));
				tfc.setNome(results.getString("nome"));
				tfc.setDescrizione(results.getString("descrizione"));
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return tfc;

	}

	public List<TariffaTemporaleTO> getTariffeTemporali() throws DaoException{
		List<TariffaTemporaleTO> tfc = new LinkedList<TariffaTemporaleTO>();
		Connection con = null;
		String query = "SELECT * from TariffeTemporali;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				TariffaTemporaleTO tfc1 = new TariffaTemporaleTO();
				tfc1.setId(results.getInt("idTariffaTemporale"));
				tfc1.setNome(results.getString("nome"));
				tfc1.setDescrizione(results.getString("descrizione"));
				tfc.add(tfc1);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return tfc;
	}

	@Override
	public void setTariffaTemporale(TariffaTemporaleTO TariffaTemporale) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO TariffeTemporali (nome, descrizione) VALUES (?, ?)";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, TariffaTemporale.getNome());
			statement.setString(2, TariffaTemporale.getDescrizione());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateTariffaTemporale(TariffaTemporaleTO TariffaTemporale) throws DaoException {

		Connection con = null;
		String query = "UPDATE TariffeTemporali SET nome = ?, descrizione = ?  WHERE idTariffaTemporale = ?";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, TariffaTemporale.getNome());
			statement.setString(2, TariffaTemporale.getDescrizione());
			statement.setInt(3, TariffaTemporale.getId());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteTariffaTemporale(TariffaTemporaleTO TariffaTemporale) throws DaoException {

		Connection con = null;
		String query = createQueryString("DELETE", TariffaTemporale);
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	String createQueryString(String beginString, TariffaTemporaleTO TariffaTemporale)
	{
		String query = beginString + " from TariffeTemporali where ";

		if(TariffaTemporale.getId() != 0)
			query += "TariffeTemporali.idTariffaTemporale = " + TariffaTemporale.getId() + " AND ";
		if(TariffaTemporale.getNome() != null)
			query += "TariffeTemporali.nome = '" + TariffaTemporale.getNome() + "' AND ";
		if(TariffaTemporale.getDescrizione() != null)
			query += "TariffeTemporali.descrizione = '" + TariffaTemporale.getDescrizione() + "' AND ";


		query = query.substring(0, query.length() - 5);
		query += ";";

		return query;
	}

}
