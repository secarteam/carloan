package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import business.bo.Cliente;
import business.bo.Lavoratore;
import business.bo.Optional;
import business.bo.SceltaOptional;
import business.bo.Vettura;
import business.to.ContrattoInCorsoTO;
import business.to.ContrattoScelteTariffeTO;
import business.to.VetturaTO;
import integration.DaoException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoContrattoInCorso;

public class MysqlDaoContrattoInCorso implements DaoContrattoInCorso {

	public MysqlDaoContrattoInCorso() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContrattoInCorsoTO getContratto(int id) throws DaoException {

		ContrattoInCorsoTO cont = null;
		Connection con = null;
		String query = "SELECT * FROM ContrattoInCorso WHERE idContratto = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, id);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				cont = new ContrattoInCorsoTO();

				cont.setId(results.getInt("idContratto"));
				cont.setVettura(getVettura(results.getString("targa")));
				cont.setCliente(getCliente(results.getString("usernameCliente")));
				cont.setDataInizio(results.getDate("dataInizio").toLocalDate());
				cont.setPrevisioneConsegna(results.getDate("previsioneConsegna").toLocalDate());
				cont.setSaldoIniziale(results.getDouble("saldoIniziale"));
				cont.setAcconto(results.getDouble("acconto"));
				cont.setPuntoConsegna(results.getInt("puntoConsegna"));

				ContrattoScelteTariffeTO scelte= getContrattoScelte(results.getInt("idContratto"));
				cont.setTariffaChilometraggio(scelte.getTariffaChilometraggio());
				cont.setTariffaTemporale(scelte.getTariffaTemporale());
				cont.setCostoTariffaChilometraggio(scelte.getCostoTariffaChilometraggio());
				cont.setCostoTariffaTemporale(scelte.getCostoTariffaTemporale());
				cont.setOptionalScelti(getListaOptionalScelti(results.getInt("idContratto")));

			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return cont;
	}

	@Override
	public List<ContrattoInCorsoTO> getContratti(Cliente cliente) throws DaoException {

		List<ContrattoInCorsoTO> listCont = new LinkedList<ContrattoInCorsoTO>();
		Connection con = null;
		String query = "SELECT * FROM ContrattoInCorso WHERE usernameCliente = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, cliente.getUsername());

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				ContrattoInCorsoTO cont = new ContrattoInCorsoTO();

				cont.setId(results.getInt("idContratto"));
				cont.setVettura(getVettura(results.getString("targa")));
				cont.setCliente(cliente);
				cont.setDataInizio(results.getDate("dataInizio").toLocalDate());
				cont.setPrevisioneConsegna(results.getDate("previsioneConsegna").toLocalDate());
				cont.setSaldoIniziale(results.getDouble("saldoIniziale"));
				cont.setAcconto(results.getDouble("acconto"));
				cont.setPuntoConsegna(results.getInt("puntoConsegna"));

				ContrattoScelteTariffeTO scelte= getContrattoScelte(results.getInt("idContratto"));
				cont.setTariffaChilometraggio(scelte.getTariffaChilometraggio());
				cont.setTariffaTemporale(scelte.getTariffaTemporale());
				cont.setCostoTariffaChilometraggio(scelte.getCostoTariffaChilometraggio());
				cont.setCostoTariffaTemporale(scelte.getCostoTariffaTemporale());
				cont.setOptionalScelti(getListaOptionalScelti(results.getInt("idContratto")));

				listCont.add(cont);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listCont;
	}

	@Override
	public List<ContrattoInCorsoTO> getContratti() throws DaoException {

		List<ContrattoInCorsoTO> listCont = new LinkedList<ContrattoInCorsoTO>();
		Connection con = null;
		String query = "SELECT * FROM ContrattoInCorso;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				ContrattoInCorsoTO cont = new ContrattoInCorsoTO();

				cont.setId(results.getInt("idContratto"));
				cont.setVettura(getVettura(results.getString("targa")));
				cont.setCliente(getCliente(results.getString("usernameCliente")));
				cont.setDataInizio(results.getDate("dataInizio").toLocalDate());
				cont.setPrevisioneConsegna(results.getDate("previsioneConsegna").toLocalDate());
				cont.setSaldoIniziale(results.getDouble("saldoIniziale"));
				cont.setAcconto(results.getDouble("acconto"));
				cont.setPuntoConsegna(results.getInt("puntoConsegna"));

				ContrattoScelteTariffeTO scelte= getContrattoScelte(results.getInt("idContratto"));
				cont.setTariffaChilometraggio(scelte.getTariffaChilometraggio());
				cont.setTariffaTemporale(scelte.getTariffaTemporale());
				cont.setCostoTariffaChilometraggio(scelte.getCostoTariffaChilometraggio());
				cont.setCostoTariffaTemporale(scelte.getCostoTariffaTemporale());
				cont.setOptionalScelti(getListaOptionalScelti(results.getInt("idContratto")));

				listCont.add(cont);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listCont;

	}

	@Override
	public List<VetturaTO> getVettureInContratto() throws DaoException
	{
		List<VetturaTO> listVett = new LinkedList<VetturaTO>();
		Connection con = null;
		String query = "SELECT targa FROM ContrattoInCorso;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				VetturaTO vettura = getVettura(results.getString("targa"));

				listVett.add(vettura);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listVett;
	}

	public List<ContrattoInCorsoTO> getContratticonVettureInContratto(String targa) throws DaoException
	{
		List<ContrattoInCorsoTO> listCont = new LinkedList<ContrattoInCorsoTO>();
		Connection con = null;
		String query = "SELECT * FROM ContrattoInCorso where targa = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, targa);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				ContrattoInCorsoTO cont = getContratto(results.getInt("idContratto"));

				listCont.add(cont);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listCont;
	}

	@Override
	public void setContratto(ContrattoInCorsoTO contratto) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO ContrattoInCorso ( targa, usernameCliente, dataInizio, previsioneConsegna, saldoIniziale, acconto, puntoConsegna) VALUES ( ?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, contratto.getVettura().getTarga());
			statement.setString(2, contratto.getCliente().getUsername());
			statement.setDate(3, java.sql.Date.valueOf(contratto.getDataInizio()));
			statement.setDate(4, java.sql.Date.valueOf(contratto.getPrevisioneConsegna()));
			statement.setDouble(5, contratto.getSaldoIniziale());
			statement.setDouble(6, contratto.getAcconto());
			statement.setInt(7, contratto.getPuntoConsegna());

			statement.executeUpdate();
			
			ResultSet rs = statement.getGeneratedKeys();
			int auto_id = 1;
			if(rs.next())
			{
				auto_id = rs.getInt(1);
			}
			
			contratto.setId(auto_id);

			setContrattoScelte(contratto);

			setOptionalScelti(contratto.getOptionalScelti(), contratto.getId());
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}


	}

	@Override
	public void deleteContratto(ContrattoInCorsoTO contratto) throws DaoException {

		Connection con = null;
		String query = "DELETE FROM ContrattoInCorso WHERE idContratto = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, contratto.getId());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateContratto(ContrattoInCorsoTO contratto) throws DaoException {

		Connection con = null;
		String query = "UPDATE ContrattoInCorso SET targa = ?, usernameCliente = ?, dataInizio = ?, previsioneConsegna = ?, saldoIniziale = ?, acconto = ?, puntoConsegna = ? WHERE idContratto = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, contratto.getVettura().getTarga());
			statement.setString(2, contratto.getCliente().getUsername());
			statement.setDate(3, java.sql.Date.valueOf(contratto.getDataInizio()));
			statement.setDate(4, java.sql.Date.valueOf(contratto.getPrevisioneConsegna()));
			statement.setDouble(5, contratto.getSaldoIniziale());
			statement.setDouble(6, contratto.getAcconto());
			statement.setInt(7, contratto.getPuntoConsegna());
			statement.setInt(8, contratto.getId());

			statement.executeUpdate();
			
			ResultSet rs = statement.getGeneratedKeys();
			int auto_id = 1;
			if(rs.next())
			{
				auto_id = rs.getInt(1);
			}
			
			contratto.setId(auto_id);

			updateContrattoScelte(contratto);

			updateOptionalScelti(contratto.getOptionalScelti(), contratto.getId());
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	private Vettura getVettura(String targa) throws DaoException
	{
		MysqlDaoVettura daoVettura = new MysqlDaoVettura();

		Vettura vet = null;

		try
		{
			vet = new Vettura(daoVettura.getVettura(targa));
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}

		return vet;
	}

	private Cliente getCliente(String username) throws DaoException
	{
		MysqlDaoCliente daoCliente = new MysqlDaoCliente();
		Cliente cl = null;

		try
		{
			cl = new Cliente(daoCliente.getCliente(username));
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}

		return cl;
	}

//	private Lavoratore getClienteLavoratore(String username) throws DaoException
//	{
//		MysqlDaoLavoratore daoLavoratore = new MysqlDaoLavoratore();
//		Lavoratore lav = null;
//
//		try
//		{
//			lav = new Lavoratore(daoLavoratore.getLavoratore(username));
//		}
//		catch(DaoException e)
//		{
//			throw new DaoException(e);
//		}
//
//		return lav;
//	}

	private List<SceltaOptional> getListaOptionalScelti(int contratto) throws DaoException
	{

		List<SceltaOptional> listOpt = new LinkedList<SceltaOptional>();
		Connection con = null;
		String query = "SELECT * FROM ContrattoOptional WHERE idContratto = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, contratto);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				SceltaOptional so = new SceltaOptional();

				MysqlDaoOptional daoOpt = new MysqlDaoOptional();

				Optional opt = new Optional(daoOpt.getOptional(results.getInt("idOptional")));

				so.setOptional(opt);
				so.setQuantita(results.getInt("quantità"));
				so.setCosto(results.getDouble("costo"));

				listOpt.add(so);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listOpt;

	}

	public List<SceltaOptional> getListaOptionalScelti() throws DaoException
	{

		List<SceltaOptional> listOpt = new LinkedList<SceltaOptional>();
		Connection con = null;
		String query = "SELECT * FROM ContrattoOptional;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			//statement.setInt(1, contratto);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				SceltaOptional so = new SceltaOptional();

				MysqlDaoOptional daoOpt = new MysqlDaoOptional();

				Optional opt = new Optional(daoOpt.getOptional(results.getInt("idOptional")));

				so.setOptional(opt);
				so.setQuantita(results.getInt("quantità"));
				so.setCosto(results.getDouble("costo"));

				listOpt.add(so);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listOpt;

	}


	private void setOptionalScelti(List<SceltaOptional> listOpt, int contratto) throws DaoException
	{
		Connection con = null;
		String query = "INSERT INTO ContrattoOptional VALUES (?, ?, ?, ?)";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			for(SceltaOptional so : listOpt)
			{
				statement.setInt(1, contratto);
				statement.setInt(2, so.getOptional().getId());
				statement.setInt(3, so.getQuantita());
				statement.setDouble(4, so.getCosto());

				statement.executeUpdate();
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	private void updateOptionalScelti(List<SceltaOptional> listOpt, int contratto) throws DaoException
	{
		Connection con = null;
		String query = "UPDATE ContrattoOptional SET idOptional = ?, quantità = ?, costo = ? WHERE idContratto = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			for(SceltaOptional so : listOpt)
			{
				statement.setInt(1, so.getOptional().getId());
				statement.setInt(2, so.getQuantita());
				statement.setDouble(3, so.getCosto());
				statement.setInt(4, contratto);

				statement.executeUpdate();
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}


	private ContrattoScelteTariffeTO getContrattoScelte(int id) throws DaoException
	{
		MysqlDaoContrattoScelte daoScelte = new MysqlDaoContrattoScelte();

		ContrattoScelteTariffeTO conScelte = null;

		try
		{
			conScelte = daoScelte.getContrattoScelte(id);
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}

		return conScelte;
	}

	private void setContrattoScelte(ContrattoInCorsoTO contratto) throws DaoException
	{
		MysqlDaoContrattoScelte daoScelte = new MysqlDaoContrattoScelte();

		ContrattoScelteTariffeTO conScelte = new ContrattoScelteTariffeTO();

		conScelte.setcontrattoID(contratto.getId());
		conScelte.setTariffaChilometraggio(contratto.getTariffaChilometraggio());
		conScelte.setTariffaTemporale(contratto.getTariffaTemporale());
		conScelte.setCostoTariffaChilometraggio(contratto.getCostoTariffaChilometraggio());
		conScelte.setCostoTariffaTemporale(contratto.getCostoTariffaTemporale());

		try
		{
			daoScelte.setContrattoScelte(conScelte);
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}
	}

	private void updateContrattoScelte(ContrattoInCorsoTO contratto) throws DaoException
	{
		MysqlDaoContrattoScelte daoScelte = new MysqlDaoContrattoScelte();

		ContrattoScelteTariffeTO conScelte = new ContrattoScelteTariffeTO();

		conScelte.setcontrattoID(contratto.getId());
		conScelte.setTariffaChilometraggio(contratto.getTariffaChilometraggio());
		conScelte.setTariffaTemporale(contratto.getTariffaTemporale());
		conScelte.setCostoTariffaChilometraggio(contratto.getCostoTariffaChilometraggio());
		conScelte.setCostoTariffaTemporale(contratto.getCostoTariffaTemporale());

		try
		{
			daoScelte.updateContrattoScelte(conScelte);
		}
		catch(DaoException e)
		{
			throw new DaoException(e);
		}
	}

}
