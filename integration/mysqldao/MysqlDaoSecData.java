package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoSecData;

public class MysqlDaoSecData implements DaoSecData {

	public MysqlDaoSecData() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getUserHash(String username) throws DaoException, NoEntryException {
		String hash = null;
		Connection con = null;
		String query = "SELECT salted_pwd from SecData where username = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, username);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				hash = results.getString("salted_pwd");
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return hash;
	}

	@Override
	public void setUserHash(String username, String pwd) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO SecData VALUES (?, ?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, username);
			statement.setString(2, pwd);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateUserHash(String username, String pwd) throws DaoException {
		Connection con = null;
		String query = "UPDATE SecData SET salted_pwd = ? WHERE username = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, pwd);
			statement.setString(2, username);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteUserHash(String username) throws DaoException {

		Connection con = null;
		String query = "DELETE from SecData WHERE username = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, username);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

}
