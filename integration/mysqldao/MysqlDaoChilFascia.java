package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import business.to.FasciaTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoChilFascia;

public class MysqlDaoChilFascia implements DaoChilFascia{

	public double getChilFasciaCosto(int idFascia, int idTC) throws DaoException, NoEntryException{
		Connection con = null;
		double cost = 0;
		String query = "SELECT costo from chilFascia where idFascia = ? and idtariffachilometraggio = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, idFascia);
			statement.setInt(2, idTC);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				cost = results.getDouble("costo");
			}
			else{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		return cost;
	}

	public void setChilFascia(int idFascia, int idTC, double costo) throws DaoException{
		Connection con = null;
		double cost = 0;
		String query = "insert into ChilFascia values(?,?,?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, idFascia);
			statement.setInt(2, idTC);
			statement.setDouble(3, costo);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	public void updateChilFascia(int idFascia, int idTC, double costo) throws DaoException{
		Connection con = null;
		double cost = 0;
		String query = "update ChilFascia set costo = ? where idtariffachilometraggio = ? and idfascia = ? ;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setDouble(1, costo);
			statement.setInt(2, idTC);
			statement.setInt(3, idFascia);


			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	public void deleteChilFascia(int idFascia, int idTT) throws DaoException{
		Connection con = null;
		String query = "delete from ChilFascia where idtariffaChilometraggio = ? and idfascia = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, idTT);
			statement.setInt(2, idFascia);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}
}
