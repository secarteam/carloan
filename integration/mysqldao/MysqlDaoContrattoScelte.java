package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.bo.TariffaChilometraggio;
import business.bo.TariffaTemporale;
import business.to.ContrattoScelteTariffeTO;
import integration.DaoException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoContrattoScelte;

public class MysqlDaoContrattoScelte implements DaoContrattoScelte {

	public MysqlDaoContrattoScelte() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ContrattoScelteTariffeTO getContrattoScelte(int contratto) throws DaoException {

		Connection con = null;
		ContrattoScelteTariffeTO csTO = null;
		String query = "SELECT * FROM ContrattoScelte WHERE idContratto = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setInt(1, contratto);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				csTO = new ContrattoScelteTariffeTO();
				csTO.setcontrattoID(results.getInt("idContratto"));
				csTO.setCostoTariffaChilometraggio(results.getDouble("costoTariffaChilometraggio"));
				csTO.setCostoTariffaTemporale(results.getDouble("costoTariffaTemporale"));
				
				MysqlDaoTariffaTemporale daoTemp = new MysqlDaoTariffaTemporale();
				MysqlDaoTariffaChilometraggio daoChil = new MysqlDaoTariffaChilometraggio();
				
				TariffaTemporale tarTemp = new TariffaTemporale(daoTemp.getTariffaTemporale(results.getInt("idTariffaTemporale")));
				TariffaChilometraggio tarChil = new TariffaChilometraggio(daoChil.getTariffaChilometraggio(results.getInt("idTariffaChilometraggio")));
				
				csTO.setTariffaChilometraggio(tarChil);
				csTO.setTariffaTemporale(tarTemp);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return csTO;
	}

	@Override
	public void setContrattoScelte(ContrattoScelteTariffeTO contrattoScelte) throws DaoException {
		
		Connection con = null;
		String query = "INSERT INTO ContrattoScelte VALUES (?, ?, ?, ?, ?);";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setInt(1, contrattoScelte.getcontrattoID());
			statement.setInt(2, contrattoScelte.getTariffaTemporale().getId());
			statement.setInt(3, contrattoScelte.getTariffaChilometraggio().getId());
			statement.setDouble(4, contrattoScelte.getCostoTariffaTemporale());
			statement.setDouble(5, contrattoScelte.getCostoTariffaChilometraggio());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateContrattoScelte(ContrattoScelteTariffeTO contrattoScelte) throws DaoException {
		
		Connection con = null;
		String query = "UPDATE ContrattoScelte SET idTariffaTemporale = ?, idTariffaChilometraggio = ?, costoTariffaTemporale = ?, costoTariffaChilometraggio = ? WHERE idContratto = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setInt(1, contrattoScelte.getTariffaTemporale().getId());
			statement.setInt(2, contrattoScelte.getTariffaChilometraggio().getId());
			statement.setDouble(3, contrattoScelte.getCostoTariffaTemporale());
			statement.setDouble(4, contrattoScelte.getCostoTariffaChilometraggio());
			statement.setInt(5, contrattoScelte.getcontrattoID());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteContrattoScelte(int contratto) throws DaoException {
		
		Connection con = null;
		String query = "DELETE from ContrattoScelte WHERE idContratto = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setInt(1, contratto);
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	@Override
	public List<ContrattoScelteTariffeTO> getContrattiScelteByTariffaTemporale(int temp) throws DaoException {
		
		Connection con = null;
		List<ContrattoScelteTariffeTO> listContr = new LinkedList<ContrattoScelteTariffeTO>();
		String query = "SELECT * FROM ContrattoScelte WHERE idTariffaTemporale = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setInt(1, temp);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				ContrattoScelteTariffeTO csTO = new ContrattoScelteTariffeTO();
				csTO.setcontrattoID(results.getInt("idContratto"));
				csTO.setCostoTariffaChilometraggio(results.getDouble("costoTariffaChilometraggio"));
				csTO.setCostoTariffaTemporale(results.getDouble("costoTariffaTemporale"));
				
				MysqlDaoTariffaTemporale daoTemp = new MysqlDaoTariffaTemporale();
				MysqlDaoTariffaChilometraggio daoChil = new MysqlDaoTariffaChilometraggio();
				
				TariffaTemporale tarTemp = new TariffaTemporale(daoTemp.getTariffaTemporale(results.getInt("idTariffaTemporale")));
				TariffaChilometraggio tarChil = new TariffaChilometraggio(daoChil.getTariffaChilometraggio(results.getInt("idTariffaChilometraggio")));
				
				csTO.setTariffaChilometraggio(tarChil);
				csTO.setTariffaTemporale(tarTemp);
				
				listContr.add(csTO);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return listContr;
	}

	@Override
	public List<ContrattoScelteTariffeTO> getContrattiScelteByTariffaChilometraggio(int chil) throws DaoException {
		Connection con = null;
		List<ContrattoScelteTariffeTO> listContr = new LinkedList<ContrattoScelteTariffeTO>();
		String query = "SELECT * FROM ContrattoScelte WHERE idTariffaChilometraggio = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setInt(1, chil);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				ContrattoScelteTariffeTO csTO = new ContrattoScelteTariffeTO();
				csTO.setcontrattoID(results.getInt("idContratto"));
				csTO.setCostoTariffaChilometraggio(results.getDouble("costoTariffaChilometraggio"));
				csTO.setCostoTariffaTemporale(results.getDouble("costoTariffaTemporale"));
				
				MysqlDaoTariffaTemporale daoTemp = new MysqlDaoTariffaTemporale();
				MysqlDaoTariffaChilometraggio daoChil = new MysqlDaoTariffaChilometraggio();
				
				TariffaTemporale tarTemp = new TariffaTemporale(daoTemp.getTariffaTemporale(results.getInt("idTariffaTemporale")));
				TariffaChilometraggio tarChil = new TariffaChilometraggio(daoChil.getTariffaChilometraggio(results.getInt("idTariffaChilometraggio")));
				
				csTO.setTariffaChilometraggio(tarChil);
				csTO.setTariffaTemporale(tarTemp);
				
				listContr.add(csTO);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return listContr;
	}

	@Override
	public List<ContrattoScelteTariffeTO> getContrattiScelteByCostoTariffaTemporale(double costo) throws DaoException {
		Connection con = null;
		List<ContrattoScelteTariffeTO> listContr = new LinkedList<ContrattoScelteTariffeTO>();
		String query = "SELECT * FROM ContrattoScelte WHERE costoTariffaTemporale = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setDouble(1, costo);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				ContrattoScelteTariffeTO csTO = new ContrattoScelteTariffeTO();
				csTO.setcontrattoID(results.getInt("idContratto"));
				csTO.setCostoTariffaChilometraggio(results.getDouble("costoTariffaChilometraggio"));
				csTO.setCostoTariffaTemporale(results.getDouble("costoTariffaTemporale"));
				
				MysqlDaoTariffaTemporale daoTemp = new MysqlDaoTariffaTemporale();
				MysqlDaoTariffaChilometraggio daoChil = new MysqlDaoTariffaChilometraggio();
				
				TariffaTemporale tarTemp = new TariffaTemporale(daoTemp.getTariffaTemporale(results.getInt("idTariffaTemporale")));
				TariffaChilometraggio tarChil = new TariffaChilometraggio(daoChil.getTariffaChilometraggio(results.getInt("idTariffaChilometraggio")));
				
				csTO.setTariffaChilometraggio(tarChil);
				csTO.setTariffaTemporale(tarTemp);
				
				listContr.add(csTO);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return listContr;
	}

	@Override
	public List<ContrattoScelteTariffeTO> getContrattiScelteByCostoTariffaChilometraggio(double costo) throws DaoException {
		Connection con = null;
		List<ContrattoScelteTariffeTO> listContr = new LinkedList<ContrattoScelteTariffeTO>();
		String query = "SELECT * FROM ContrattoScelte WHERE idTariffaTemporale = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setDouble(1, costo);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				ContrattoScelteTariffeTO csTO = new ContrattoScelteTariffeTO();
				csTO.setcontrattoID(results.getInt("idContratto"));
				csTO.setCostoTariffaChilometraggio(results.getDouble("costoTariffaChilometraggio"));
				csTO.setCostoTariffaTemporale(results.getDouble("costoTariffaTemporale"));
				
				MysqlDaoTariffaTemporale daoTemp = new MysqlDaoTariffaTemporale();
				MysqlDaoTariffaChilometraggio daoChil = new MysqlDaoTariffaChilometraggio();
				
				TariffaTemporale tarTemp = new TariffaTemporale(daoTemp.getTariffaTemporale(results.getInt("idTariffaTemporale")));
				TariffaChilometraggio tarChil = new TariffaChilometraggio(daoChil.getTariffaChilometraggio(results.getInt("idTariffaChilometraggio")));
				
				csTO.setTariffaChilometraggio(tarChil);
				csTO.setTariffaTemporale(tarTemp);
				
				listContr.add(csTO);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return listContr;
	}

}
