package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


import business.to.ClienteTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoCliente;

public class MysqlDaoCliente implements DaoCliente {

	public MysqlDaoCliente() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ClienteTO getCliente(String username) throws DaoException, NoEntryException {
		
		ClienteTO clienteTO = null;
		Connection con = null;
		String query = "SELECT * from Cliente where username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, username);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				clienteTO = new ClienteTO();
				clienteTO.setUsername(username);
				clienteTO.setNome(results.getString("nome"));
				clienteTO.setCognome(results.getString("cognome"));
				clienteTO.setEmail(results.getString("email"));
				clienteTO.setNumTelefono(results.getString("numtelefono"));
				clienteTO.setCodiceFiscale(results.getString("codicefiscale"));
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return clienteTO;
	}
	
	@Override
	public ClienteTO getClienteByEmail(String email) throws DaoException, NoEntryException
	{
		ClienteTO clienteTO = null;
		Connection con = null;
		String query = "SELECT * from Cliente where email = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, email);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				clienteTO = new ClienteTO();
				clienteTO.setUsername(results.getString("username"));
				clienteTO.setNome(results.getString("nome"));
				clienteTO.setCognome(results.getString("cognome"));
				clienteTO.setEmail(email);
				clienteTO.setNumTelefono(results.getString("numtelefono"));
				clienteTO.setCodiceFiscale(results.getString("codicefiscale"));
			}
			else
			{
				throw new NoEntryException("EmptyResultSet");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return clienteTO;
	}

	@Override
	public List<ClienteTO> getClienti(ClienteTO cliente) throws DaoException {
		
		List<ClienteTO> clientiTO = new LinkedList<ClienteTO>();
		Connection con = null;
		String query = createQueryString("SELECT *", cliente);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{	
				ClienteTO ClienteTO = new ClienteTO();
				ClienteTO.setUsername(results.getString("username"));
				ClienteTO.setNome(results.getString("nome"));
				ClienteTO.setCognome(results.getString("cognome"));
				ClienteTO.setEmail(results.getString("email"));
				ClienteTO.setNumTelefono(results.getString("numtelefono"));
				ClienteTO.setCodiceFiscale(results.getString("codicefiscale"));
				
				clientiTO.add(ClienteTO);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return clientiTO;
		
	}

	@Override
	public void setCliente(ClienteTO cliente) throws DaoException {
		
		Connection con = null;
		String query = "INSERT INTO Cliente VALUES (?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, cliente.getUsername());
			statement.setString(2, cliente.getEmail());
			statement.setString(3, cliente.getNome());
			statement.setString(4, cliente.getCognome());
			statement.setString(5, cliente.getNumTelefono());
			statement.setString(6, cliente.getCodiceFiscale());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteCliente(ClienteTO cliente) throws DaoException {
		
		Connection con = null;
		String query = createQueryString("DELETE", cliente);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateCliente(ClienteTO cliente) throws DaoException {
		
		Connection con = null;
		String query = "UPDATE Cliente SET email = ?, nome = ?, cognome = ?, numtelefono = ?, codicefiscale = ? WHERE username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, cliente.getEmail());
			statement.setString(2, cliente.getNome());
			statement.setString(3, cliente.getCognome());
			statement.setString(4, cliente.getNumTelefono());
			statement.setString(5, cliente.getCodiceFiscale());
			statement.setString(6, cliente.getUsername());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}
	
	private String createQueryString(String beginString, ClienteTO cliente)
	{
		String query = beginString + " from Cliente where ";
		
		if(cliente.getUsername() != null)
			query += "Cliente.username = '" + cliente.getUsername() + "' AND ";
		if(cliente.getEmail() != null)
			query += "Cliente.email = '" + cliente.getEmail() + "' AND ";
		if(cliente.getNome() != null)
			query += "Cliente.nome = '" + cliente.getNome() + "' AND ";
		if(cliente.getCognome() != null)
			query += "Cliente.cognome = '" + cliente.getCognome() + "' AND ";
		if(cliente.getNumTelefono() != null)
			query += "Cliente.numtelefono = '" + cliente.getNumTelefono() + "' AND ";
		if(cliente.getCodiceFiscale() != null)
			query += "Cliente.codicefiscale = '" + cliente.getCodiceFiscale() + "' AND ";
		
		query = query.substring(0, query.length() - 5);
		query += ";";
		
		return query;
	}

}
