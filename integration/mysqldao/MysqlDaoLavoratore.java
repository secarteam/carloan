package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.to.LavoratoreRuolo;
import business.to.LavoratoreTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoLavoratore;

public class MysqlDaoLavoratore implements DaoLavoratore{

	public MysqlDaoLavoratore() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public LavoratoreTO getLavoratore(String username) throws DaoException, NoEntryException {
		LavoratoreTO lavoratoreTO = null;
		Connection con = null;
		String query = "SELECT * from PersonaleDitta WHERE username = ?;";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, username);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				lavoratoreTO = new LavoratoreTO();
				lavoratoreTO.setUsername(username);
				lavoratoreTO.setNome(results.getString("nome"));
				lavoratoreTO.setCognome(results.getString("cognome"));
				lavoratoreTO.setEmail(results.getString("email"));
				lavoratoreTO.setRuolo(Enum.valueOf(LavoratoreRuolo.class, results.getString("ruolo")));
				lavoratoreTO.setAzienda(results.getInt("azienda"));
			}
			else
				throw new NoEntryException("Lavoratore non trovato");
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return lavoratoreTO;
		
	}

	@Override
	public LavoratoreTO getLavoratore(LavoratoreTO lavoratore) throws DaoException, NoEntryException {
		LavoratoreTO lavoratoreTO = null;
		Connection con = null;
		String query = createQueryString("SELECT *", lavoratore);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			if(results.next())
			{
				lavoratoreTO = new LavoratoreTO();
				lavoratoreTO.setUsername(results.getString("username"));
				lavoratoreTO.setNome(results.getString("nome"));
				lavoratoreTO.setCognome(results.getString("cognome"));
				lavoratoreTO.setEmail(results.getString("email"));
				lavoratoreTO.setRuolo(Enum.valueOf(LavoratoreRuolo.class, results.getString("ruolo")));
				lavoratoreTO.setAzienda(results.getInt("azienda"));
			}
			else
			{
				throw new NoEntryException("Non ci sono lavoratori con i dati richiesti");
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return lavoratoreTO;
		
	}

	
	@Override
	public List<LavoratoreTO> getLavoratori(LavoratoreTO lavoratore) throws DaoException {
		
		List<LavoratoreTO> lavoratoriTO = new LinkedList<LavoratoreTO>();
		Connection con = null;
		String query = createQueryString("SELECT *", lavoratore);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			ResultSet results = statement.executeQuery();
			
			while(results.next())
			{
				
				LavoratoreTO lavoratoreTO = new LavoratoreTO();
				lavoratoreTO.setUsername(results.getString("username"));
				lavoratoreTO.setNome(results.getString("nome"));
				lavoratoreTO.setCognome(results.getString("cognome"));
				lavoratoreTO.setEmail(results.getString("email"));
				lavoratoreTO.setRuolo(Enum.valueOf(LavoratoreRuolo.class, results.getString("ruolo")));
				lavoratoreTO.setAzienda(results.getInt("azienda"));
				
				lavoratoriTO.add(lavoratoreTO);
			}
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
		return lavoratoriTO;
	}

	@Override
	public void setLavoratore(LavoratoreTO lavoratore) throws DaoException {
		
		Connection con = null;
		String query = "INSERT INTO PersonaleDitta VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, lavoratore.getUsername());
			statement.setString(2, lavoratore.getEmail());
			statement.setString(3, lavoratore.getNome());
			statement.setString(4, lavoratore.getCognome());
			statement.setInt(5, lavoratore.getAzienda());
			statement.setString(6, lavoratore.getRuolo().name());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
	}

	@Override
	public void deleteLavoratore(LavoratoreTO lavoratore) throws DaoException {

		Connection con = null;
		String query = createQueryString("DELETE", lavoratore);
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	@Override
	public void updateLavoratore(LavoratoreTO lavoratore) throws DaoException {
		
		Connection con = null;
		String query = "UPDATE PersonaleDitta SET email = ?, nome = ?, cognome = ?, azienda = ?, ruolo = ? WHERE username = ?";
		PreparedStatement statement = null;
		
		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			
			statement.setString(1, lavoratore.getEmail());
			statement.setString(2, lavoratore.getNome());
			statement.setString(3, lavoratore.getCognome());
			statement.setInt(4, lavoratore.getAzienda());
			statement.setString(5, lavoratore.getRuolo().name());
			statement.setString(6, lavoratore.getUsername());
			
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
		
	}
	
	String createQueryString(String beginString, LavoratoreTO lavoratore)
	{
		String query = beginString + " from PersonaleDitta where ";
		
		if(lavoratore.getNome() != null)
			query += "PersonaleDitta.nome = '" + lavoratore.getNome() + "' AND ";
		if(lavoratore.getCognome() != null)
			query += "PersonaleDitta.cognome = '" + lavoratore.getCognome() + "' AND ";
		if(lavoratore.getEmail() != null)
			query += "PersonaleDitta.email = '" + lavoratore.getEmail() + "' AND ";
		if(lavoratore.getRuolo() != null)
			query += "PersonaleDitta.ruolo = '" + lavoratore.getRuolo().name() + "' AND ";
		if(lavoratore.getAzienda() != 0)
			query += "PersonaleDitta.azienda = " + lavoratore.getAzienda() + " AND ";
		
		query = query.substring(0, query.length() - 5);
		query += ";";
		
		return query;
	}

}
