package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.bo.Fascia;
import business.to.FasciaTO;
import business.to.ModelloTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoModello;

public class MysqlDaoModello implements DaoModello {

	public MysqlDaoModello() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ModelloTO getModello(String modello) throws DaoException, NoEntryException {

		ModelloTO mto = null;
		Connection con = null;
		String query = "SELECT * FROM ModelloFascia WHERE modello = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, modello);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				mto = new ModelloTO();

				mto.setModello(results.getString("modello"));

				MysqlDaoFascia daoFascia = new MysqlDaoFascia();

				Fascia fsc = new Fascia(daoFascia.getFascia(results.getInt("idFascia")));

				mto.setFascia(fsc);
			}
			else
			{
				throw new NoEntryException("Modello non presente");
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return mto;

	}

	@Override
	public List<ModelloTO> getModelliFromFascia(FasciaTO fascia) throws DaoException {

		List<ModelloTO> modelliList = new LinkedList<ModelloTO>();
		Connection con = null;
		String query = "SELECT * FROM ModelloFascia WHERE idFascia = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, fascia.getId());

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				ModelloTO mto = new ModelloTO();
				mto.setModello(results.getString("modello"));

				MysqlDaoFascia daoFascia = new MysqlDaoFascia();

				Fascia fsc = new Fascia(daoFascia.getFascia(results.getInt("idFascia")));

				mto.setFascia(fsc);

				modelliList.add(mto);

			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return modelliList;

	}

	@Override
	public List<ModelloTO> getModelli() throws DaoException {

		List<ModelloTO> modelliList = new LinkedList<ModelloTO>();
		Connection con = null;
		String query = "SELECT * FROM ModelloFascia;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				ModelloTO mto = new ModelloTO();
				mto.setModello(results.getString("modello"));

				MysqlDaoFascia daoFascia = new MysqlDaoFascia();

				Fascia fsc = new Fascia(daoFascia.getFascia(results.getInt("idFascia")));

				mto.setFascia(fsc);

				modelliList.add(mto);

			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return modelliList;
	}

	@Override
	public void setModello(ModelloTO modello) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO ModelloFascia VALUES (?, ?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, modello.getModello());
			statement.setInt(2, modello.getFascia().getId());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateModello(ModelloTO modello) throws DaoException {

		Connection con = null;
		String query = "UPDATE ModelloFascia SET idFascia = ? WHERE modello = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, modello.getFascia().getId());
			statement.setString(2, modello.getModello());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteModello(ModelloTO modello) throws DaoException {

		Connection con = null;
		String query = createQueryString("DELETE", modello);
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	private String createQueryString(String beginString, ModelloTO mto)
	{

		String query = beginString + " from ModelloFascia where ";

		if(mto.getModello() != null)
			query += "ModelloFascia.modello = '" + mto.getModello() + "' AND ";
		if(mto.getFascia() != null)
			query += "ModelloFascia.idFascia = " + mto.getFascia().getId() + " AND ";

		query = query.substring(0, query.length() - 5);
		query += ";";

		return query;
	}

}
