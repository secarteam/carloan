package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoTempFascia;

public class MysqlDaoTempFascia implements DaoTempFascia {

	public double getTempFasciaCosto(int idFascia, int idTT) throws DaoException, NoEntryException
	{
		Connection con = null;
		double cost = 0;
		String query = "SELECT costo from tempFascia where idFascia = ? and idtariffatemporale = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, idFascia);
			statement.setInt(2, idTT);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				cost = results.getDouble("costo");
			}
			else{
				throw new NoEntryException("EmptyResultSet");
			}

			return cost;
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	public void setTempFascia(int idFascia, int idTT, double costo) throws DaoException{
		Connection con = null;
		double cost = 0;
		String query = "insert into TempFascia values(?,?,?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setInt(1, idFascia);
			statement.setInt(2, idTT);
			statement.setDouble(3, costo);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	public void updateTempFascia(int idFascia, int idTT, double costo) throws DaoException{
		Connection con = null;
		double cost = 0;
		String query = "update TempFascia set costo = ? where idtariffatemporale = ? and idfascia = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setDouble(1, costo);
			statement.setInt(2, idTT);
			statement.setInt(3, idFascia);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}

	public void deleteTempFascia(int idFascia, int idTT) throws DaoException{
		Connection con = null;
		String query = "delete from TempFascia where idtariffatemporale = ? and idfascia = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, idTT);
			statement.setInt(2, idFascia);

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}
	}
}
