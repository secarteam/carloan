package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import business.to.ContrattoInCorsoTO;
import integration.DaoException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoContrattoTerminato;

public class MysqlDaoContrattoTerminato implements DaoContrattoTerminato{
	public MysqlDaoContrattoTerminato(){}

	@Override
	public void setContratto(ContrattoInCorsoTO contratto, LocalDate dataCE, double saldoF) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO ContrattiTerminati (idContratto, targa, usernameCliente, dataRitiro, dataConsegnaEffettiva, saldo) VALUES ( ?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			statement.setInt(1, contratto.getId());
			statement.setString(2, contratto.getVettura().getTarga());
			statement.setString(3, contratto.getCliente().getUsername());
			statement.setDate(4, java.sql.Date.valueOf(contratto.getDataInizio()));
			statement.setDate(5, java.sql.Date.valueOf(dataCE));
			statement.setDouble(6, saldoF);

			statement.executeUpdate();

		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}


	}

	@Override
	public void setContratto(ContrattoInCorsoTO contratto) throws DaoException {
		// TODO Auto-generated method stub

	}
}
