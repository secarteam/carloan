package integration.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import business.bo.Modello;
import business.to.LavoratoreRuolo;
import business.to.ManutenzioneType;
import business.to.VetturaTO;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.MysqlDaoFactory;
import integration.interfaces.DaoVettura;

public class MysqlDaoVettura implements DaoVettura {

	public MysqlDaoVettura() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public VetturaTO getVettura(String targa) throws DaoException, NoEntryException {

		VetturaTO vettura = null;
		Connection con = null;
		String query = "SELECT * FROM Vettura WHERE targa = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, targa);

			ResultSet results = statement.executeQuery();

			if(results.next())
			{
				vettura = new VetturaTO();
				vettura.setTarga(results.getString("targa"));
				vettura.setCasaProduttrice(results.getString("casa_produttrice"));
				vettura.setInManutenzione(Enum.valueOf(ManutenzioneType.class, results.getString("manutenzione")));
				vettura.setAgenzia(results.getInt("agenzia"));
				vettura.setKm(results.getDouble("km"));

				MysqlDaoModello mysqlMod = new MysqlDaoModello();

				Modello modello = new Modello(mysqlMod.getModello(results.getString("modello")));

				vettura.setModello(modello);
			}
			else
			{
				throw new NoEntryException("Vettura non presente");
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return vettura;
	}

	@Override
	public List<VetturaTO> getVetture(VetturaTO vettura) throws DaoException {

		List<VetturaTO> listVet = new LinkedList<VetturaTO>();
		Connection con = null;
		String query = createQueryString("SELECT *", vettura);
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				VetturaTO vet = new VetturaTO();
				vet.setTarga(results.getString("targa"));
				vet.setCasaProduttrice(results.getString("casa_produttrice"));

				vet.setInManutenzione(Enum.valueOf(ManutenzioneType.class, results.getString("manutenzione")));
				vet.setAgenzia(results.getInt("agenzia"));
				vettura.setKm(results.getDouble("km"));

				MysqlDaoModello mysqlMod = new MysqlDaoModello();

				Modello modello = new Modello(mysqlMod.getModello(results.getString("modello")));

				vet.setModello(modello);

				listVet.add(vet);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listVet;
	}

	@Override
	public void setVettura(VetturaTO vettura) throws DaoException {

		Connection con = null;
		String query = "INSERT INTO Vettura VALUES (?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, vettura.getTarga());
			statement.setString(2, vettura.getCasaProduttrice());
			statement.setString(3, vettura.getModello().getModello());
			statement.setString(4, vettura.inManutenzione().name());
			statement.setInt(5, vettura.getAgenzia());
			statement.setDouble(6, vettura.getKm());

			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void updateVettura(VetturaTO vettura) throws DaoException {

		Connection con = null;
		String query = "UPDATE Vettura SET casa_produttrice = ?, modello = ?, manutenzione = ?, agenzia = ?, km = ? WHERE targa = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setString(1, vettura.getCasaProduttrice());
			statement.setString(2, vettura.getModello().getModello());
			//statement.setBoolean(3, vettura.isInManutenzione());
			statement.setString(3, vettura.inManutenzione().name());
			statement.setInt(4, vettura.getAgenzia());
			statement.setDouble(5, vettura.getKm());
			statement.setString(6, vettura.getTarga());


			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}


	}

	@Override
	public void deleteVettura(VetturaTO vettura) throws DaoException {

		Connection con = null;
		String query = createQueryString("DELETE", vettura);
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);


			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public void deleteVettura(String targa) throws DaoException {

		Connection con = null;
		String query = "DELETE from Vettura where targa = ?";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);
			statement.setString(1, targa);


			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

	}

	@Override
	public List<VetturaTO> getVetture() throws DaoException {

		List<VetturaTO> listVet = new LinkedList<VetturaTO>();
		Connection con = null;
		String query = "SELECT * FROM Vettura;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				VetturaTO vet = new VetturaTO();
				vet.setTarga(results.getString("targa"));
				vet.setCasaProduttrice(results.getString("casa_produttrice"));

				vet.setInManutenzione(Enum.valueOf(ManutenzioneType.class, results.getString("manutenzione")));
				vet.setAgenzia(results.getInt("agenzia"));
				vet.setKm(results.getDouble("km"));

				MysqlDaoModello mysqlMod = new MysqlDaoModello();

				Modello modello = new Modello(mysqlMod.getModello(results.getString("modello")));

				vet.setModello(modello);

				listVet.add(vet);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listVet;
	}

public List<VetturaTO> getVetture(int idAzienda) throws DaoException {

		List<VetturaTO> listVet = new LinkedList<VetturaTO>();
		Connection con = null;
		String query = "SELECT * FROM Vettura where agenzia = ?;";
		PreparedStatement statement = null;

		try
		{
			con = MysqlDaoFactory.getConnection();
			statement = con.prepareStatement(query);

			statement.setInt(1, idAzienda);

			ResultSet results = statement.executeQuery();

			while(results.next())
			{
				VetturaTO vet = new VetturaTO();
				vet.setTarga(results.getString("targa"));
				vet.setCasaProduttrice(results.getString("casa_produttrice"));

				vet.setInManutenzione(Enum.valueOf(ManutenzioneType.class, results.getString("manutenzione")));
				vet.setAgenzia(results.getInt("agenzia"));
				vet.setKm(results.getDouble("km"));

				MysqlDaoModello mysqlMod = new MysqlDaoModello();

				Modello modello = new Modello(mysqlMod.getModello(results.getString("modello")));

				vet.setModello(modello);

				listVet.add(vet);
			}
		}
		catch(SQLException | DaoException e)
		{
			throw new DaoException(e);
		}
		finally
		{
			if(statement != null)
				try {
					statement.close();
					MysqlDaoFactory.closeConnection(con);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					throw new DaoException(e);
				}
		}

		return listVet;
	}

	private String createQueryString(String beginString, VetturaTO vet)
	{

		String query = beginString + " from Vettura where ";

		if(vet.getTarga() != null)
			query += "Vettura.targa = '" + vet.getTarga() + "' AND ";
		if(vet.getCasaProduttrice() != null)
			query += "Vettura.casa_produttrice = '" + vet.getCasaProduttrice() + "' AND ";
		if(vet.getModello() != null)
			query += "Vettura.modello = '" + vet.getModello().getModello() + "' AND ";
		if(vet.isInManutenzione() == true)
			query += "Vettura.manutenzione = 1" + " AND ";
		if(vet.isInManutenzione() == false)
			query += "Vettura.manutenzione = 0" + " AND ";
		if(vet.getAgenzia() != 0)
			query += "Vettura.modello = " + vet.getAgenzia() + " AND ";
		if(vet.getKm() != 0)
			query += "Vettura.km = " + vet.getKm() + " AND ";



		query = query.substring(0, query.length() - 5);
		query += ";";

		return query;
	}

//	private String queryIsInManutenzione(String beginString, boolean isInManutenzione)
//	{
//		String query = beginString + " from Vettura where manutenzione = ";
//
//		if(isInManutenzione == true)
//			query += "y;";
//		else
//			query += "n;";
//
//		return query;
//	}

}
