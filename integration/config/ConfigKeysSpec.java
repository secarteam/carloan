package integration.config;

public class ConfigKeysSpec {

	private final static String PATH = "/integration/config/mysql.properties";
	private final static String HOSTKEY = "host";
	private final static String DBKEY = "database";
	private final static String USERKEY = "dbuser";
	private final static String PWDKEY = "dbpassword";
	private final static String TARGIOR = "tariffagiornaliera";
	private final static String TARSET = "tariffasettimanale";
	private final static String TARLIM = "tariffalimitata";
	private final static String TARILL = "tariffaillimitata";


	public static String getPath() {
		return PATH;
	}
	public static String getHostkey() {
		return HOSTKEY;
	}
	public static String getDbkey() {
		return DBKEY;
	}
	public static String getUserkey() {
		return USERKEY;
	}
	public static String getPwdkey() {
		return PWDKEY;
	}
	public static String getTargior() {
		return TARGIOR;
	}
	public static String getTarset() {
		return TARSET;
	}
	public static String getTarlim() {
		return TARLIM;
	}
	public static String getTarill() {
		return TARILL;
	}

}
