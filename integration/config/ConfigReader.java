package integration.config;

/**
 * Si occupa delle operazioni di lettura dei file .properties
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import java.io.IOException;

public class ConfigReader {

	private Properties properties;
	private InputStream input = null;
	
	/**
	 * Costruttore che legge il file collocato nella stringa espressa nel parametro
	 * di input e ne effettua i loading nella struttura di tipo {@link Properties Properties}.
	 * @param path percorso in cui si trova il file properties
	 */
	public ConfigReader(String path) {
		try
		{
			input = ConfigReader.class.getResourceAsStream(path);
			properties = new Properties();
			properties.load(input);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Permette di ottenere la stringa associata al parametro di input.
	 * @param propName	chiave
	 * @return	stringa associata alla chiave fornita come input
	 */
	public String getProperty(String propName)
	{
		if(properties.containsKey(propName))
			return properties.getProperty(propName);
		else
			return null;
	}
	
	public Set<Object> keySet()
	{
		return properties.keySet();
	}
	
	public void close()
	{
		if(input != null)
		{
			try
			{
				input.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}

}
