/**
 * Contiene i file di configurazione e le classi per accedere a tali file
 * 
 * @author Vito Vito Vincenzo Covella, Francesca Gaudiomonte
 */
package integration.config;