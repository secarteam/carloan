package integration.factories;

import integration.DaoException;
import integration.interfaces.DaoAccount;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoChilFascia;
import integration.interfaces.DaoCliente;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoContrattoScelte;
import integration.interfaces.DaoContrattoTerminato;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoLavoratore;
import integration.interfaces.DaoModello;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoSecData;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;
import integration.interfaces.DaoVettura;

/**
 * Classe astratta che permette di ottenere le DAO Factory concrete,
 * ognuna delle quali riguarda una specifica logica di accesso ai dati
 * memorizzati su storage. In questo caso viene reso disponibile l'accesso al
 * {@link MysqlDaoFactory} tramite la costante statica MYSQL e il metodo
 * {@link #getDaoFactory(int choice)}. Inoltre fornisce l'interfaccia per ottenere
 * i DAO relativi a determinate entità e transfer object.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public abstract class DaoFactory {

	public static final int MYSQL = 1;

	public static DaoFactory getDaoFactory(int choice)
	{
		DaoFactory dao = null;

		switch(choice)
		{

			case MYSQL:
				dao = MysqlDaoFactory.getInstance();
				break;
			default:
				dao = null;
				break;
		}

		return dao;
	}

	@SuppressWarnings("rawtypes")
	protected static Object createDao(Class c) throws DaoException
	{
		try {
			return c.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new DaoException(e);
		}
	}

	public abstract DaoAccount getDaoAccount() throws DaoException;
	public abstract DaoAgenzia getDaoAgenzia() throws DaoException;
	public abstract DaoCliente getDaoCliente() throws DaoException;
	public abstract DaoContrattoInCorso getDaoContratto() throws DaoException;
	public abstract DaoContrattoScelte getDaoContrattoScelte() throws DaoException;
	public abstract DaoFascia getDaoFascia() throws DaoException;
	public abstract DaoLavoratore getDaoLavoratore() throws DaoException;
	public abstract DaoModello getDaoModello() throws DaoException;
	public abstract DaoOptional getDaoOptional() throws DaoException;
	public abstract DaoTariffaChilometraggio getDaoTariffaChilometraggio() throws DaoException;
	public abstract DaoTariffaTemporale getDaoTariffaTemporale() throws DaoException;
	public abstract DaoVettura getDaoVettura() throws DaoException;
	public abstract DaoSecData getDaoSecData() throws DaoException;
	public abstract DaoTempFascia getDaoTempFascia() throws DaoException;
	public abstract DaoChilFascia getDaoChilFascia() throws DaoException;
	public abstract DaoContrattoTerminato getDaoContrattoTerminato() throws DaoException;

}
