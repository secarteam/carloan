package integration.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import integration.DaoException;
import integration.config.ConfigKeysSpec;
import integration.config.ConfigReader;
import integration.interfaces.DaoAccount;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoChilFascia;
import integration.interfaces.DaoCliente;
import integration.interfaces.DaoContrattoInCorso;
import integration.interfaces.DaoContrattoScelte;
import integration.interfaces.DaoContrattoTerminato;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoLavoratore;
import integration.interfaces.DaoModello;
import integration.interfaces.DaoOptional;
import integration.interfaces.DaoSecData;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import integration.interfaces.DaoTempFascia;
import integration.interfaces.DaoVettura;
import integration.mysqldao.MysqlDaoAgenzia;
import integration.mysqldao.MysqlDaoChilFascia;
import integration.mysqldao.MysqlDaoCliente;
import integration.mysqldao.MysqlDaoContrattoInCorso;
import integration.mysqldao.MysqlDaoContrattoScelte;
import integration.mysqldao.MysqlDaoContrattoTerminato;
import integration.mysqldao.MysqlDaoFascia;
import integration.mysqldao.MysqlDaoLavoratore;
import integration.mysqldao.MysqlDaoModello;
import integration.mysqldao.MysqlDaoOptional;
import integration.mysqldao.MysqlDaoSecData;
import integration.mysqldao.MysqlDaoTariffaChilometraggio;
import integration.mysqldao.MysqlDaoTariffaTemporale;
import integration.mysqldao.MysqlDaoTempFascia;
import integration.mysqldao.MysqlDaoVettura;

/**
 * Implementa {@link DaoFactory} e permette di ottenere i DAO
 * che usano MySQL come logica di accesso ai dati.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class MysqlDaoFactory extends DaoFactory{

	private static MysqlDaoFactory mysqlDaoFactory;

	private static String host;
	private static String db;
	private static String username;
	private static String password;
	private static String connessione;


	public static MysqlDaoFactory getInstance()
	{
		if (mysqlDaoFactory == null) {
			mysqlDaoFactory = new MysqlDaoFactory();
		}
		return mysqlDaoFactory;
	}

	/**
	 * Costruttore privato, usato da {@link #getInstance()} (pattern Singleton).
	 * In questo costruttore viene letto il file .properties in cui sono contenute
	 * le informazioni sulla corretta configurazione per accedere al database.
	 * Questo costruttore usa {@link ConfigReader} per accedere al file
	 * .properties.
	 */
	private MysqlDaoFactory() {
		ConfigReader reader = new ConfigReader(ConfigKeysSpec.getPath());
		host = reader.getProperty(ConfigKeysSpec.getHostkey());
		db = reader.getProperty(ConfigKeysSpec.getDbkey());
		username = reader.getProperty(ConfigKeysSpec.getUserkey());

		/*TODO: per ora verrà usata la password in chiaro, nelle fasi successive
		 * del development consiglierei di memorizzare su config una versione criptata
		 * e decriptarla all'occorrenza
		 */
		password = reader.getProperty(ConfigKeysSpec.getPwdkey());
		connessione = "jdbc:mysql://" + host + "/" + db;
	}

	public static Connection getConnection()throws SQLException
	{

		getInstance();
		Connection con = DriverManager.getConnection(connessione, username, password);

		return con;

	}

	public static void closeConnection(Connection con) throws SQLException
	{
		if(con != null && !con.isClosed())
		{
			con.close();
			con = null;
		}
	}

	@Override
	public DaoAccount getDaoAccount() throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DaoAgenzia getDaoAgenzia() throws DaoException {
		return (DaoAgenzia) createDao(MysqlDaoAgenzia.class);
	}

	@Override
	public DaoCliente getDaoCliente() throws DaoException {
		return (DaoCliente) createDao(MysqlDaoCliente.class);
	}

	@Override
	public DaoContrattoInCorso getDaoContratto() throws DaoException {
		return (DaoContrattoInCorso) createDao(MysqlDaoContrattoInCorso.class);
	}

	@Override
	public DaoContrattoScelte getDaoContrattoScelte() throws DaoException {
		return (DaoContrattoScelte) createDao(MysqlDaoContrattoScelte.class);
	}

	@Override
	public DaoFascia getDaoFascia() throws DaoException {
		return (DaoFascia) createDao(MysqlDaoFascia.class);
	}

	@Override
	public DaoLavoratore getDaoLavoratore() throws DaoException {
		return (DaoLavoratore) createDao(MysqlDaoLavoratore.class);
	}

	@Override
	public DaoModello getDaoModello() throws DaoException {
		return (DaoModello) createDao(MysqlDaoModello.class);
	}

	@Override
	public DaoOptional getDaoOptional() throws DaoException {
		return (DaoOptional) createDao(MysqlDaoOptional.class);
	}

	@Override
	public DaoTariffaChilometraggio getDaoTariffaChilometraggio() throws DaoException {
		return (DaoTariffaChilometraggio) createDao(MysqlDaoTariffaChilometraggio.class);
	}

	@Override
	public DaoTariffaTemporale getDaoTariffaTemporale() throws DaoException {
		return (DaoTariffaTemporale) createDao(MysqlDaoTariffaTemporale.class);
	}

	@Override
	public DaoVettura getDaoVettura() throws DaoException {
		return (DaoVettura) createDao(MysqlDaoVettura.class);
	}

	@Override
	public DaoSecData getDaoSecData() throws DaoException {
		return (DaoSecData) createDao(MysqlDaoSecData.class);
	}

	@Override
	public DaoTempFascia getDaoTempFascia() throws DaoException {
		return (DaoTempFascia) createDao(MysqlDaoTempFascia.class);
	}

	@Override
	public DaoChilFascia getDaoChilFascia() throws DaoException {
		return (DaoChilFascia) createDao(MysqlDaoChilFascia.class);
	}

	@Override
	public DaoContrattoTerminato getDaoContrattoTerminato() throws DaoException{
		return (DaoContrattoTerminato) createDao(MysqlDaoContrattoTerminato.class);
	}
}
