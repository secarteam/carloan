/**
 * Contiene le factory dei DAO
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */
package integration.factories;