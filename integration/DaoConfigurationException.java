package integration;

public class DaoConfigurationException extends RuntimeException {

	public DaoConfigurationException() {
		// TODO Auto-generated constructor stub
	}

	public DaoConfigurationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DaoConfigurationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DaoConfigurationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DaoConfigurationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
