package integration.interfaces;

import java.util.List;

import business.to.FasciaTO;
import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.DaoException;

public interface DaoFascia {
	
	public FasciaTO getFascia(int id) throws DaoException;
	public FasciaTO getFascia(String nome) throws DaoException;
	
	public List<FasciaTO> getFasce() throws DaoException;
	public List<TariffaTemporaleTO> getTariffeTemporaliByFasciaName(String nome) throws DaoException;
	public List<TariffaChilometraggioTO> getTariffeChilometraggioByFasciaName(String nome) throws DaoException;
	
	public void setFascia(FasciaTO fascia) throws DaoException;
	public void updateFascia(FasciaTO fascia) throws DaoException;
	public void deleteFascia(FasciaTO fascia) throws DaoException;

}
