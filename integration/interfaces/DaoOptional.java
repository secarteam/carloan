package integration.interfaces;

import java.util.List;
import business.to.OptionalTO;
import integration.DaoException;

public interface DaoOptional {

	public OptionalTO getOptional(int id) throws DaoException;
	public OptionalTO getOptional(String nome) throws DaoException;
	public List<OptionalTO> getOptional() throws DaoException;
	public List<OptionalTO> getOptionals(OptionalTO optional) throws DaoException;
	public List<OptionalTO> getOptionalCostZero() throws DaoException;
	public List<OptionalTO> getOptionalAvalZero() throws DaoException;
	
	public void setOptional(OptionalTO optional) throws DaoException;
	public void updateOptional(OptionalTO optional) throws DaoException;
	public void deleteOptional(OptionalTO optional) throws DaoException;
}
