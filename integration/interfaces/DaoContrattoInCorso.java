package integration.interfaces;

import business.bo.Cliente;
import business.bo.SceltaOptional;
import business.to.ContrattoInCorsoTO;
import business.to.VetturaTO;
import integration.DaoException;

import java.util.List;

public interface DaoContrattoInCorso {

	public ContrattoInCorsoTO getContratto(int id) throws DaoException;
	//public List<ContrattoInCorsoTO> getContratti(ContrattoInCorsoTO contratto);
	public List<ContrattoInCorsoTO> getContratti(Cliente cliente) throws DaoException;
	public List<ContrattoInCorsoTO> getContratti() throws DaoException;
	public List<ContrattoInCorsoTO> getContratticonVettureInContratto(String targa) throws DaoException;
	public List<VetturaTO> getVettureInContratto() throws DaoException;
	public void setContratto(ContrattoInCorsoTO contratto) throws DaoException;
	public void deleteContratto(ContrattoInCorsoTO contratto) throws DaoException;
	public void updateContratto(ContrattoInCorsoTO contratto) throws DaoException;
	public List<SceltaOptional> getListaOptionalScelti() throws DaoException;

}
