package integration.interfaces;

import java.util.List;

import business.to.ClienteTO;
import integration.DaoException;
import integration.NoEntryException;

public interface DaoCliente {
	
	public ClienteTO getCliente(String username) throws DaoException, NoEntryException;
	public ClienteTO getClienteByEmail(String email) throws DaoException, NoEntryException;
	public List<ClienteTO> getClienti(ClienteTO cliente) throws DaoException;
	public void setCliente(ClienteTO cliente) throws DaoException;
	public void deleteCliente(ClienteTO cliente) throws DaoException;
	public void updateCliente(ClienteTO cliente) throws DaoException;
}
