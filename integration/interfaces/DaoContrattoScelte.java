package integration.interfaces;

import business.to.ContrattoScelteTariffeTO;
import integration.DaoException;

import java.util.List;

public interface DaoContrattoScelte {
	
	public ContrattoScelteTariffeTO getContrattoScelte(int contratto) throws DaoException;
	public List<ContrattoScelteTariffeTO> getContrattiScelteByTariffaTemporale(int temp) throws DaoException;
	public List<ContrattoScelteTariffeTO> getContrattiScelteByTariffaChilometraggio(int chil) throws DaoException;
	public List<ContrattoScelteTariffeTO> getContrattiScelteByCostoTariffaTemporale(double costo) throws DaoException;
	public List<ContrattoScelteTariffeTO> getContrattiScelteByCostoTariffaChilometraggio(double costo) throws DaoException;
	
	public void setContrattoScelte(ContrattoScelteTariffeTO contrattoScelte) throws DaoException;
	public void updateContrattoScelte(ContrattoScelteTariffeTO contrattoScelte) throws DaoException;
	public void deleteContrattoScelte(int contratto) throws DaoException;
}
