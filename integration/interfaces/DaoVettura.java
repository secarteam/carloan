package integration.interfaces;

import java.util.List;
import business.to.VetturaTO;
import integration.DaoException;
import integration.NoEntryException;

public interface DaoVettura {

	public VetturaTO getVettura(String targa) throws DaoException, NoEntryException;
	public List<VetturaTO> getVetture(VetturaTO vettura) throws DaoException;
	public List<VetturaTO> getVetture() throws DaoException;
	public List<VetturaTO> getVetture(int idAzienda) throws DaoException;
	public void setVettura(VetturaTO vettura) throws DaoException;
	public void updateVettura(VetturaTO vettura) throws DaoException;
	public void deleteVettura(VetturaTO vettura) throws DaoException;
	public void deleteVettura(String targa) throws DaoException;

}
