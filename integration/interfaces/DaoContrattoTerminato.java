package integration.interfaces;

import java.time.LocalDate;

import business.to.ContrattoInCorsoTO;
import integration.DaoException;

public interface DaoContrattoTerminato {

	void setContratto(ContrattoInCorsoTO contratto) throws DaoException;
	void setContratto(ContrattoInCorsoTO contratto, LocalDate dataCE, double saldoF) throws DaoException;

}
