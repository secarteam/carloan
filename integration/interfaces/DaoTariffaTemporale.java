package integration.interfaces;

import java.util.List;

import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.DaoException;

public interface DaoTariffaTemporale {

	public TariffaTemporaleTO getTariffaTemporale(int id) throws DaoException;
	public TariffaTemporaleTO getTariffaTemporale(String nome) throws DaoException;
	public List<TariffaTemporaleTO> getTariffeTemporali() throws DaoException;
	public void setTariffaTemporale(TariffaTemporaleTO tariffaTemporale) throws DaoException;
	public void updateTariffaTemporale(TariffaTemporaleTO tariffaTemporale) throws DaoException;
	public void deleteTariffaTemporale(TariffaTemporaleTO tariffaTemporale) throws DaoException;

}
