package integration.interfaces;

import integration.DaoException;
import integration.NoEntryException;

public interface DaoChilFascia {
	public double getChilFasciaCosto(int idFascia, int idTC) throws DaoException, NoEntryException;
	public void setChilFascia(int idFascia, int idTC, double costo) throws DaoException;
	public void updateChilFascia(int idFascia, int idTC, double costo) throws DaoException;
	public void deleteChilFascia(int idFascia, int idTT) throws DaoException;
}
