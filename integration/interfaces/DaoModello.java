package integration.interfaces;

import java.util.List;
import business.to.ModelloTO;
import integration.DaoException;
import integration.NoEntryException;
import business.to.FasciaTO;

public interface DaoModello {

	public ModelloTO getModello(String modello) throws DaoException, NoEntryException;
	public List<ModelloTO> getModelliFromFascia(FasciaTO fascia) throws DaoException;

	public void setModello(ModelloTO modello) throws DaoException;
	public void updateModello(ModelloTO modello) throws DaoException;
	public void deleteModello(ModelloTO modello) throws DaoException;
	public List<ModelloTO> getModelli() throws DaoException;

}
