package integration.interfaces;

import integration.DaoException;
import integration.NoEntryException;

public interface DaoTempFascia {
	public double getTempFasciaCosto(int idFascia, int idTT) throws DaoException, NoEntryException;
	public void setTempFascia(int idFascia, int idTT, double costo) throws DaoException;
	public void updateTempFascia(int idFascia, int idTT, double costo) throws DaoException;
	public void deleteTempFascia(int idFascia, int idTT) throws DaoException;
}
