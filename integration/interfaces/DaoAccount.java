package integration.interfaces;

import java.util.List;

import business.to.AccountTO;
import integration.DaoException;

public interface DaoAccount {

	public AccountTO getAccount(String username) throws DaoException;
	public List<AccountTO> getAccounts(AccountTO accountTO) throws DaoException;
	public void setAccount(AccountTO accountTO) throws DaoException;
	public void deleteAccount(AccountTO accountTO) throws DaoException;
	public void updateAccount(AccountTO accountTO) throws DaoException;
}
