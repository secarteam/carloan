package integration.interfaces;

import java.util.List;

import business.to.TariffaChilometraggioTO;
import integration.DaoException;

public interface DaoTariffaChilometraggio {

	public TariffaChilometraggioTO getTariffaChilometraggio(int id) throws DaoException;
	public TariffaChilometraggioTO getTariffaChilometraggio(String nome) throws DaoException;
	public List<TariffaChilometraggioTO> getTariffeChilometraggio() throws DaoException;
	public void setTariffaChilometraggio(TariffaChilometraggioTO tariffaChilometraggio) throws DaoException;
	public void updateTariffaChilometraggio(TariffaChilometraggioTO tariffaChilometraggio) throws DaoException;
	public void deleteTariffaChilometraggio(TariffaChilometraggioTO tariffaChilometraggio) throws DaoException;

}
