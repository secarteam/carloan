package integration.interfaces;

import integration.DaoException;
import integration.NoEntryException;

public interface DaoSecData {
	
	public String getUserHash(String username) throws DaoException, NoEntryException;
	public void setUserHash(String username, String pwd) throws DaoException;
	public void updateUserHash(String username, String pwd) throws DaoException;
	public void deleteUserHash(String username) throws DaoException;
}
