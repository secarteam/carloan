package integration.interfaces;


import java.util.List;

import business.to.AgenziaTO;
import business.to.LavoratoreTO;
import integration.DaoException;
import integration.NoEntryException;

public interface DaoAgenzia {
	
	public AgenziaTO getAgenzia(String nome) throws DaoException, NoEntryException;
	public AgenziaTO getAgenzia(int id) throws DaoException;
	public List<AgenziaTO> getAgenzie(AgenziaTO agenzia) throws DaoException;
	
	public void setAgenzia(AgenziaTO agenzia) throws DaoException;
	 
	public List<AgenziaTO> getAgenzie() throws DaoException;
	public List<LavoratoreTO> getLavoratori(AgenziaTO agenzia) throws DaoException;
	
	public void deleteAgenzia(AgenziaTO agenzia) throws DaoException;
	public void updateAgenzia(AgenziaTO agenzia) throws DaoException;
	 

}
