package integration.interfaces;

import java.util.List;

import business.to.LavoratoreTO;
import integration.DaoException;
import integration.NoEntryException;

public interface DaoLavoratore {

	public LavoratoreTO getLavoratore(String username) throws DaoException, NoEntryException;
	public List<LavoratoreTO> getLavoratori(LavoratoreTO lavoratore) throws DaoException;
	public void setLavoratore(LavoratoreTO lavoratore) throws DaoException;
	public void deleteLavoratore(LavoratoreTO lavoratore) throws DaoException;
	public void updateLavoratore(LavoratoreTO lavoratore) throws DaoException;
	LavoratoreTO getLavoratore(LavoratoreTO lavoratore) throws DaoException, NoEntryException;
	
}
