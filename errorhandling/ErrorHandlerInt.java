package errorhandling;

/**
 * Interfaccia per la gestione degli errori
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 *
 */

public interface ErrorHandlerInt {
	public void handleError();
}
