/**
 * Contiene le classi per la gestione degli errori ed eccezioni
 * 
 * @author Vito Vito Vincenzo Covella, Francesca Gaudiomonte
 */
package errorhandling;