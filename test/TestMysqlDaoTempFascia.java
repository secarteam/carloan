package test;

import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

import integration.DaoException;
import integration.NoEntryException;
import integration.mysqldao.MysqlDaoTempFascia;

public class TestMysqlDaoTempFascia extends DatabaseTestCase {
	
	private IDataSet loadedDataSet;
	
	@Override
	protected IDatabaseConnection getConnection() throws Exception {
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/CarLoanDB", "carloan", "carloanPwd");
		return new DatabaseConnection(con);
		
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/dbseed/tempfascia-seed.xml"));
		return loadedDataSet;
	}
	
	protected DatabaseOperation getSetUpOperation() throws Exception 
	{
			   return DatabaseOperation.CLEAN_INSERT;
	}
	
	protected DatabaseOperation getTearDownOperation() throws Exception
	{
		return DatabaseOperation.NONE;
	}
	
	/**
	 * Testiamo cosa accade qualora si richieda il costo di una tariffa associato ad una
	 * specifica fascia. Dato che tale associazione esiste effettivamente nel database
	 * ci aspettiamo che venga ritornato il costo effettivo.
	 */
	@Test
	public void testGetTempFasciaCosto_TC1() {
		
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		double costoPriori = 50;
		double costoRet = 0;
		
		costoRet = daoTempFascia.getTempFasciaCosto(1, 1);
		
		assertTrue(costoPriori == costoRet);
	}
	
	/**
	 * Testiamo cosa accade qualora si richieda il costo di una tariffa associato ad una specifica
	 * fascia nel caso in cui tale associazione non esiste nel database.
	 * Ci aspettiamo una NoEntryException.
	 */
	@Test
	public void testGetTempFasciaCosto_TC2()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		try
		{
			daoTempFascia.getTempFasciaCosto(1, 2);
			fail("ottenuti dati inesistenti nel database");
		}
		catch(NoEntryException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	/**
	 * Test dell'inserimento in TempFascia del costo relativo ad una tariffa temporale
	 * associata ad una specifica fascia. Entrambi gli id di fascia e tariffa sono presenti
	 * nelle rispettive tabelle ma la coppia, che costituisce una primary key composita,
	 * non è presente in TempFascia.
	 */
	@Test
	public void testSetTempFascia_TC1()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		double costoAPriori = 70;
		double costoRet = 0;
		
		daoTempFascia.setTempFascia(1, 2, costoAPriori);
		
		costoRet = daoTempFascia.getTempFasciaCosto(1, 2);
		
		assertTrue(costoRet == costoAPriori);
	}
	
	/**
	 * Test dell'inserimento in TempFascia del costo relativo ad una tariffa temporale
	 * associata ad una specifica fascia. Gli id scelti sono già presenti in coppia in
	 * TempFascia e ci aspettiamo che venga restituita una DaoException.
	 */
	@Test
	public void testSetTempFascia_TC2()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		try
		{
				daoTempFascia.setTempFascia(1, 1, 70);
				fail("inseriti i dati pur essendo già presenti");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Test dell'inserimento in TempFascia del costo relativo ad una tariffa temporale
	 * associata ad una specifica fascia. La fascia con id 4 non esiste, quindi veirichiamo
	 * che venga lanciata una DaoException.
	 */
	@Test
	public void testSetTempFascia_TC3()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		try
		{
				daoTempFascia.setTempFascia(4, 1, 70);
				fail("violato il vincolo di foreign key durante l'inserimento");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Test dell'inserimento in TempFascia del costo relativo ad una tariffa temporale
	 * associata ad una specifica fascia. La tariffa temporale con id 3
	 * non esiste, quindi veirichiamo che venga lanciata una DaoException.
	 */
	@Test
	public void testSetTempFascia_TC4()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		try
		{
				daoTempFascia.setTempFascia(3, 3, 70);
				fail("violato il vincolo di foreign key durante l'inserimento");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Test dell'inserimento in TempFascia con dati di fascia invalidi (input < 1)
	 * e altri dati validi
	 */
	@Test
	public void testSetTempFascia_TC5()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		try
		{
				daoTempFascia.setTempFascia(0, 1, 70);
				fail("inserimento avvenuto con successo pur usando dati invalidi");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Test dell'inserimento in TempFascia con i dati di tariffa invalidi (input < 1)
	 * e altri dati validi
	 */
	@Test
	public void testSetTempFascia_TC6()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		try
		{
				daoTempFascia.setTempFascia(1, 0, 70);
				fail("inserimento avvenuto con successo pur usando dati invalidi");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Test dell'update in TempFascia del costo relativo ad una tariffa temporale
	 * associata ad una specifica fascia. Entrambi gli id specificati
	 * esistono come coppia e come primary key nella tabella target.
	 * Ci aspettiamo che il costo venga effettivamente aggiornato.
	 */
	@Test
	public void testUpdateTempFascia_TC1()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		double costoAPriori = 70;
		double costoRet = 0;
		
		daoTempFascia.updateTempFascia(1, 1, costoAPriori);
		
		costoRet = daoTempFascia.getTempFasciaCosto(1, 1);
		
		assertTrue(costoRet == costoAPriori);
	}
	
	/**
	 * Test dell'update in TempFascia del costo relativo ad una tariffa temporale
	 * associata ad una specifica fascia. La coppia di id scelta non esiste 
	 * nel database target.
	 * @throws Exception 
	 */
	@Test
	public void testUpdateTempFascia_TC2() throws Exception
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		daoTempFascia.updateTempFascia(1, 2, 70);
		
		ITable queryTable = getConnection().createTable("tempfascia");
		
		Assertion.assertEquals(loadedDataSet.getTable("tempfascia"), queryTable);
	}
	
	/**
	 * Test di updateTempFascia con fascia non valida perché minore di 1
	 */
	@Test
	public void testUpdateTempFascia_TC3() throws Exception
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		daoTempFascia.updateTempFascia(0, 1, 70);
		
		ITable queryTable = getConnection().createTable("tempfascia");
		
		Assertion.assertEquals(loadedDataSet.getTable("tempfascia"), queryTable);
	}
	
	/**
	 * Test di updateTempFascia con tariffa non valida perché minore di 1
	 */
	@Test
	public void testUpdateTempFascia_TC4() throws Exception
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		daoTempFascia.updateTempFascia(1, 0, 70);
		
		ITable queryTable = getConnection().createTable("tempfascia");
		
		Assertion.assertEquals(loadedDataSet.getTable("tempfascia"), queryTable);
	}
	
	/**
	 * Test della delete di una riga in TempFascia.
	 * Entrambi gli id scelti esistono come coppia nella tabella target,
	 * ci aspettiamo che la rimozione avvenga con successo.
	 */
	@Test
	public void testDeleteTempFascia_TC1()
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		daoTempFascia.deleteTempFascia(1, 1);
		
		try
		{
			daoTempFascia.getTempFasciaCosto(1, 1);
			fail("Ottenuto il costo della riga rimossa");
		}
		catch(NoEntryException e)
		{
			//ignore
		}
	}
	
	/**
	 * Test della delete di una riga in TempFascia.
	 * Entrambi gli id scelti non esistono come coppia nella tabella target.
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@Test
	public void testDeleteTempFascia_TC2() throws SQLException, Exception
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		int beforeCount = getConnection().getRowCount("tempfascia");
		
		daoTempFascia.deleteTempFascia(1, 2);
		
		int afterCount = getConnection().getRowCount("tempfascia");
		
		assertTrue(beforeCount == afterCount);
	}
	
	/**
	 * Test di deleteTempFascia con parametro di input sulla fascia invalido (perché minore di 1)
	 */
	@Test
	public void testDeleteTempFascia_TC3() throws SQLException, Exception
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		int beforeCount = getConnection().getRowCount("tempfascia");
		
		daoTempFascia.deleteTempFascia(0, 1);
		
		int afterCount = getConnection().getRowCount("tempfascia");
		
		assertTrue(beforeCount == afterCount);

	}
	
	/**
	 * Test di deleteTempFascia con parametro di input sulla tariffa invalido (perché minore di 1)
	 */
	@Test
	public void testDeleteTempFascia_TC4() throws SQLException, Exception
	{
		MysqlDaoTempFascia daoTempFascia = new MysqlDaoTempFascia();
		
		int beforeCount = getConnection().getRowCount("tempfascia");
		
		daoTempFascia.deleteTempFascia(1, 0);
		
		int afterCount = getConnection().getRowCount("tempfascia");
		
		assertTrue(beforeCount == afterCount);
	}
	

}
