package test;

import org.junit.Test;
import static org.junit.Assert.assertThat;

import org.hamcrest.core.IsInstanceOf;

import business.OperazioneNonCompletata;
import business.appservice.AppServiceFactory;
import business.appservice.AppServiceInt;
import business.appservice.Registrazione;
import junit.framework.TestCase;

public class TestAppServiceFactory extends TestCase {
	
	private AppServiceFactory appServiceFactory;
	
	public void setUp()
	{
		appServiceFactory = AppServiceFactory.getInstance();
	}
	
	@Test
	public void testGetService_TC1()
	{
		String sName = "Registrazione";
		
		try {
			AppServiceInt service = appServiceFactory.getService(sName);
			assertThat(service, IsInstanceOf.instanceOf(Registrazione.class));
		} catch (OperazioneNonCompletata e) {
			fail("Fallimento: istanza con chiave corretta non ottenuta.");
		}
	}
	
	@Test
	public void testGetService_TC2()
	{
		String sName = "SignIn";
		
		try {
			AppServiceInt service = appServiceFactory.getService(sName);
			fail("Ottenuta istanza con chiave non esistente nell'xml e nella map dei servizi");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NullPointerException.class));
		}
	}
	
	@Test
	public void testGetService_TC3()
	{
		String sName = "";
		
		try {
			AppServiceInt service = appServiceFactory.getService(sName);
			fail("Ottenuta istanza con chiave non esistente nell'xml e nella map dei servizi");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NullPointerException.class));
		}
	}
	
	@Test
	public void testGetService_TC4()
	{
		String sName = null;
		
		try {
			AppServiceInt service = appServiceFactory.getService(sName);
			fail("Ottenuta istanza con chiave non esistente nell'xml e nella map dei servizi");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NullPointerException.class));
		}
	}
	
	@Test
	public void testGetView_TC1()
	{
		String sName = "Registrazione";
		String aprioriView = "/presentation/views/RegistrazioneView.fxml";
		
		String view = appServiceFactory.getView(sName);
		
		assertTrue(aprioriView.equals(view));
	}
	
	@Test
	public void testGetView_TC2()
	{
		String sName = "SignIn";
		try
		{
			String view = appServiceFactory.getView(sName);
			fail("Ottenuta view non esistente nell'xml e nella map");
		}
		catch(Exception e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NullPointerException.class));
		}
	}
	
	@Test
	public void testGetView_TC3()
	{
		String sName = "";
		try
		{
			String view = appServiceFactory.getView(sName);
			fail("Ottenuta view non esistente nell'xml e nella map");
		}
		catch(Exception e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NullPointerException.class));
		}
	}
	
	@Test
	public void testGetView_TC4()
	{
		String sName = null;
		try
		{
			String view = appServiceFactory.getView(sName);
			fail("Ottenuta view non esistente nell'xml e nella map");
		}
		catch(Exception e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NullPointerException.class));
		}
	}

}
