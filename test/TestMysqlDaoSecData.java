package test;

import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.Assertion;
import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

import integration.DaoException;
import integration.NoEntryException;
import integration.mysqldao.MysqlDaoSecData;

public class TestMysqlDaoSecData extends DatabaseTestCase {
	
	private IDataSet loadedDataSet;
	
	@Override
	protected IDatabaseConnection getConnection() throws Exception {
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/CarLoanDB", "carloan", "carloanPwd");
		
		return new DatabaseConnection(con);
		
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/dbseed/secdata-seed.xml"));
		return loadedDataSet;
	}
	
	protected DatabaseOperation getSetUpOperation() throws Exception 
	{
			   return DatabaseOperation.CLEAN_INSERT;
	}
	
	protected DatabaseOperation getTearDownOperation() throws Exception
	{
		return DatabaseOperation.NONE;
	}
	
	/**
	 * Test che verifica cosa succede se si cerca di ottenere il salted hash
	 * di un utente non presente nella tabella. Ci aspettiamo una NoEntryException
	 */
	@Test
	public void testGetUserHash_TC2()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		try
		{
			daoSecData.getUserHash("pl0004");
			fail("ottenuti i dati di un utente non presente in database");
		}
		catch(NoEntryException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	/**
	 * Test che verifica il corretto funzionamento del metodo quando si richiede
	 * l'hash di uno username valido e presente nella tabella del database
	 */
	@Test
	public void testGetUserHash_TC1()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String expected = "ZEJIWXVCWDDVJBUBYFZYCGUMAKATWTOQHXKPZVLRWKYMCDJMYTIVSOAWJWFUROUCTNZIOSN";
		
		String result = daoSecData.getUserHash("cl0003");
		
		assertTrue(result.equals(expected));
	}
	
	/**
	 * Test per verificare cosa accade quando si richiede la password di un utente
	 * la cui username è nulla. Ci aspettiamo una NoEntryException
	 */
	@Test
	public void testGetUserHash_TC3()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		try
		{
			daoSecData.getUserHash(null);
			fail("ottenuti i dati con username invalida");
		}
		catch(NoEntryException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	/**
	 * Test di getUserHash con una username invalida perché maggiore di 20 caratteri
	 */
	@Test
	public void testGetUserHash_TC4()
	{
		String user = "cl0000000000000000001";
		
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		try
		{
			daoSecData.getUserHash(user);
			fail("ottenuti i dati con username invalida");
		}
		catch(NoEntryException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	@Test
	public void testSetUserHash_TC1()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0009";
		String pwd = "OYMYBSNJDLVEHUSRXZCHAPTNPJVPXILRJJYOPAFHURGHUQDAPFPMKMVZBXLGSXHYHUHESSL";
		
		daoSecData.setUserHash(user, pwd);
		
		String retrievedUserHash = daoSecData.getUserHash(user);
		
		assertTrue(retrievedUserHash.equals(pwd));
	}
	
	/**
	 * Tentativo di inserimento di un utente già esistente nel database.
	 * Ci si aspetta una DaoException.
	 */
	@Test
	public void testSetUserHash_TC2()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0001";
		String pwd = "OYMYBSNJDLVEHUSRXZCHAPTNPJVPXILRJJYOPAFHURGHUQDAPFPMKMVZBXLGSXHYHUHESSL";
		
		try
		{
			daoSecData.setUserHash(user, pwd);
			fail("inserimento avvenuto con successo pur essendo già presente una riga con gli stessi dati");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Aggiornamento del campo password con dati di input validi ed utente esistente nella
	 * tabella target del database.
	 */
	@Test
	public void testUpdateUserHash_TC1()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0001";
		String pwd = "OYMYBSNJDLVEHUSRXZCHAPTNPJVPXILRJJYOPAFHURGHUQDAPFPMKMVZBXLGSXHYHUHESSL";
		
		daoSecData.updateUserHash(user, pwd);
		
		String result = daoSecData.getUserHash(user);
		
		assertTrue(result.equals(pwd));
	}
	
	/**
	 * Aggiornamento del campo password di un utente non esistente.
	 */
	@Test
	public void testUpdateUserHash_TC2() throws Exception
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "pl0001";
		String pwd = "OYMYBSNJDLVEHUSRXZCHAPTNPJVPXILRJJYOPAFHURGHUQDAPFPMKMVZBXLGSXHYHUHESSL";
		
		daoSecData.updateUserHash(user, pwd);
		
		ITable queryTable = getConnection().createTable("secdata");
		
		Assertion.assertEquals(loadedDataSet.getTable("secdata"), queryTable);
		
	}
	
	/**
	 * test dell'aggiornamento della password dello user
	 */
	@Test
	public void testUpdateUserHash_TC3() throws Exception
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = null;
		String pwd = "OYMYBSNJDLVEHUSRXZCHAPTNPJVPXILRJJYOPAFHURGHUQDAPFPMKMVZBXLGSXHYHUHESSL";

		daoSecData.updateUserHash(user, pwd);
		
		ITable queryTable = getConnection().createTable("secdata");
		
		Assertion.assertEquals(loadedDataSet.getTable("secdata"), queryTable);
	}
	
	@Test
	public void testUpdateUserHash_TC4()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0001";
		String pwd = null;
		
		try
		{
			daoSecData.updateUserHash(user, pwd);
			fail("aggiornamento avvenuto con successo violando il vincolo not null della password");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Test dell'aggiornamento della password di uno user quando la password
	 * supera il limite di caratteri consentiti.
	 */
	@Test
	public void testUpdateUserHash_TC5()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0001";
		String pwd = "BCBNICSKKCQVUQFHJXLGSTTHOAEXOUHAOZQLYXWAWYITPZNBDCCXEKEKHEWHCHDCXYSRCLPETNDJYZKQHMDGYMGFYTTUZFZUVQOUJPABZATNMHRDMXRBESQDP";
		
		try
		{
			daoSecData.updateUserHash(user, pwd);
			fail("aggiornamento effettuato con successo violando il limite di caratteri");
		}
		catch(DaoException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(DaoException.class));
		}
	}
	
	/**
	 * Test dell'aggiornamento della password quando la username supera il limite consentito
	 * di caratteri e la password è valida. Ci si aspetta che non avvenga alcuna
	 * modifica al database, perché non ci sono
	 * username che corrispondono a quella desiderata.
	 */
	@Test
	public void testUpdateUserHash_TC6() throws Exception
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0000000000000000001";
		String pwd = "OYMYBSNJDLVEHUSRXZCHAPTNPJVPXILRJJYOPAFHURGHUQDAPFPMKMVZBXLGSXHYHUHESSL";

		daoSecData.updateUserHash(user, pwd);
		
		ITable queryTable = getConnection().createTable("secdata");
		
		Assertion.assertEquals(loadedDataSet.getTable("secdata"), queryTable);
	}
	
	/**
	 * rimozione della riga relativa all'utente avente uno username registrato nel db
	 */
	@Test
	public void testDeleteUserHash_TC1()
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0001";
		
		daoSecData.deleteUserHash(user);
		
		try
		{
			daoSecData.getUserHash(user);
			fail("Ottenuta la password dell'utente rimosso");
		}
		catch(NoEntryException e)
		{
			//ignore
		}
	}
	
	/**
	 * rimozione della riga relativa ad un utente la cui username non è presente nel db.
	 * Ci si aspetta che non venga apportata alcuna modifica al database.
	 */
	@Test
	public void testDeleteUserHash_TC2() throws Exception
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "pl0001";
		
		int beforeCount = getConnection().getRowCount("secdata");
		
		daoSecData.deleteUserHash(user);
		
		int afterCount = getConnection().getRowCount("secdata");
		
		assertTrue(beforeCount == afterCount);
	}
	
	@Test
	public void testDeleteUserHash_TC3() throws Exception
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = null;
		
		int beforeCount = getConnection().getRowCount("secdata");
		
		daoSecData.deleteUserHash(user);
		
		int afterCount = getConnection().getRowCount("secdata");
		
		assertTrue(beforeCount == afterCount);
	}
	
	@Test
	public void testDeleteUserHash_TC4() throws Exception
	{
		MysqlDaoSecData daoSecData = new MysqlDaoSecData();
		
		String user = "cl0000000000000000001";
		
		int beforeCount = getConnection().getRowCount("secdata");
		
		daoSecData.deleteUserHash(user);
		
		int afterCount = getConnection().getRowCount("secdata");
		
		assertTrue(beforeCount == afterCount);
	}
}
