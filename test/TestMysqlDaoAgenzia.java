package test;

import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

import business.bo.Lavoratore;
import business.to.AgenziaTO;
import integration.NoEntryException;
import integration.mysqldao.MysqlDaoAgenzia;

public class TestMysqlDaoAgenzia extends DatabaseTestCase {
	
	@Override
	protected IDatabaseConnection getConnection() throws Exception {
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/CarLoanDB", "carloan", "carloanPwd");
		
		return new DatabaseConnection(con);
		
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/dbseed/agenzia-seed.xml"));
	}
	
	protected DatabaseOperation getSetUpOperation() throws Exception 
	{
			   return DatabaseOperation.CLEAN_INSERT;
	}
	
	protected DatabaseOperation getTearDownOperation() throws Exception
	{
		return DatabaseOperation.NONE;
	}
	
	public boolean compareAgenzie(AgenziaTO a1, AgenziaTO a2)
	{
		
		if(a1.getId() == a2.getId() && a1.getNome().equals(a2.getNome()) && 
			a1.getCitta().equals(a2.getCitta()) && a1.getProvincia().equals(a2.getProvincia()) && 
			a1.getVia().equals(a2.getVia()) && a1.getNumCivico().equals(a2.getNumCivico()))
			return true;
		else
			return false;
		
	}
	
	public boolean compareLavList(List<Lavoratore> l1, List<Lavoratore> l2)
	{
		if(l1.size() != l2.size())
			return false;
		else
		{
			boolean retVal = true;
			
			Iterator<Lavoratore> iter1 = l1.iterator();
			Iterator<Lavoratore> iter2 = l2.iterator();
			
			while(iter1.hasNext() && retVal == true)
			{
				Lavoratore lav1 = iter1.next();
				Lavoratore lav2 = iter2.next();
				
				if(!lav1.getUsername().equals(lav2.getUsername()))
					retVal = false;
			}
			
			return retVal;
		}
	}
	
	@Test
	public void testGetAgenzia_TC1()
	{
		MysqlDaoAgenzia daoAgenzia = new MysqlDaoAgenzia();
		
		List<Lavoratore> listLavPriori = new LinkedList<Lavoratore>();
		Lavoratore lavPriori = new Lavoratore();
		lavPriori.setUsername("l0001");
		listLavPriori.add(lavPriori);
		
		AgenziaTO agPriori = new AgenziaTO();
		agPriori.setId(1);
		agPriori.setNome("a1");
		agPriori.setProvincia("BA");
		agPriori.setCitta("Bari");
		agPriori.setVia("Orabona");
		agPriori.setNumCivico("42");
		agPriori.setLavoratori(listLavPriori);
		
		AgenziaTO agRetrieved = daoAgenzia.getAgenzia("a1");
		
		assertTrue(compareAgenzie(agRetrieved, agPriori) && compareLavList(agRetrieved.getLavoratori(), agPriori.getLavoratori()));
		
	}
	
	@Test
	public void testGetAgenzia_TC2()
	{
		MysqlDaoAgenzia daoAgenzia = new MysqlDaoAgenzia();
		
		try
		{
			daoAgenzia.getAgenzia("b1");
			fail("agenzia inesistente ottenuta");
		}
		catch(NoEntryException e)
		{
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	@Test
	public void testGetAgenzia_TC3()
	{
		MysqlDaoAgenzia daoAgenzia = new MysqlDaoAgenzia();
		
		List<Lavoratore> listLavPriori = new LinkedList<Lavoratore>();
		
		AgenziaTO agPriori = new AgenziaTO();
		agPriori.setId(2);
		agPriori.setNome("a2");
		agPriori.setProvincia("BA");
		agPriori.setCitta("Bari");
		agPriori.setVia("Unità");
		agPriori.setNumCivico("24");
		agPriori.setLavoratori(listLavPriori);
		
		AgenziaTO agRetrieved = daoAgenzia.getAgenzia("a2");
		
		assertTrue(compareAgenzie(agRetrieved, agPriori) && compareLavList(agRetrieved.getLavoratori(), agPriori.getLavoratori()));
	}

}
