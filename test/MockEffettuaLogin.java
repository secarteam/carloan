package test;

import java.util.List;

import business.OperazioneNonCompletata;
import business.appservice.AppServiceFactory;
import business.appservice.CryptoInt;
import business.bo.Cliente;
import business.bo.Lavoratore;
import integration.DaoException;
import integration.NoEntryException;
import integration.factories.DaoFactory;
import integration.interfaces.DaoLavoratore;
import integration.interfaces.DaoSecData;
import presentation.Logger;
import presentation.UserRole;

public class MockEffettuaLogin{

	private String username;
	private String password;

	
	public Object execute() throws Exception {

		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		Logger logger = Logger.getInstance();
		UserRole backupRole = logger.getUserRole();
		String backupUsername = logger.getUsername();

		try
		{
			//DaoCliente daoCliente = daoFactory.getDaoCliente();
			DaoLavoratore daoLavoratore = daoFactory.getDaoLavoratore();
			DaoSecData daoSecData = daoFactory.getDaoSecData();
			AppServiceFactory appFactory = AppServiceFactory.getInstance();
			CryptoInt cryptoApp = appFactory.getEncryptionService("Encryption");

			Cliente cliente = new Cliente();
			cliente.setUsername(username);
			Lavoratore lavoratore = new Lavoratore();
			lavoratore.setUsername(username);


			String dbPwd = daoSecData.getUserHash(username);

			if(cryptoApp.verify(password, dbPwd))
			{
				if(cliente.isClienteInStorageByUsername())
				{
					logger.setUserRole(UserRole.CLIENT);
					logger.setUsername(username);
				}
				else
				{
					logger.setUsername(username);

					lavoratore = new Lavoratore(daoLavoratore.getLavoratore(username));

					switch(lavoratore.getRuolo())
					{
					case CAPOAGENZIA:
						logger.setUserRole(UserRole.CHIEF);
						break;
					case CAPODITTA:
						logger.setUserRole(UserRole.HEADCHIEF);
						break;
					case OPERATOREAZIENDA:
						logger.setUserRole(UserRole.OPERATOR);
						break;
					default:
						logger.setUserRole(backupRole);
						logger.setUsername(backupUsername);
						break;

					}
				}
			}
			else
			{
				throw new NoEntryException();
			}

		}
		catch(DaoException | OperazioneNonCompletata e)
		{
			logger.setUserRole(backupRole);
			logger.setUsername(backupUsername);

			throw e;
		}
		catch(NoEntryException e)
		{
			logger.setUserRole(backupRole);
			logger.setUsername(backupUsername);

			NoEntryException noEntry = new NoEntryException("Username o password errate");

			throw e;
		}
		return null;
	}

	public void init(List<Object> parameters) {

		username = (String) parameters.get(0);
		password = (String) parameters.get(1);

	}

}
