package test;

import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

import integration.NoEntryException;
import presentation.Logger;
import presentation.UserRole;

public class TestEffettuaLogin extends DatabaseTestCase {
	
private IDataSet loadedDataSet;
	
	@Override
	protected IDatabaseConnection getConnection() throws Exception {
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/CarLoanDB", "carloan", "carloanPwd");
		return new DatabaseConnection(con);
		
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/dbseed/login-seed.xml"));
		return loadedDataSet;
	}
	
	protected DatabaseOperation getSetUpOperation() throws Exception 
	{
			   return DatabaseOperation.CLEAN_INSERT;
	}
	
	protected DatabaseOperation getTearDownOperation() throws Exception
	{
		return DatabaseOperation.NONE;
	}
	
	public MockEffettuaLogin getService(List<Object> parameters)
	{
		MockEffettuaLogin loginService = new MockEffettuaLogin();
		loginService.init(parameters);
		
		return loginService;
	}
	
	@Test
	public void testEffettuaLogin_TC1()
	{
		List<Object> parameters = new ArrayList<Object>();
		Collections.addAll(parameters, "pippo", "pwd1234");
		
		MockEffettuaLogin loginService = getService(parameters);
		
		Logger logger = Logger.getInstance();
		logger.setUsername("none");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		
		try {
			loginService.execute();
			assertTrue(logger.getUsername().equals("pippo"));
		} catch (Exception e) {
			fail("Generata eccezione");
		}
	}
	
	@Test
	public void testEffettuaLogin_TC2()
	{
		List<Object> parameters = new ArrayList<Object>();
		Collections.addAll(parameters, "carmack", "pwd1234");
		
		MockEffettuaLogin loginService = getService(parameters);
		
		Logger logger = Logger.getInstance();
		logger.setUsername("none");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		
		try {
			loginService.execute();
			fail("Login avvenuto pur non avendo la username in database");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	@Test
	public void testEffettuaLogin_TC3()
	{
		List<Object> parameters = new ArrayList<Object>();
		Collections.addAll(parameters, "pippo", "dwp1234");
		
		MockEffettuaLogin loginService = getService(parameters);
		
		Logger logger = Logger.getInstance();
		logger.setUsername("none");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		
		try {
			loginService.execute();
			fail("Login avvenuto con password errata");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	@Test
	public void testEffettuaLogin_TC4()
	{
		List<Object> parameters = new ArrayList<Object>();
		Collections.addAll(parameters, "pippooooooooooooooooo", "pwd1234");
		
		MockEffettuaLogin loginService = getService(parameters);
		
		Logger logger = Logger.getInstance();
		logger.setUsername("none");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		
		try {
			loginService.execute();
			fail("Login avvenuto pur non avendo la username in database");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	@Test
	public void testEffettuaLogin_TC5()
	{
		List<Object> parameters = new ArrayList<Object>();
		Collections.addAll(parameters, null, "pwd1234");
		
		MockEffettuaLogin loginService = getService(parameters);
		
		Logger logger = Logger.getInstance();
		logger.setUsername("none");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		
		try {
			loginService.execute();
			fail("Login avvenuto pur non avendo la username in database");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	@Test
	public void testEffettuaLogin_TC6()
	{
		List<Object> parameters = new ArrayList<Object>();
		Collections.addAll(parameters, "pippo", "");
		
		MockEffettuaLogin loginService = getService(parameters);
		
		Logger logger = Logger.getInstance();
		logger.setUsername("none");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		
		try {
			loginService.execute();
			fail("Login avvenuto con password errata");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NoEntryException.class));
		}
	}
	
	@Test
	public void testEffettuaLogin_TC7()
	{
		List<Object> parameters = new ArrayList<Object>();
		Collections.addAll(parameters, "pippo", null);
		
		MockEffettuaLogin loginService = getService(parameters);
		
		Logger logger = Logger.getInstance();
		logger.setUsername("none");
		logger.setUserRole(UserRole.ANONYMOUS);
		
		
		try {
			loginService.execute();
			fail("Login avvenuto pur non avendo la username in database");
		} catch (Exception e) {
			assertThat(e, IsInstanceOf.instanceOf(NullPointerException.class));
		}
	}

}
