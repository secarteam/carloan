package presentation;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ViewMapping {
	
	public static final String startView = ""; //TODO: definire un fxml della schermata iniziale
	
	public static Map<UserRole, String> homeView = new EnumMap<UserRole, String>(UserRole.class);
	public static Map<String, Class> viewStringToControllerClass = new HashMap<String, Class>();
	
	static
	{
		homeView.put(UserRole.CLIENT, "/presentation/views/HomeCliente.fxml");
		homeView.put(UserRole.ANONYMOUS, "/presentation/views/AnonymousHome.fxml");
		homeView.put(UserRole.OPERATOR, "/presentation/views/OperatoreAziendaHome.fxml");
		homeView.put(UserRole.CHIEF, "/presentation/views/CapoAgenziaHome.fxml");
		homeView.put(UserRole.HEADCHIEF, "/presentation/views/CapoDittaHome.fxml");
	}
	

}
