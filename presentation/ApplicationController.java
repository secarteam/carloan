package presentation;

import java.util.List;

import business.OperazioneNonCompletata;
import business.appservice.AppServiceFactory;
import business.appservice.AppServiceInt;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;


/**
 * Implementa {@link ApplicationControllerInt} e ne definise i metodi.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */


public class ApplicationController implements ApplicationControllerInt {

	public ApplicationController() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ottiene l'Application Service (interfaccia {@link business.appservice.AppServiceInt AppServiceInt})
	 * che realizza il servizio richiesto, utilizzando {@link business.appservice.AppServiceFactory AppServiceFactory},
	 * gli passa i parametri invocanone il metodo init() e
	 * invoca il metodo execute(), catturando e gestendo le eventuali
	 * eccezioni.
	 * 
	 * @param serviceName	nome del servizio richiesto
	 * @param parameters	lista di parametri di input da passare all'Application Service
	 * @return				oggetto che rappresenta il dato di ritorno della computazione
	 * (tipicamente null).
	 * @see business.appservice.AppServiceFactory
	 * @see business.appservice.AppServiceInt
	 */
	@Override
	public Object handleRequest(String serviceName, List<Object> parameters) {
		
		AppServiceFactory appServiceFactory = AppServiceFactory.getInstance();
		Object returnObj = null;
		
		try {
			AppServiceInt appService = appServiceFactory.getService(serviceName);
			appService.init(parameters);
			returnObj = appService.execute();
			
		} catch (OperazioneNonCompletata e) {
			ErrorHandlerInt errorWindow = new ErrorHandler(e);
			errorWindow.handleError();
		}
		
		return returnObj;
	}

}
