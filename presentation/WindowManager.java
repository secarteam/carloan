package presentation;

import java.io.IOException;
import java.util.List;

import business.OperazioneNonCompletata;
import business.appservice.AppServiceFactory;
import errorhandling.ErrorHandler;
import errorhandling.ErrorHandlerInt;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import presentation.views.controllers.ControllerInt;

public class WindowManager {
	
//	public void startApplication()
//	{
//		//TODO: definire con cosa avviare l'applicazione
//	}
	
	public Stage createNewStage(String viewPath, String title, List<Object> parameters, boolean resizable, Modality modality)
	{
		Stage stage = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource(viewPath));
		
		try {
			stage.setScene(new Scene((Pane) loader.load()));
			
			ControllerInt controller = (ControllerInt) loader.getController();
			
			controller.init(parameters);
			
			stage.initModality(modality);
			stage.setTitle(title);
			stage.setResizable(resizable);
			
		} catch (IOException e) {
			OperazioneNonCompletata opExc = new OperazioneNonCompletata(e);
			ErrorHandlerInt errorWindow = new ErrorHandler(opExc);
			errorWindow.handleError();
		}
		
		return stage;
	}
	
	public void updateStageFromViewPath(Stage stage, String viewPath, String title, List<Object> parameters, boolean resizable)
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource(viewPath));
		
		try {
			stage.setScene(new Scene((Pane) loader.load()));
			
			ControllerInt controller = (ControllerInt) loader.getController();
			
			controller.init(parameters);
			
		} catch (IOException e) {
			OperazioneNonCompletata opExc = new OperazioneNonCompletata(e);
			ErrorHandlerInt errorWindow = new ErrorHandler(opExc);
			errorWindow.handleError();
		}
		
	}
	
	public void changeStageFromServiceName(Stage stage, String service, String title, List<Object> parameters, boolean resizable)
	{
		AppServiceFactory appServiceFactory = AppServiceFactory.getInstance();
		
		String view = appServiceFactory.getView(service);
		
		updateStageFromViewPath(stage, view, title, parameters, resizable);
		
		
	}
	
	public Stage createNewStageFromService(String service, String title, List<Object> parameters, boolean resizable, Modality modality)
	{
		AppServiceFactory appServiceFactory = AppServiceFactory.getInstance();
		
		String view = appServiceFactory.getView(service);
		
		return createNewStage(view, title, parameters, resizable, modality);
	}

}
