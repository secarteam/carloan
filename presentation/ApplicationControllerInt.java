package presentation;

import java.util.List;

/**
 * Interfaccia per le classi di tipo Application Controller.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public interface ApplicationControllerInt {	
	
	public Object handleRequest(String serviceName, List<Object> parameters);
	
}
