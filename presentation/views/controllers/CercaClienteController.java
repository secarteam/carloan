package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import business.bo.Cliente;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import presentation.FrontController;
import presentation.Logger;
import presentation.Main;
import presentation.UserRole;
import presentation.ViewMapping;
import presentation.WindowManager;

public class CercaClienteController implements ControllerInt, Initializable {

	@FXML
	private TextField emailField;

	@FXML
	private TextField nomeField;

	@FXML
	private TextField cognomeField;

	@FXML
	private TextField codiceFiscaleField;

//	@FXML
//	private TextField passwordField;

	@FXML
	private TextField usernameField;

	@FXML
	private Button cercaClienteButton;

	@FXML
	private Button annullaButton;

	@FXML
	private Button logAsClienteButton;

	private Logger logger;


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		logger = Logger.getInstance();

	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub

	}

	private boolean validateCercaButton()
	{
		if(emailField.getText() == null || emailField.getText().trim().isEmpty())
			return false;
		else
			return true;
	}

	private boolean validateOverrideButton()
	{
		if(usernameField.getText() == null || usernameField.getText().trim().isEmpty())
			return false;
		else
			return true;
	}

	@FXML
	private void handleCercaButton()
	{
		if(validateCercaButton())
		{
			FrontController frontController = new FrontController();

			List<Object> parameters = new ArrayList<Object>();

			parameters.add(emailField.getText());

			Cliente retCliente = (Cliente) frontController.processRequest("OttieniDatiCliente", parameters);

			usernameField.setText(retCliente.getUsername());
			nomeField.setText(retCliente.getNome());
			cognomeField.setText(retCliente.getCognome());
			codiceFiscaleField.setText(retCliente.getCodiceFiscale());
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare il campo relativo all'email");

			alert.showAndWait();
		}
	}

	@FXML
	private void handleLogAsButton()
	{
		if(validateOverrideButton())
		{
			logger.setUsername(usernameField.getText());
			logger.setUserRole(UserRole.CLIENT);

			WindowManager winManager = new WindowManager();
			winManager.updateStageFromViewPath(Main.getPrimaryStage(), ViewMapping.homeView.get(logger.getUserRole()), "CarLoan", null, true);

			Stage stage = (Stage) logAsClienteButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare il campo relativo all'email e cercare un cliente");

			alert.showAndWait();
		}
	}

	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}


}
