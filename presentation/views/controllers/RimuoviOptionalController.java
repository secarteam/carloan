package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import business.to.OptionalTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoOptional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;
import presentation.Main;
import presentation.ViewMapping;
import presentation.WindowManager;

public class RimuoviOptionalController implements ControllerInt, Initializable {

	@FXML
	private ComboBox nomebox;

	@FXML
	private Button rimuoviB;

	@FXML
	private Button annullaButton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoOptional = daoFactory.getDaoOptional();

		List<OptionalTO> listOpt = daoOptional.getOptional();

		ObservableList<String> options = FXCollections.observableArrayList();

		for(OptionalTO oTO : listOpt)
		{
				String nome = oTO.getNome();
				options.add(nome);
		}

		nomebox.setItems(options);
	}

	@Override
	public void init(List<Object> parameters) {

	}

	private boolean validate(){
		if(nomebox.getValue() != null)
			return true;
		else
			return false;
	}

	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void handleRimuoviButton()
	{
		if(validate()){
			FrontController frontController = new FrontController();

			List<Object> parameters = new ArrayList<Object>();

			Collections.addAll(parameters, (String) nomebox.getValue());

			frontController.processRequest("RimuoviOptional", parameters);

			Stage stage = (Stage) rimuoviB.getScene().getWindow();
			stage.close();
		}
		else{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Impossibile Rimuovere");

			alert.showAndWait();
		}
	}

}
