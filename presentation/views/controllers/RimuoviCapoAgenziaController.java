package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import business.bo.Lavoratore;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;

public class RimuoviCapoAgenziaController implements ControllerInt, Initializable {
	
	@FXML
	private Button okButton;
	
	@FXML
	private TextField nomeField;
	
	@FXML
	private TextField cognomeField;
	
	@FXML
	private TextField mailField;
	
	@FXML
	private Button annullaButton;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean validateData()
	{
		if(nomeField.getText() == null || cognomeField.getText() == null || 
				mailField.getText() == null ||
				nomeField.getText().trim().isEmpty() ||
				cognomeField.getText().trim().isEmpty() || 
				mailField.getText().trim().isEmpty())
		{
			return false;
		}
		else
			return true;
	}
	
	@FXML
	private void handleOkButton()
	{
		if(validateData() == true)
		{
			String nome = nomeField.getText();
			String cognome = cognomeField.getText();
			String mail = mailField.getText();
			
			
			Lavoratore lavoratore = new Lavoratore();
			lavoratore.setNome(nome);
			lavoratore.setCognome(cognome);
			lavoratore.setEmail(mail);
			
			List<Object> parameters = new ArrayList<Object>();
			
			parameters.add(lavoratore);
			
			FrontController frontController = new FrontController();
			
			frontController.processRequest("RimuoviCapoAgenzia", parameters);
			
			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
			
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		
		stage.close();
	}

}
