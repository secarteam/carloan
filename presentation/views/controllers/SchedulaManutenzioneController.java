package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import integration.factories.DaoFactory;
import integration.interfaces.DaoVettura;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;

public class SchedulaManutenzioneController implements ControllerInt, Initializable {

	@FXML
	private TextField tbox;

	@FXML
	private Button imButton;

	@FXML
	private Button annullaButton;

	@FXML
	private RadioButton straoRadio;

	@FXML
	private RadioButton ordRadio;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}

	@Override
	public void init(List<Object> parameters) {

	}

	private boolean validateData(){
		if(tbox.getText().isEmpty())
			return false;
		else
			return true;
	}

	@FXML
	private void handleConfermaButton(){
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura dv = daoFactory.getDaoVettura();

		if(tbox.getText().equals(dv.getVettura(tbox.getText()).getTarga()) || validateData())
		{
			List<Object> parameters = new ArrayList<Object>();
			parameters.add(tbox.getText());
			String s = null;

			if(straoRadio.isSelected())
				s = "STRAORDINARIA";
			else
				s = "ORDINARIA";

			parameters.add(s);

			FrontController frontController = new FrontController();

			frontController.processRequest("SchedulaManutenzione", parameters);

			Stage stage = (Stage) imButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Targa sbagliata o campo vuoto");

			alert.showAndWait();
		}
	}


	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}
}
