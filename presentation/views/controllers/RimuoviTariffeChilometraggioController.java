package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import business.to.FasciaTO;
import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;

public class RimuoviTariffeChilometraggioController implements Initializable, ControllerInt {

	@FXML
	private ComboBox nomeTbox;

	@FXML
	private ComboBox nomeFbox;

	@FXML
	private Button annullaButton;

	@FXML
	private Button RimuoviButton;

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
	     DaoFascia daoFascia = daoFactory.getDaoFascia();

	     DaoTariffaChilometraggio daoTT = daoFactory.getDaoTariffaChilometraggio();

	     List<TariffaChilometraggioTO> tt = daoTT.getTariffeChilometraggio();
	     ObservableList<String> optF = FXCollections.observableArrayList();

	     for(TariffaChilometraggioTO ttTO : tt)
	     {
				String nome = ttTO.getNome();
				optF.add(nome);
	     }

	     nomeTbox.setItems(optF);

	     List<FasciaTO> fasceTO = daoFascia.getFasce();
	     optF = FXCollections.observableArrayList();


		 for(FasciaTO fTO : fasceTO)
		 {
			String nome = fTO.getNome();
			optF.add(nome);
		 }

		 nomeFbox.setItems(optF);
	}

	private boolean validateData(){
		if(nomeTbox.getValue() == null || nomeFbox.getValue() == null)
			return false;
		else
			return true;
	}

	@FXML
	private void handleConfermaButton(){
		if(validateData())
		{
			List<Object> parameters = new ArrayList<Object>();

			parameters.add((String) nomeTbox.getValue());
			parameters.add((String) nomeFbox.getValue());
			FrontController frontController = new FrontController();

			frontController.processRequest("RimuoviTariffaChilometraggio", parameters);

			Stage stage = (Stage) RimuoviButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}


	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}


}
