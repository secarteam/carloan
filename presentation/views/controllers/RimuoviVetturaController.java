package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import presentation.FrontController;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RimuoviVetturaController implements ControllerInt, Initializable {

	@FXML
	private TextField targaField;
	
	@FXML
	private Button okButton;
	
	@FXML
	private Button annullaButton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}
	
	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		
		stage.close();
	}
	
	@FXML
	private void handleOkButton()
	{
		if(validateData())
		{
			String targa = targaField.getText();
			
			List<Object> parameters = new ArrayList<Object>();
			
			parameters.add(targa);
			
			FrontController frontController = new FrontController();
			
			frontController.processRequest("RimuoviVettura", parameters);
			
			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}
	
	private boolean validateData()
	{
		if(targaField.getText() == null || targaField.getText().trim().isEmpty())
			return false;
		else
			return true;
	}

}
