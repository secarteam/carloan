/**
 * Contiene i controller delle varie view in JavaFX
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */
package presentation.views.controllers;