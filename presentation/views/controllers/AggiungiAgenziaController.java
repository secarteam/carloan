package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import presentation.FrontController;

public class AggiungiAgenziaController implements ControllerInt, Initializable {
	
	@FXML
	private TextField nomeField;
	
	@FXML
	private TextField provinciaField;
	
	@FXML
	private TextField cittaField;
	
	@FXML
	private TextField viaField;
	
	@FXML
	private TextField numCivicoField;
	
	@FXML
	private Button okButton;
	
	@FXML
	private Button annullaButton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean validateData()
	{
		if(nomeField.getText() == null || nomeField.getText().trim().isEmpty() || 
			provinciaField.getText() == null || provinciaField.getText().trim().isEmpty() ||
			viaField.getText() == null || viaField.getText().trim().isEmpty() ||
			numCivicoField.getText() == null || numCivicoField.getText().trim().isEmpty() ||
			cittaField.getText() == null || cittaField.getText().trim().isEmpty())
		{
			return false;
		}
		else
			return true;
	}
	
	@FXML
	private void handleOkButton()
	{
		if(validateData())
		{
			String nome = nomeField.getText();
			String citta = cittaField.getText();
			String provincia = provinciaField.getText();
			String via = viaField.getText();
			String numCivico = numCivicoField.getText();
			
			List<Object> parameters = new ArrayList<Object>();
			
			Collections.addAll(parameters, nome, provincia, citta, via, numCivico);
			
			FrontController frontController = new FrontController();
			frontController.processRequest("AggiungiAgenzia", parameters);
			
			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}

}
