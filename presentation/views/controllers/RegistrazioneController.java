package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;
import presentation.UserRole;

public class RegistrazioneController implements ControllerInt, Initializable {
	
	@FXML
	private TextField nomeField;
	
	@FXML
	private TextField cognomeField;
	
	@FXML
	private TextField usernameField;
	
	@FXML
	private TextField mailField;
	
	@FXML
	private TextField passwordField;
	
	@FXML
	private TextField codiceFiscaleField;
	
	@FXML
	private TextField numTelefonoField;
	
	@FXML
	private Button okButton;
	
	@FXML
	private Button annullaButton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}
	
	@FXML
	private void handleOkButton()
	{
		if(validateData())
		{
			String username = usernameField.getText();
			String email = mailField.getText();
			String nome = nomeField.getText();
			String cognome = cognomeField.getText();
			String numTelefono = numTelefonoField.getText();
			String codiceFiscale = codiceFiscaleField.getText();
			String password = passwordField.getText();
			
			List<Object> parameters = new ArrayList<Object>();
			
			Collections.addAll(parameters, username, email, nome, cognome, numTelefono, codiceFiscale, password);
			
			FrontController frontController = new FrontController();
			
			frontController.processRequest("Registrazione", parameters);
			
			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}
	
	private boolean validateData()
	{
		if(nomeField.getText() == null || cognomeField.getText() == null || usernameField.getText() == null || 
				mailField.getText() == null || passwordField.getText() == null ||
				nomeField.getText().trim().isEmpty() ||
				cognomeField.getText().trim().isEmpty() || usernameField.getText().trim().isEmpty() ||
				mailField.getText().trim().isEmpty() || passwordField.getText().trim().isEmpty() ||
				numTelefonoField.getText() == null || numTelefonoField.getText().trim().isEmpty() ||
				codiceFiscaleField.getText() == null || codiceFiscaleField.getText().trim().isEmpty())
		{
			return false;
		}
		else
			return true;
	}

}
