package presentation.views.controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import business.to.OptionalTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoOptional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;
import presentation.Logger;
import presentation.Main;
import presentation.UserRole;
import presentation.ViewMapping;
import presentation.WindowManager;

public class ScegliOptionalController implements ControllerInt, Initializable {

	private String vettura;
	private LocalDate ritiroData;
	private LocalDate consegnaData;
	private String tarTemp;
	private String tarChil;
	private String puntoConsegna;

	@FXML
	private ComboBox optBox1;

	@FXML
	private ComboBox optBox2;

	@FXML
	private Slider optQuantity1;

	@FXML
	private Slider optQuantity2;

	@FXML
	private TextField saldoField;

	@FXML
	private Button noleggiaButton;

	@FXML
	private Button annullaButton;

	private Logger logger;

	private double preventivo;

	private Map<String, Integer> optionalScelti;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		logger = Logger.getInstance();
		preventivo = 0.0;
		optionalScelti = new HashMap<String, Integer>();

		//initialize optional combobox
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoOptional = daoFactory.getDaoOptional();

		List<OptionalTO> listOpt = daoOptional.getOptional();

		ObservableList<String> options = FXCollections.observableArrayList();

		for(OptionalTO oTO : listOpt)
		{
			if(oTO.getQuantitaDisponibile() != 0)
			{
				String nome = oTO.getNome();
				options.add(nome);
			}
		}

		optBox1.setItems(options);
		optBox2.setItems(options);

	}

	@FXML
	private void handleOptBox1()
	{
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoOptional = daoFactory.getDaoOptional();

		if(optBox1.getValue() != null)
		{
			optQuantity1.setMin(0);
			OptionalTO oTO = daoOptional.getOptional((String) optBox1.getValue());
			optQuantity1.setMax(oTO.getQuantitaDisponibile());
		}
	}

	@FXML
	private void handleOptBox2()
	{
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoOptional daoOptional = daoFactory.getDaoOptional();

		if(optBox2.getValue() != null)
		{
			optQuantity2.setMin(0);
			OptionalTO oTO = daoOptional.getOptional((String) optBox2.getValue());
			optQuantity2.setMax(oTO.getQuantitaDisponibile());
		}
	}

	@Override
	public void init(List<Object> parameters) {

		vettura = (String) parameters.get(0);
		ritiroData = (LocalDate) parameters.get(1);
		consegnaData = (LocalDate) parameters.get(2);
		tarTemp = (String) parameters.get(3);
		tarChil = (String) parameters.get(4);
		puntoConsegna = (String) parameters.get(5);
	}

	@FXML
	private void handleCalcolaPreventivo()
	{
		FrontController frontController = new FrontController();
		optionalScelti.clear();

		List<Object> parameters = new ArrayList<Object>();
		chooseOptional(optBox1, optQuantity1, optionalScelti);
		chooseOptional(optBox2, optQuantity2, optionalScelti);

		Collections.addAll(parameters, vettura, logger.getUsername(), ritiroData, consegnaData, 0.0, tarChil, tarTemp, optionalScelti);

		preventivo = (double) frontController.processRequest("CalcolaPreventivo", parameters);

		saldoField.setText(Double.toString(preventivo) + "€");
	}

	private void chooseOptional(ComboBox box, Slider slider, Map<String, Integer> map)
	{
		if(box.getValue() != null && slider.getValue() != 0.0)
		{
			map.put( (String) box.getValue(), (int) slider.getValue());
		}
	}

	@FXML
	private void handleNoleggiaButton()
	{
		Logger logStatus = Logger.getInstance();
		if(validateOpt() && logStatus.getUserRole().equals(UserRole.CLIENT)){
			FrontController frontController = new FrontController();

			List<Object> parameters = new ArrayList<Object>();

			Collections.addAll(parameters, vettura, logger.getUsername(), ritiroData, consegnaData, preventivo, tarChil, tarTemp, optionalScelti, 0.0, puntoConsegna);

			frontController.processRequest("Noleggia", parameters);

			Stage stage = (Stage) noleggiaButton.getScene().getWindow();
			stage.close();

			WindowManager winManager = new WindowManager();
			winManager.updateStageFromViewPath(Main.getPrimaryStage(), ViewMapping.homeView.get(logger.getUserRole()), "CarLoan", null, true);
		}
		else{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Scelta degli optional errati oppure autenticarsi come cliente");

			alert.showAndWait();
		}
	}

	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}

	@FXML
	private boolean validateOpt(){
		if(optBox1.getValue() == null || optBox2.getValue() == null || optBox1.getValue() != optBox2.getValue())
			return true;
		else
			return false;
	}

}
