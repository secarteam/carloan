package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import business.bo.Vettura;
import business.to.AgenziaTO;
import business.to.VetturaTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoVettura;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;

public class RendiNoleggiabileController implements ControllerInt, Initializable {

	@FXML
	private ComboBox tbox;

	@FXML
	private Button imButton;

	@FXML
	private Button annullaButton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura daoVettura = daoFactory.getDaoVettura();
		List<VetturaTO> lv = new LinkedList<VetturaTO>();

		lv = daoVettura.getVetture();

		Iterator<VetturaTO> vetIter = lv.iterator();
		ObservableList<String> options = FXCollections.observableArrayList();

		while(vetIter.hasNext()){
			Vettura v = new Vettura(vetIter.next());
			if(v.isInManutenzione())
					options.add(v.getTarga());
		}

		tbox.setItems(options);
	}

	@Override
	public void init(List<Object> parameters) {

	}

	private boolean validateData(){
		if(tbox.getValue()==null)
			return false;
		else
			return true;
	}

	@FXML
	private void handleConfermaButton(){
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoVettura dv = daoFactory.getDaoVettura();

		if(tbox.getValue().equals(dv.getVettura((String)tbox.getValue()).getTarga()) || validateData())
		{
			List<Object> parameters = new ArrayList<Object>();
			parameters.add(tbox.getValue());
			FrontController frontController = new FrontController();

			frontController.processRequest("RendiNoleggiabile", parameters);

			Stage stage = (Stage) imButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Targa sbagliata o campo vuoto");

			alert.showAndWait();
		}
	}


	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}
}
