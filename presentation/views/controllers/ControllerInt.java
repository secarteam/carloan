package presentation.views.controllers;

import java.util.List;

public interface ControllerInt {
	
	public void init(List<Object> parameters);

}
