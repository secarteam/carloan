package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import business.to.FasciaTO;
import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoFascia;
import integration.interfaces.DaoTariffaChilometraggio;
import integration.interfaces.DaoTariffaTemporale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import presentation.FrontController;

public class InserisciTariffeChilometraggioController implements Initializable, ControllerInt {

	@FXML
	private ComboBox nomebox;

	@FXML
	private TextField descTField;

	@FXML
	private TextField costoTField;

	@FXML
	private ComboBox fasciabox;

	@FXML
	private Button okButton;

	@FXML
	private Button annullaButton;

	@Override
	public void init(List<Object> parameters) {

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		 DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
	     DaoFascia daoFascia = daoFactory.getDaoFascia();
	     DaoTariffaChilometraggio daoTC = daoFactory.getDaoTariffaChilometraggio();
	     DaoTariffaTemporale daoTT = daoFactory.getDaoTariffaTemporale();

	    	 List<TariffaChilometraggioTO> tc = daoTC.getTariffeChilometraggio();
	    	 ObservableList<String> optF = FXCollections.observableArrayList();

	    	 for(TariffaChilometraggioTO tcTO : tc)
			 {
				String nome = tcTO.getNome();
				optF.add(nome);
			 }

			 nomebox.setItems(optF);

			 if(nomebox.getValue() != null)
				 descTField.setText(daoTC.getTariffaChilometraggio((String)nomebox.getValue()).getDescrizione());


	     List<FasciaTO> fasceTO = daoFascia.getFasce();
		 optF = FXCollections.observableArrayList();

		 for(FasciaTO fTO : fasceTO)
		 {
			String nome = fTO.getNome();
			optF.add(nome);
		 }

		 fasciabox.setItems(optF);

	}

	private boolean validateData(){
		if(nomebox.getValue() == null || costoTField.getText().isEmpty() || fasciabox.getValue() == null)
			return false;
		else
			return true;
	}

	@FXML
	private void handleConfermaButton(){
		if(validateData())
		{
			List<Object> parameters = new ArrayList<Object>();

			parameters.add((String) nomebox.getValue());
			parameters.add(descTField.getText());
			parameters.add((String)fasciabox.getValue());
			parameters.add(Double.valueOf(costoTField.getText()));
			FrontController frontController = new FrontController();

			frontController.processRequest("AggiungiTariffaChilometraggio", parameters);

			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}


	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}

}
