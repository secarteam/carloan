package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import business.to.AgenziaTO;
import business.to.FasciaTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoFascia;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import presentation.FrontController;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class AggiungiVetturaController implements ControllerInt, Initializable {

	@FXML
	private TextField targaField;

	@FXML
	private TextField modelloField;

	@FXML
	private TextField casaProduttriceField;

	@FXML
	private ComboBox fasciaBox;

	@FXML
	private ComboBox agenziaBox;

	@FXML
	private RadioButton OrdButton;

	@FXML
	private RadioButton StraoButton;

	@FXML
	private RadioButton noButton;

	@FXML
	private Button annullaButton;

	@FXML
	private Button okButton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(List<Object> parameters) {

		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoFascia daoFascia = daoFactory.getDaoFascia();

		List<FasciaTO> fasce = daoFascia.getFasce();

		ObservableList<String> options = FXCollections.observableArrayList();

		for(FasciaTO fto : fasce)
		{
			String ftoName = fto.getNome();
			options.add(ftoName);
		}

		fasciaBox.setItems(options);

		DaoAgenzia daoAgenzia = daoFactory.getDaoAgenzia();

		List<AgenziaTO> agenzieTO = daoAgenzia.getAgenzie();

		ObservableList<String> options2 = FXCollections.observableArrayList();

		for (AgenziaTO aTO : agenzieTO)
		{
			String nome = aTO.getNome();
			options2.add(nome);
		}

		agenziaBox.setItems(options2);

	}

	private boolean validateData()
	{
		if(targaField.getText() == null || targaField.getText().trim().isEmpty() ||
			modelloField.getText() == null || modelloField.getText().trim().isEmpty() ||
			casaProduttriceField.getText() == null || casaProduttriceField.getText().trim().isEmpty() ||
			fasciaBox.getValue() == null || agenziaBox.getValue() == null)
		{
			return false;
		}
		else
			return true;
	}

	@FXML
	private void handleOkButton()
	{
		if(validateData() == true)
		{
			String targa = targaField.getText();
			String modello = modelloField.getText();
			String fascia = (String) fasciaBox.getValue();
			String agenzia = (String) agenziaBox.getValue();
			String casaProduttrice = casaProduttriceField.getText();
			String manutenzione = null;

			if(OrdButton.isSelected())
				manutenzione = "ORDINARIA";
			if(StraoButton.isSelected())
				manutenzione = "STRAORDINARIA";
			if(noButton.isSelected())
				manutenzione = "NO";

			List<Object> parameters = new ArrayList<Object>();

			parameters.add(targa);
			parameters.add(modello);
			parameters.add(fascia);
			parameters.add(agenzia);
			parameters.add(casaProduttrice);
			parameters.add(manutenzione);

			FrontController frontController = new FrontController();

			frontController.processRequest("AggiungiVettura", parameters);

			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}

	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();

		stage.close();
	}

}
