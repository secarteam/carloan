package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import business.bo.Agenzia;
import business.bo.Lavoratore;
import business.to.AgenziaTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import presentation.FrontController;
import presentation.Logger;

public class InserisciCapoAgenziaController implements ControllerInt, Initializable {
	
	@FXML
	private Pane agenziaPane;
	
	@FXML
	private ComboBox agenzieBox;
	
	@FXML
	private Button okButton;
	
	@FXML
	private TextField nomeField;
	
	@FXML
	private TextField cognomeField;
	
	@FXML
	private TextField usernameField;
	
	@FXML
	private TextField mailField;
	
	@FXML
	private TextField passwordField;
	
	@FXML
	private Button annullaButton;
	
	private Logger log;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		log = Logger.getInstance();
		
		
		agenziaPane.setVisible(true);
		
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoAgenzia daoAgenzia = daoFactory.getDaoAgenzia();
		
		List<AgenziaTO> agenzieTO = daoAgenzia.getAgenzie();
		
		ObservableList<String> options = FXCollections.observableArrayList();
		
		for (AgenziaTO aTO : agenzieTO)
		{
			String nome = aTO.getNome();
			options.add(nome);
		}
		
		agenzieBox.setItems(options);
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean validateData()
	{
		if(nomeField.getText() == null || cognomeField.getText() == null || usernameField.getText() == null || 
				mailField.getText() == null || passwordField.getText() == null ||
				nomeField.getText().trim().isEmpty() ||
				cognomeField.getText().trim().isEmpty() || usernameField.getText().trim().isEmpty() ||
				mailField.getText().trim().isEmpty() || passwordField.getText().trim().isEmpty() ||
				agenzieBox.getValue() == null)
		{
			return false;
		}
		else
			return true;
	}
	
	@FXML
	private void handleOkButton()
	{
		if(validateData() == true)
		{
			String nome = nomeField.getText();
			String cognome = cognomeField.getText();
			String username = usernameField.getText();
			String mail = mailField.getText();
			String password = passwordField.getText();
			Agenzia agency = null;
			int agenziaID;
			
			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
			DaoAgenzia daoAgenzia = daoFactory.getDaoAgenzia();
			Agenzia selAgenzia = new Agenzia(daoAgenzia.getAgenzia( (String) agenzieBox.getValue()));
			agency = selAgenzia;
			
			Lavoratore lavoratore = new Lavoratore();
			lavoratore.setUsername(username);
			lavoratore.setNome(nome);
			lavoratore.setCognome(cognome);
			lavoratore.setEmail(mail);
			
			List<Object> parameters = new ArrayList<Object>();
			
			parameters.add(agency);
			parameters.add(lavoratore);
			parameters.add(password);
			
			FrontController frontController = new FrontController();
			
			frontController.processRequest("InserisciCapoAgenzia", parameters);
			
			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
			
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		
		stage.close();
	}

}
