package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import presentation.FrontController;
import presentation.Logger;
import presentation.Main;
import presentation.UserRole;
import presentation.ViewMapping;
import presentation.WindowManager;

public class EffettuaLoginController implements ControllerInt, Initializable {
	
	@FXML
	private TextField usernameField;
	
	@FXML
	private TextField passwordField;
	
	@FXML
	private Button okButton;
	
	@FXML
	private Button annullaButton;
	
	private Logger logger;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		logger = Logger.getInstance();
		
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}
	
	private boolean validateData()
	{
		if(usernameField.getText() == null || usernameField.getText().trim().isEmpty() || 
			passwordField.getText() == null || passwordField.getText().trim().isEmpty())
		{
			return false;
		}
		else
			return true;
	}
	
	@FXML
	private void handleOkButton()
	{
		if(validateData())
		{
			String backupUsername = logger.getUsername();
			UserRole backupRole = logger.getUserRole();
			String username = usernameField.getText();
			String password = passwordField.getText();
			
			List<Object> parameters = new ArrayList<Object>();
			
			Collections.addAll(parameters, username, password);
			
			FrontController frontController = new FrontController();
			frontController.processRequest("EffettuaLogin", parameters);
			
			if(!logger.getUsername().equals(backupUsername) || logger.getUserRole() != backupRole)
			{
				WindowManager winManager = new WindowManager();
				
				winManager.updateStageFromViewPath(Main.getPrimaryStage(), ViewMapping.homeView.get(logger.getUserRole()), "CarLoan", null, true);
			}
			
			Stage stage = (Stage) okButton.getScene().getWindow();
			stage.close();
			
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi o digitare annulla");

			alert.showAndWait();
		}
		
		
	}
	
	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}

}
