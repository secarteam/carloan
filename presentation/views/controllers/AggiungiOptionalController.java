package presentation.views.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import presentation.FrontController;

public class AggiungiOptionalController implements ControllerInt, Initializable {
	
	@FXML
	private TextField nomeField;
	
	@FXML
	private TextField descrizioneField;
	
	@FXML
	private TextField quantityField;
	
	@FXML
	private TextField costoField;
	
	@FXML
	private Button annullaButton;
	
	@FXML
	private Button aggiungiButton;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean validateData()
	{
		if(nomeField.getText() == null || nomeField.getText().trim().isEmpty() ||
			descrizioneField.getText() == null || descrizioneField.getText().trim().isEmpty() ||
			quantityField.getText() == null || quantityField.getText().trim().isEmpty() ||
			costoField.getText() == null || costoField.getText().trim().isEmpty())
		{
			return false;
		}
		else
			return true;
	}
	
	@FXML
	private void handleAggiungiButton()
	{
		if(validateData())
		{
			String nome = nomeField.getText();
			String descrizione = descrizioneField.getText();
			int quantity = Integer.parseInt(quantityField.getText());
			double costo = Double.parseDouble(costoField.getText());
			
			List<Object> parameters = new ArrayList<Object>();
			
			Collections.addAll(parameters, nome, descrizione, quantity, costo);
			
			FrontController frontController = new FrontController();
			frontController.processRequest("AggiungiOptional", parameters);
			
			Stage stage = (Stage) aggiungiButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi");

			alert.showAndWait();
		}
	}
	
	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) annullaButton.getScene().getWindow();
		stage.close();
	}

}
