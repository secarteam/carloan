package presentation.views.controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import business.bo.Cliente;
import business.bo.ContrattoInCorso;
import business.to.ContrattoInCorsoTO;
import integration.config.ConfigKeysSpec;
import integration.config.ConfigReader;
import integration.factories.DaoFactory;
import integration.interfaces.DaoCliente;
import integration.interfaces.DaoContrattoInCorso;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import presentation.FrontController;

public class ChiudiContrattoController implements Initializable, ControllerInt {

	@FXML
	private BorderPane chiudiCPane;

	@FXML
	private TextField userField;

	@FXML
	private Button cercaCoButton;

	@FXML
	private ComboBox listaCComboBox;

	@FXML
	private TextField idContField;

	@FXML
	private TextField targaField;

	@FXML
	private TextField date1Field;

	@FXML
	private TextField date2Field;

	@FXML
	private TextField saldoIField;

	@FXML
	private TextField puntoConsegnaField;

	@FXML
	private TextField accontoField;

	@FXML
	private TextField insertAccontoField;

	@FXML
	private Button insertAccontoButton;

	@FXML
	private Pane ChiloPane;

	@FXML
	private TextField setChiloField;

	@FXML
	private Button calcolaSaldoButton;

	@FXML
	private TextField importoFinaleField;

	@FXML
	private Button chiudiContrattoButton;

	@FXML
	private Button AnnullaButton;

	@FXML
	private DatePicker dataCE;

	private int idC = 0;

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		listaCComboBox.setDisable(true);
		//ChiloPane.setDisable(true);

		/*if(userField.getText() != null && !userField.getText().trim().isEmpty())
			listaCComboBox.setDisable(false);*/

		//inizializzazione ComboBox;

		/*DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
        DaoCliente daoC = daoFactory.getDaoCliente();
        DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();

        List<ContrattoInCorsoTO> listC = new LinkedList<ContrattoInCorsoTO>();

        listC = daoCiC.getContratti(new Cliente(daoC.getCliente(userField.getText())));

        ObservableList<String> options = FXCollections.observableArrayList();

		for (ContrattoInCorsoTO cTO : listC)
		{
			int idC1 = cTO.getId();
			options.add(Integer.toString(idC1));
		}

		listaCComboBox.setItems(options);*/
	}

	@FXML
	private void handleDettagliContrCB(){
		if(listaCComboBox.getValue() != null)
		{
			idC = Integer.valueOf((String) listaCComboBox.getValue());

			updateContrattoDetail(idC);


			ConfigReader reader = new ConfigReader(ConfigKeysSpec.getPath());

			/*if(daoCiC.getContratto(idC).getTariffaChilometraggio().getNome().equals(reader.getProperty(ConfigKeysSpec.getTarlim()))){
				ChiloPane.setDisable(false);
			}*/


		}
	}
	
	private void updateContrattoDetail(int idC)
	{
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
		DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();

		idContField.setText(Integer.toString(daoCiC.getContratto(idC).getId()));
		targaField.setText(daoCiC.getContratto(idC).getVettura().getTarga());
		date1Field.setText(daoCiC.getContratto(idC).getDataInizio().toString());
		date2Field.setText(daoCiC.getContratto(idC).getPrevisioneConsegna().toString());
		saldoIField.setText(Double.toString(daoCiC.getContratto(idC).getSaldoIniziale()));
		accontoField.setText(Double.toString(daoCiC.getContratto(idC).getAcconto()));
		puntoConsegnaField.setText(Integer.toString(daoCiC.getContratto(idC).getPuntoConsegna()));
	}

	private boolean validateCercaButton()
	{
		if(userField.getText() == null || userField.getText().trim().isEmpty())
			return false;
		else
			return true;
	}


	private boolean validateRegistraAccButton()
	{
		if(insertAccontoField.getText() == null || insertAccontoField.getText().trim().isEmpty())
			return false;
		else
			return true;
	}

	@FXML
	private void handleRegistraAccButton()
	{
		if(validateRegistraAccButton())
		{
			List<Object> parameters = new ArrayList<Object>();
			
			Collections.addAll(parameters, idC, Double.valueOf(insertAccontoField.getText()));
			
			FrontController frontController = new FrontController();
			
			frontController.processRequest("RegistraAcconto", parameters);
			
			updateContrattoDetail(idC);
			
//			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
//			DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();
//
//			daoCiC.getContratto(idC).setAcconto(Double.valueOf(insertAccontoField.getText()));
		}
	}

	@FXML
	private void handleCercaButton()
	{
		if(validateCercaButton())
		{
			listaCComboBox.setDisable(false);

			DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
	        DaoCliente daoC = daoFactory.getDaoCliente();
	        DaoContrattoInCorso daoCiC = daoFactory.getDaoContratto();

	        List<ContrattoInCorsoTO> listC = new LinkedList<ContrattoInCorsoTO>();

	        listC = daoCiC.getContratti(new Cliente(daoC.getCliente(userField.getText())));

	        ObservableList<String> options = FXCollections.observableArrayList();

			for (ContrattoInCorsoTO cTO : listC)
			{
				int idC1 = cTO.getId();
				options.add(Integer.toString(idC1));
			}

			listaCComboBox.setItems(options);
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Username non corretto o inesistente");

			alert.showAndWait();
		}
	}
	
	public boolean validateCalcolaSaldoButton()
	{		
		if(dataCE.getValue() == null || setChiloField.getText() == null || setChiloField.getText().trim().isEmpty())
			return false;
		else
			return true;
	}

	@FXML
	private void handleCalcolaSButton()
	{
		if(validateCalcolaSaldoButton()){
			FrontController frontController = new FrontController();

			List<Object> parameters = new ArrayList<Object>();

			Collections.addAll(parameters, idContField.getText(), setChiloField.getText(), accontoField.getText(), dataCE.getValue());

			double saldoFinale = (double) frontController.processRequest("CalcolaSaldo", parameters);

			importoFinaleField.setText(Double.toString(saldoFinale));
		}
		else{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Inserire data effettiva di consegna e i chilometri effettivamente percorsi.");

			alert.showAndWait();
		}
	}

	@FXML
	private void handleAnnullaButton()
	{
		Stage stage = (Stage) AnnullaButton.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void handleChiudiCButton()
	{
		if(importoFinaleField.getText() != null && !importoFinaleField.getText().trim().isEmpty())
		{
			FrontController frontController = new FrontController();
	
			List<Object> parameters = new ArrayList<Object>();
	
			Collections.addAll(parameters, idContField.getText(), (LocalDate) dataCE.getValue(), Double.valueOf(importoFinaleField.getText()), setChiloField.getText());
	
			frontController.processRequest("ChiudiContratto", parameters);
	
			Stage stage = (Stage) chiudiContrattoButton.getScene().getWindow();
			stage.close();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Calcolare il saldo");

			alert.showAndWait();
		}
	}
}
