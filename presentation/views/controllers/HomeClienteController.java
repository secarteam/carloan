package presentation.views.controllers;

import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import business.appservice.AppServiceFactory;
import business.bo.Vettura;
import business.to.AgenziaTO;
import business.to.FasciaTO;
import business.to.TariffaChilometraggioTO;
import business.to.TariffaTemporaleTO;
import integration.factories.DaoFactory;
import integration.interfaces.DaoAgenzia;
import integration.interfaces.DaoFascia;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import presentation.FrontController;
import presentation.Logger;
import presentation.Main;
import presentation.UserRole;
import presentation.ViewMapping;
import presentation.WindowManager;

public class HomeClienteController implements Initializable, ControllerInt {

    @FXML
    private BorderPane clienteHome;

    @FXML
    private CheckBox consegnaCheckbox;

    @FXML
    private ComboBox puntoDiConsegna;

    @FXML
    private Button searchButton;

    @FXML
    private ScrollPane resultView;

    @FXML
    private TilePane tilePane;

    @FXML
    private Group detailGroup;

    @FXML
    private TextField targaField;

    @FXML
    private TextField kmField;

    @FXML
    private TextField casaProduttriceField;

    @FXML
    private TextField modelloField;

    @FXML
    private TextField costoField;

    @FXML
    private VBox carDetailBox;

    @FXML
    private Button noleggiaButton;

    @FXML
	private Button hButton;

    @FXML
    private Label usernameLabel;

    @FXML
    private Button logoutButton;

    @FXML
    private ComboBox puntoDiRitiro;

    @FXML
    private ComboBox fasciaBox;

    @FXML
    private DatePicker dataRitiro;

    @FXML
    private DatePicker dataConsegna;

    @FXML
    private ComboBox tariffaTempBox;

    @FXML
    private ComboBox tariffaChilBox;

    private String targa;

    private String ritiro;

    private String consegna;

    private LocalDate ritiroData;

    private LocalDate consegnaData;

    private String fascia;

    private Logger logStatus;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		assert puntoDiConsegna != null : "fx:id=\"puntoDiConsegna\" was not injected: check your FXML file 'HomeCliente.fxml'.";
        assert searchButton != null : "fx:id=\"searchButton\" was not injected: check your FXML file 'HomeCliente.fxml'.";
        assert consegnaCheckbox != null : "fx:id=\"consegnaCheckbox\" was not injected: check your FXML file 'HomeCliente.fxml'.";
        assert clienteHome != null : "fx:id=\"clienteHome\" was not injected: check your FXML file 'HomeCliente.fxml'.";

        puntoDiConsegna.setDisable(true);
        tilePane.setPrefColumns(3);
        carDetailBox.setVisible(false);
        logStatus = Logger.getInstance();

        if(logStatus.getUserRole() == UserRole.ANONYMOUS || logStatus.getUsername() == null)
        	usernameLabel.setText("Anonymous");
        else
        	usernameLabel.setText(logStatus.getUsername());

        //inizializzazione delle ComboBox
        DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
        DaoFascia daoFascia = daoFactory.getDaoFascia();
        DaoAgenzia daoAgenzia = daoFactory.getDaoAgenzia();

		List<AgenziaTO> agenzieTO = daoAgenzia.getAgenzie();

		ObservableList<String> options = FXCollections.observableArrayList();

		for (AgenziaTO aTO : agenzieTO)
		{
			String nome = aTO.getNome();
			options.add(nome);
		}

		puntoDiRitiro.setItems(options);
		puntoDiConsegna.setItems(options);


		List<FasciaTO> fasceTO = daoFascia.getFasce();
		ObservableList<String> optF = FXCollections.observableArrayList();

		for(FasciaTO fTO : fasceTO)
		{
			String nome = fTO.getNome();
			optF.add(nome);
		}

		fasciaBox.setItems(optF);

	}

	@FXML
	private void handleConsegnaCheckbox()
	{
		if(consegnaCheckbox.isSelected())
			puntoDiConsegna.setDisable(false);
		else
			puntoDiConsegna.setDisable(true);
	}

	@FXML
	private void handleSearchButton()
	{
		/*
		 * Qui otterremo la lista di vetture che soddisfano la query (tramite l'uso degli AS).
		 * Per ogni vettura creeremo un Button che renderà visibile la VBox destra con i dettagli
		 * della vettura stessa. Sarà quindi disponibile il pulsante prenota.
		 */

		tilePane.getChildren().clear();

		if(validateSearchData())
		{
			List<String> stringList = new ArrayList<String>();

			ritiro = (String) puntoDiRitiro.getValue();
			consegna = null;

			if(consegnaCheckbox.isSelected() && puntoDiConsegna.getValue() != null)
				consegna = (String) puntoDiConsegna.getValue();
			else
				consegna = new String(ritiro);

			ritiroData = (LocalDate) dataRitiro.getValue();
			consegnaData = (LocalDate) dataConsegna.getValue();

			fascia = (String) fasciaBox.getValue();

			List<Object> parameters = new ArrayList();

			Collections.addAll(parameters, ritiro, fascia);

			//inizializzazione delle combobox delle tariffe temporali e tariffe chilometraggio
			updateTariffeBox(fascia);

			FrontController frontController = new FrontController();
			List<Vettura> vetture = (List<Vettura>) frontController.processRequest("EsploraServizi", parameters);

			if(vetture != null && !vetture.isEmpty())
			{
				for(Vettura v : vetture)
				{
					Button btn = new Button(v.getModello().getModello());

					btn.setOnAction(new EventHandler<ActionEvent>() {

						@Override
						public void handle(ActionEvent event)
						{
							carDetailBox.setVisible(true);

							targaField.setText(v.getTarga());
							casaProduttriceField.setText(v.getCasaProduttrice());
							modelloField.setText(v.getModello().getModello());
							kmField.setText(Double.toString(v.getKm()));

							targa = v.getTarga();
						}

					});

					tilePane.getChildren().add(btn);
				}
			}
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi");

			alert.showAndWait();
		}

	}

	private boolean validateSearchData()
	{
		if(puntoDiRitiro.getValue() == null || fasciaBox.getValue() == null ||
			dataRitiro.getValue() == null || dataConsegna.getValue() == null)
			return false;
		else
			return true;
	}

	private void updateTariffeBox(String fsc)
	{
		DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.MYSQL);
        DaoFascia daoFascia = daoFactory.getDaoFascia();

        List<TariffaTemporaleTO> tarTemp = daoFascia.getTariffeTemporaliByFasciaName(fsc);
        List<TariffaChilometraggioTO> tarChil = daoFascia.getTariffeChilometraggioByFasciaName(fsc);

        ObservableList<String> optF = FXCollections.observableArrayList();

		for(TariffaTemporaleTO tTO : tarTemp)
		{
			String nome = tTO.getNome();
			optF.add(nome);
		}

		tariffaTempBox.setItems(optF);

		ObservableList<String> optC = FXCollections.observableArrayList();

		for(TariffaChilometraggioTO tTO : tarChil)
		{
			String nome = tTO.getNome();
			optC.add(nome);
		}

		tariffaChilBox.setItems(optC);
	}

	@Override
	public void init(List<Object> parameters) {
		// TODO Auto-generated method stub

	}

	@FXML
	private void handleLogoutButton()
	{
		FrontController frontController = new FrontController();
		frontController.processRequest("EffettuaLogout", null);

		WindowManager winManager = new WindowManager();
		winManager.updateStageFromViewPath(Main.getPrimaryStage(), ViewMapping.homeView.get(logStatus.getUserRole()), "CarLoan", null, true);
	}

	private boolean validateData()
	{
		if(tariffaTempBox.getValue() == null || tariffaChilBox.getValue() == null)
		{
			return false;
		}
		else
			return true;
	}

	private boolean dataCorretta(){
		long tempIntervallo = ChronoUnit.DAYS.between(ritiroData, consegnaData);
		if(tempIntervallo <=0)
			return false;
		else
			return true;
	}

	@FXML
	private void handleNoleggiaButton()
	{
		if(validateData() && dataCorretta())
		{
			AppServiceFactory appServiceFactory = AppServiceFactory.getInstance();
			String view = appServiceFactory.getView("Noleggia");

			WindowManager winManager = new WindowManager();

			List<Object> parameters = new ArrayList<Object>();

			Collections.addAll(parameters, targa, ritiroData, consegnaData, (String) tariffaTempBox.getValue(), (String) tariffaChilBox.getValue(), consegna);

			Stage newStage = winManager.createNewStage(view, "Scegli Optional", parameters, true, Modality.APPLICATION_MODAL);

			newStage.show();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Compilare tutti i campi necessari o data non corretta");

			alert.showAndWait();
		}

	}

	@FXML
	private void handleHomeB(){
		WindowManager winManager = new WindowManager();
		winManager.updateStageFromViewPath(Main.getPrimaryStage(), ViewMapping.homeView.get(logStatus.getUserRole()), "CarLoan", null, true);
	}

}
