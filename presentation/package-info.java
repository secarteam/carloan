/**
 * Presentation tier: contiene Front Controller, Application Controller,
 * gestore delle finestre, gestione della sessione con i dati dell'utente,
 * view fxml e relativi controller javafx.
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 *
 */
package presentation;