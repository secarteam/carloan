package presentation;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {

	public static WindowManager windowManager = new WindowManager();
	private static Stage pStage;
	private static Logger logStatus;

	@Override
	public void start(Stage primaryStage) {
		try {
			setPrimaryStage(primaryStage);
			logStatus = Logger.getInstance();
			logStatus.setUserRole(UserRole.ANONYMOUS);
			logStatus.setUsername("anonymous");
			String viewResource = ViewMapping.homeView.get(logStatus.getUserRole());

			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource(viewResource));
			Scene scene = new Scene(root,800,600);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}


	public static Stage getPrimaryStage() {
        return pStage;
    }

	private static void setPrimaryStage(Stage newStage) {
        pStage = newStage;
    }

	public static void main(String[] args) {
		launch(args);
	}
}
