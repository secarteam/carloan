package presentation;

import java.util.List;

/**
 * Fornisce un punto di accesso centralizzato per eseguire le operazioni
 * relative ai casi d'uso.
 * 
 * @author Vito Vincenzo Covella, Francesca Gaudiomonte
 */

public class FrontController {
	
	private ApplicationControllerInt applicationController;

	public FrontController() {
		this.applicationController = new ApplicationController();
	}
	
	/**
	 * Fornisce un punto di accesso centralizzato per eseguire le operazioni relative
	 * ai casi d'uso. Delega le richieste all'Application Controller istanziato
	 * dal costruttore. 
	 * @param request	nome del servizio richiesto
	 * @param parameters	lista di parametri necessari per eseguire il servizio richiesto
	 * @return			dato restituito dopo l'esecuzione del servizio.
	 * @see presentation.ApplicationControllerInt
	 * @see presentation.ApplicationController
	 */
	public Object processRequest(String request, List<Object> parameters)
	{
		return applicationController.handleRequest(request, parameters);
	}

}
